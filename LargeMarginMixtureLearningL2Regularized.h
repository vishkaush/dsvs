#include <vector>

class LargeMarginMixtureLearningL2Regularized {
    private:
        int num_weights;
        int num_samples;
        int num_iter;
        double momentum;
        //double reg_g;
        //double lambda;
        double lambda1;
        double lambda2;
        double timestep;
        double eps;
        std::vector<double> e_grad_sq;
        std::vector<double> w;
        std::vector<double> w_avg;
        bool useAda;

    public:
        LargeMarginMixtureLearningL2Regularized(
                int num_weights, int num_samples, int num_iter,
                std::vector<double> weights, double lambda1, double lambda2, bool useAda,
                double momentum=0.9);//, double reg_g = 1000.0, );
        void update_weights(
                const std::vector<double> &y_max,
                const std::vector<double> &y_gt,
                const std::vector<bool> &negWeightsAllowed,
        	const std::vector<bool> &isLambdaOne);
        std::vector<double> get_weights();
        //std::vector<double> get_avg_weights();
        //double get_lambda();
};

