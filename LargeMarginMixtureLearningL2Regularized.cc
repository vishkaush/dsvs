/**
  Implementation of the Large Margin Framework in
  Lin, Hui, and Jeff A. Bilmes.
  "Learning mixtures of submodular shells with application to document summarization."
  arXiv preprint arXiv:1210.4871 (2012).

  with an AdaGrad flavor.
 **/
#include <math.h>
#include <float.h>
#include <iostream>
#include <string>
#include <cassert>
#include <stdexcept>
#include "LargeMarginMixtureLearningL2Regularized.h"


LargeMarginMixtureLearningL2Regularized::LargeMarginMixtureLearningL2Regularized(int num_weights, int num_samples, int num_iter, std::vector<double> weights, double lambda1, double lambda2, bool useAda, double momentum) : num_weights(num_weights), num_samples(num_samples), num_iter(num_iter), w(weights), lambda1(lambda1),  lambda2(lambda2), useAda(useAda), momentum(momentum), timestep(0), e_grad_sq(num_weights, 0.0), eps(10e-6), w_avg(weights) {
        if(num_weights <= 0) {
            throw std::invalid_argument(
                    std::string("num_weights = ") + std::to_string(num_weights)
                    + std::string(". num_weights must be non-negative."));
        }
        if(num_samples <= 0) {
            throw std::invalid_argument(
                    std::string("num_samples = ") + std::to_string(num_samples)
                    + std::string(". num_samples must be non-negative."));
        }
        if(num_iter <= 0) {
            throw std::invalid_argument(
                    std::string("num_iter = ") + std::to_string(num_iter)
                    + std::string(". num_iter must be non-negative."));
        }
        /*if(reg_g <=0) {
            throw std::invalid_argument(
                    std::string("reg_g = ") + std::to_string(reg_g)
                    + std::string(". reg_g must be non-negative."));
        }*/
        if(momentum < 0 || momentum >= 1) {
            throw std::invalid_argument(
                    std::string("momentum = ") + std::to_string(momentum)
                    + std::string(". momentum must be in [0, 1)."));
        }

        //lambda = (1.0 * reg_g / num_weights) * sqrt(2.0 * (1.0 + log(num_iter * num_samples)) / (1.0 * num_iter * num_samples));
	//std::cout << "Old Lambda is: " << lambda << std::endl;
        //if(weights == NULL) {
        //    cout << "Weight vector not provided" << endl;
        //    w(num_weights, 0.0)
        //        w_avg(num_weights, 0.0)
        //} else {
        //    cout << "Weight vector provided" << endl;
        //}


    }

void LargeMarginMixtureLearningL2Regularized::update_weights(
        const std::vector<double> &y_max, const std::vector<double> &y_gt,
        const std::vector<bool> &negWeightsAllowed,
        const std::vector<bool> &isLambdaOne) {

    double learn_r = 0.0;
    double grad_i = 0.0;

    assert(num_weights == negWeightsAllowed.size());

    if(y_max.size() != num_weights) {
        throw std::invalid_argument(
                std::string("y_max size = ") + std::to_string(y_max.size())
                + std::string(". y_max must be of size ")
                + std::to_string(num_weights) + std::string("."));
    }
    if(y_gt.size() != num_weights) {
        throw std::invalid_argument(
                std::string("y_gt size = ") + std::to_string(y_gt.size())
                + std::string(". y_gt must be of size ")
                + std::to_string(num_weights) + std::string("."));
    }

    timestep++;
    if(useAda) {
	      std::cout << "Using AdaGrad!!" << std::endl;
        learn_r = 0.1;
    }else {
	      std::cout << "Using SGD!!" << std::endl;
        learn_r = 0.2/timestep;
    }
    //learn_r = 2 / (lambda * timestep);
    //learn_r = 2 / (std::max(lambda1,lambda2) * timestep);
    // double max_w = std::numeric_limits<double>::min();

    for(int i = 0; i < num_weights; i++) {
        if(isLambdaOne[i] == true) {
            grad_i = lambda1 * w[i] + y_max[i] - y_gt[i];
        } else {
            grad_i = lambda2 * w[i] + y_max[i] - y_gt[i];
        }
        std::cout << "Gradient of " << i << " at time step " << timestep << " : " << grad_i << std::endl;
        //grad_i = lambda * w[i] + y_max[i] - y_gt[i];
        if(useAda) {
            //e_grad_sq[i] = momentum * e_grad_sq[i] + (1 - momentum) * grad_i * grad_i;
	          e_grad_sq[i] = e_grad_sq[i] + grad_i * grad_i;
            w[i] = w[i] - learn_r / sqrt(e_grad_sq[i] + eps) * grad_i;
        } else {
            w[i] = w[i] - learn_r * grad_i;
        }
        w[i] = negWeightsAllowed[i] ? w[i] : std::max(0.0, w[i]);
        // if(abs(w[i]) > abs(max_w)) max_w = w[i];
        w_avg[i] = (w_avg[i] * (timestep - 1) + w[i]) / timestep;
    }
    // std::cout<<"Max w: "<<max_w<<std::endl;
}

std::vector<double> LargeMarginMixtureLearningL2Regularized::get_weights() {
    return w;
}

/*std::vector<double> LargeMarginMixtureLearningL2Regularized::get_avg_weights() {
    return w_avg;
}

double LargeMarginMixtureLearningL2Regularized::get_lambda() {
    return lambda;
}*/
