import json
import glob
import argparse
import os

def parse(filename):
    try:
        with open(filename) as f:
            return json.load(f)

    except ValueError as e:
        print('invalid json: %s' % e)
        return None # or: raise

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to verify summary sizes of a particular video for a particular budget")
    parser.add_argument('--video', required=True, help='''Video name''')
    parser.add_argument('--budget', required=True, help='''0.05|0.15|0.3''')
    parser.add_argument('--summaries', required=True, help='''Full path to folder with summaries that need to be compared''')
    parser.add_argument('--dataset', required=True, help='''Full path to the dataset JSON which will be used for getting duration info''')
    args = parser.parse_args()
    dataset = parse(args.dataset)
    category = args.video.split("_")[0]
    videoDuration = dataset["categories"][category][args.video]["duration"]
    print("Video duration = " + str(videoDuration))
    tmp = int(float(args.budget) * int(videoDuration))
    if tmp%2 == 1:
        tmp = tmp - 1
    expectedNumSnippets = tmp/2
    print("Expected number of snippets in summary of budget " + args.budget + " = " + str(expectedNumSnippets))
    count = 0
    for filename in glob.glob(args.summaries + '**/*.json', recursive=True):
        #print("Checking " + os.path.basename(filename))
        test = parse(filename)
        actualNumSnippets = len(test["summary"])
        #print("Found actualNumSnippets = " + str(actualNumSnippets))
        if actualNumSnippets != expectedNumSnippets:
            print(os.path.basename(filename) + ": Mismatch: expected " + str(expectedNumSnippets) + " found " + str(actualNumSnippets))
        count += 1
    print("Done. Analyzed " + str(count) + " summaries.")
