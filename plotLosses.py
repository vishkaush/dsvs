import json
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import sys

if len(sys.argv) < 2:
    print("Usage: python plotLosses.py <name_of_losses_json> <output_file_name>")
    exit(0)

losses_json=json.load(open(sys.argv[1]))

trainingLosses = []
deltaNorms = []

trainingLosses = losses_json["training_losses"]
deltaNorms = losses_json["delta_norms"]

if len(trainingLosses) != len(deltaNorms):
    print("Strange! Both sizes are different! CAUTION")
    exit(0)

n=len(trainingLosses)
fig = plt.figure()
ax1 = fig.add_subplot(211)
#ax.plot(range(n),squareDiff,range(n),nonSquareDiff)
ax1.plot(trainingLosses,'k.-',label='Training Losses',color='red')
ax1.set_ylabel("Training Loss")
ax2 = fig.add_subplot(212)
ax2.plot(deltaNorms,'k.-',label='Delta Norms', color='blue')
ax2.set_ylabel("Delta Norms")
#plt.xticks(range(n), x_axis, size='small')
#plt.setp(ax1.get_xticklabels(), visible=True)
#plt.setp(ax2.get_xticklabels(), visible=True)
fig.suptitle('Training Losses and Delta Norms')
#plt.ylabel('Values')
plt.xlabel('Examples/Iteration')
fig.savefig(sys.argv[2] + '.png')
#plt.close()
