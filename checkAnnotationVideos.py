import json
import glob
import argparse
import os
from moviepy.editor import VideoFileClip
import math

def getLength(filename):
    clip = VideoFileClip(filename)
    return math.ceil(clip.duration)


def parse(filename):
    try:
        with open(filename) as f:
            return json.load(f)

    except ValueError as e:
        print('invalid json: %s' % e)
        return None # or: raise

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to do a sanity check on all gt_annotation videos")
    parser.add_argument('--root', required=True, help='''Full path to root folder inside which all videos need to be checked''')
    parser.add_argument('--dataset', required=True, help='''Full path to the dataset JSON which will be used for getting info about last segment of annoation''')
    args = parser.parse_args()
    dataset = parse(args.dataset)
    for filename in glob.glob(args.root + '**/*.avi', recursive=True):
        print("Checking " + filename)
        baseFileName = os.path.basename(filename)
        videoName = os.path.splitext(baseFileName)[0]
        components = videoName.split("_")
        category = components[0]
        video = components[1] + "_" + components[2]
        annotationFileName = dataset["categories"][category][video]["annotation"]
        annotationFile = dataset["root"] + category + "/" + annotationFileName
        annotationJson = parse(annotationFile)
        lastEnd = ""
        for item in annotationJson:
            lastEnd = item["end"]
        durationNumbers = lastEnd.replace(':','')
        calculatedDuration = int(durationNumbers[4])+10*int(durationNumbers[3])+60*int(durationNumbers[2])+600*int(durationNumbers[1])+3600*int(durationNumbers[0])
        print("Duration as per JSON = " + str(calculatedDuration))
        videoDuration = getLength(filename)
        print("Duration of annotated videos = " + str(videoDuration))
        if calculatedDuration != videoDuration:
            print("**** DURATION MISMATCH ****")


