#Give the root folder path as argument. The root folder should have one folder for each category. Each category folder should have all videos and annotations of that category.
#The categories should be mentioned in Line 17

import json
from os.path import join
from glob import glob
import io
import sys
import os
import subprocess
from moviepy.editor import VideoFileClip
import re
import argparse
from decimal import Decimal
import math

try:
    to_unicode = unicode
except NameError:
    to_unicode = str

def time_in_seconds_annotation(numbers):
    # print(numbers)
    seconds=int(numbers[4])+10*int(numbers[3])+60*int(numbers[2])+600*int(numbers[1])+3600*int(numbers[0])
    return seconds



#def getLength(filename):
#  result = subprocess.Popen(["ffprobe", filename],
#    stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
#  time = [x for x in result.stdout.readlines() if "Duration" in x]
#  #print(time)
#  segments=time[0].split(':')
#  # print(segments)
#  seconds= 3600*int(segments[1])+60*int(segments[2])+int(segments[3].split(".")[0])
#  # print(seconds)
#  return seconds

def getLength(filename):
    clip = VideoFileClip(filename)
    return math.ceil(clip.duration)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to generate metadata JSON for the dataset")
    parser.add_argument('--name', default='Dataset for Video Summarization', help='''A name for this dataset''')
    parser.add_argument('--path', required=True, help='''Path to dataset root, a directory under which there is a folder for each category''')
    parser.add_argument('--out', default='dataset', help='''Name of the dataset JSON to be created''')
    parser.add_argument('--category', default='all', help='''Category for which dataset JSON needs to be created. Default is all''')
    args = parser.parse_args()

    # Define data
    print("Number of folders found inside root folder = " + str(len(os.listdir(args.path))))

    data = {"name" : args.name,
            "root" : args.path,
            "num_categories" : len(os.listdir(args.path)),
            "categories" : {},
           }

    print(data)
    for category in os.listdir(args.path):
        if args.category != 'all' and category != args.category:
            continue
        files = []
        for ext in ('*.mp4', '*.avi'):
            files.extend(glob(join(data["root"]+category, ext)))
        print(files)
        data["categories"][category]={}
        data["categories"][category]["num_videos"]=len(files)
        total_duration=0
        for file in files:
            print(file)
            fileName = os.path.basename(file)
            videoName = os.path.splitext(fileName)[0]
            annotation_file = videoName + ".json"
            duration=0
            duration=getLength(file)
            total_duration+=duration
            video={"file_name" : fileName, "annotation" : annotation_file, "duration" : duration}
            data["categories"][category][videoName]=video
            data["categories"][category]["total_duration"]=total_duration

    # Write JSON file
    with io.open(args.out + '.json', 'w', encoding='utf8') as outfile:
        str_ = json.dumps(data,
                      indent=4, sort_keys=True,
                      separators=(',', ': '))
        outfile.write(to_unicode(str_))

    # Read JSON file
    with open(args.out+'.json') as data_file:
        data_loaded = json.load(data_file)

    print(data == data_loaded)
