import os
import sys
import json
import random
datasetJsonPath = sys.argv[1]
outputFolder = sys.argv[2]
budget = 0.05
with open(datasetJsonPath) as f:
    data = json.load(f)
    dataset = data["categories"]
    for domain, videoData in dataset.iteritems():
        print "Domain: ", domain
        pathToDomain = os.path.join(outputFolder, domain)
        os.mkdir(pathToDomain)
        for video, metaData in videoData.iteritems():
            if (video.split("_")[0] == domain):
                print "Video Name: ", video
                pathToVideo = os.path.join(pathToDomain, video)
                os.mkdir(pathToVideo)
                pathToBudget = os.path.join(pathToVideo, str(budget))
                os.mkdir(pathToBudget)
                print metaData, type(metaData)
                videoDuration = metaData['duration']
                print "Video Duration: ", videoDuration
                snippets = []
                for i in range(videoDuration + 1):
                    if(i % 2 == 0):
                        snippets.append(i)
                totalNumSnippets = videoDuration / 2
                numSummarySnippets = int(budget * totalNumSnippets)
                print "Number of snippets required: ", numSummarySnippets
                jsonDump = {}
                jsonDump["category"] = domain
                Summary = []
                for i in range(len(snippets)-1):
                    Summary.append({"end":snippets[i + 1], "begin":snippets[i]})
                print "Total snippets available: ", len(Summary)
                for x in range(500):
                    summaryIndices = random.sample(range(0, len(Summary)), numSummarySnippets)
                    uniformSummary = []
                    for summaryIndex in summaryIndices:
                        uniformSummary.append(Summary[summaryIndex])
                        print "Adding summary at index: ", summaryIndex
                    print "Final Uniform Summary: ", uniformSummary
                    print "Added ", len(uniformSummary), " Summaries"
                    jsonDump["summary"] = uniformSummary
                    jsonDump["name"] = video
                    jsonDump["budget"] = budget
                    fileName = os.path.join(pathToBudget, domain + "_" + video + "_random_" + str(budget) + "_" + str(x) + ".json" )
                    with open(fileName, 'w') as outfile:
                        json.dump(jsonDump, outfile)
