import json
import numpy as np
import math
import sys
import glob
import os
import argparse


def time_in_seconds_length(numbers):
    # print(numbers)
    seconds=int(numbers[5])+10*int(numbers[4])+60*int(numbers[3])+600*int(numbers[2])+3600*int(numbers[1])+36000*int(numbers[0])
    return seconds

def time_in_seconds_annotation(numbers):
    # print(numbers)
    seconds=int(numbers[4])+10*int(numbers[3])+60*int(numbers[2])+600*int(numbers[1])+3600*int(numbers[0])
    return seconds

def candidate_array(length,data):
    candidate=np.zeros(length)

    for block in data:
        begin=block["begin"]
        end=block["end"]
        candidate[begin:end]=1
    return candidate

k=2
beta=6
alpha=1
def reward(r):
    if(r>=0):
        return np.exp(alpha*r)
    else:
        return -k

#def penalty(r):
#    return np.exp(r)

def get_score(candidate,gt_json,duration):
    # Activation refers to mode, i.e., mode either 1 or 2.
    # If 2 you square the intersection else you don't.
    assert duration % 2 == 0
    summary_reward=0
    summary_reward_with_diversity=0
    penalty=0
    for block in gt_json:
        intersection=0
        beginSeconds = time_in_seconds_annotation(block["begin"].replace(':',''))
        endSeconds = time_in_seconds_annotation(block["end"].replace(':',''))
        if beginSeconds % 2 == 1:
            beginSeconds = beginSeconds + 1
        if endSeconds % 2 == 1 and endSeconds < duration:
            endSeconds = endSeconds + 1
        elif endSeconds % 2 == 1 and endSeconds > duration:
            endSeconds = endSeconds - 1
        elif endSeconds % 2 == 1:
            raise ValueError("Invalid endSeconds", endSeconds)
        if beginSeconds >= endSeconds:
            continue
        for t in range(beginSeconds, endSeconds):
            if candidate[t]==1:
                intersection+=1
        #intersection=intersection/2
        if intersection==0:
            continue
        if block["rating"]<0:
            # Note: reward for -ve rating is negative, therfore penalty is also negative
            penalty += intersection * reward(block["rating"])
        elif "repetitive" in block and block["repetitive"]==True:
            summary_reward_with_diversity+= min(beta,intersection) * (1 + min(intersection,beta) / min(beta, endSeconds -  beginSeconds)) * reward(block["rating"])
        else:
            summary_reward+= intersection * (1 + intersection / (endSeconds - beginSeconds)) * reward(block["rating"])
    assert penalty <= 0
    candidate_summary_score = summary_reward + summary_reward_with_diversity + penalty

    return candidate_summary_score

def get_true_score(candidate,gt_json):
    # print(activation)
    # Activation refers to mode, i.e., mode either 1 or 2.
    # If 2 you square the intersection else you don't.
    summary_reward=0
    summary_reward_with_diversity=0
    penalty=0
    for block in gt_json:
        intersection=0
        beginSeconds = time_in_seconds_annotation(block["begin"].replace(':',''))
        endSeconds = time_in_seconds_annotation(block["end"].replace(':',''))
        for t in range(beginSeconds, endSeconds):
            if candidate[t]==1:
                intersection+=1
        #intersection=intersection/2
        if block["rating"]<0:
            # Note: reward for -ve rating is negative, therfore penalty is also negative
            penalty += intersection * reward(block["rating"])
        elif "repetitive" in block and block["repetitive"]==True:
            summary_reward_with_diversity+= min(beta,intersection) * (1 + min(intersection, beta) / min(beta, endSeconds - beginSeconds)) *reward(block["rating"])
        else:
            summary_reward+= intersection * (1 + intersection/ (endSeconds - beginSeconds)) *  reward(block["rating"])
    candidate_summary_score = summary_reward + summary_reward_with_diversity + penalty

    return candidate_summary_score

def compute(dataset_json,category,video_name,json_file_name):
    # Activation refers to mode, i.e., mode either 1 or 2.
    # If 2 you square the intersection else you don't.
    # print(activation)
    candidate_json = json.loads(open(json_file_name).read())
    print(dataset_json["root"]+category+"/"+dataset_json["categories"][category][video_name]["annotation"])
    annotation_file=dataset_json["root"]+category+"/"+dataset_json["categories"][category][video_name]["annotation"]
    with open(annotation_file) as data_file:
        gt_json = json.loads(data_file.read())

    length=dataset_json["categories"][category][video_name]["duration"]
    length = length if length % 2 == 0 else length - 1
    # rating,repetitive=set_rating(length,gt_json)
    candidate=candidate_array(length, candidate_json["summary"])
    final_score=get_score(candidate,gt_json, length)
    print("Final Score: " + str(final_score))
    if "score" in candidate_json:
        if final_score==candidate_json["score"]:
            print("New Score same as Old Score")
        else:
            print("WATCH OUT! New Score different from Old Score")
    candidate_json["score"]=final_score

    with open(json_file_name, 'w') as outfile:
        json.dump(candidate_json, outfile)
    return final_score

# summaryFiles = glob.glob(sys.argv[2] + "*.json")
# dataset_json=json.load(open(sys.argv[1]))

def compute_json(gt_json,candidate_json, length):
    # activation=activation.split()
    # activation = map(int, activation)
    # print(activation)

    candidate=candidate_array(length,candidate_json)
    final_score=get_score(candidate,gt_json,  length)
    print("Final Score: " + str(final_score))
    if "score" in candidate_json:
        if final_score==candidate_json["score"]:
            print("New Score same as Old Score")
        else:
            print("WATCH OUT! New Score different from Old Score")
    # candidate_json["score"]=final_score

    # with open(json_file_name, 'w') as outfile:
    #     json.dump(candidate_json, outfile)
    return final_score

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to calculate and append score of summaries")
    parser.add_argument('--dataset', required=True, help='''Full path to metadata JSON of the dataset''')
    parser.add_argument('--path', required=True, help='''Path of summaries root folder''')

    args = parser.parse_args()
    dataset_json=json.load(open(args.dataset))

    for budget in [0.05,0.15,0.3]:
        for category in dataset_json["categories"]:
            for video_name in dataset_json["categories"][category]:
                if video_name != "num_videos" and video_name != "total_duration":
                    file_path=args.path+category+'/'+video_name+'/'+ str(budget)+'/'
                    print("Loading File Path: "+file_path)
                    summaryFiles = glob.glob(file_path + "*.json")
                    for file in summaryFiles:
                        fileName = os.path.basename(file)
                        summaryName = os.path.splitext(fileName)[0]
                        print("Processing summary: " + summaryName)
                        print(video_name)
                        #components = summaryName.split("_")
                        # video_name=components[1]
                        compute(dataset_json,category,video_name,file)

