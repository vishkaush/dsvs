import os
import sys
import csv
import json

normsJsonPath = "/research/vishal/submodularMixturesSummarization/aux_results/norms/norms.json"
def normaliseScore(score, video):
    with open(normsJsonPath) as f:
        data = json.load(f)
        minScore = data[video]["min_score"]
        maxScore = data[video]["max_score"]
        normalisedScore = (score - minScore) / (maxScore - minScore)
        return normalisedScore

results = {}
uniform_summaries_with_score = "/research/vishal/submodularMixturesSummarization/aux_results/uniform_summaries_with_score"
gt_summaries_with_score = "/research/vishal/submodularMixturesSummarization/aux_results/gt_summaries_with_score"
with open('uniformBaseline.csv', 'wb') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    domains = os.listdir(uniform_summaries_with_score)
    for domain in domains:
        with open("/research/vishal/submodularMixturesSummarization/dataset/" + domain.lower() + "_train_test.json") as file:
            split = json.load(file)
            trainVideos = [str(x) for x in split["train_videos"]]
            testVideos = [str(x) for x in split["test_videos"]]
            print "Train Videos ", trainVideos, " Type ", type(trainVideos)
            print "Test Videos ", testVideos, " Type ", type(testVideos)
        perBudget = {"0.05":[0,0,0,0,0,0,0,0,0], "0.15":[0,0,0,0,0,0,0,0,0], "0.3":[0,0,0,0,0,0,0,0,0]}
        pathToDomain = os.path.join(uniform_summaries_with_score, domain)
        videosInDomain = os.listdir(pathToDomain)
        for video in videosInDomain:
            pathToVideo = os.path.join(pathToDomain, video)
            videoBudgets = os.listdir(pathToVideo)
            for budget in videoBudgets:
                pathToBudget = os.path.join(pathToVideo, budget)
                randomSummaries = os.listdir(pathToBudget)
                minRandom = sys.maxint
                maxRandom = -sys.maxint-1
                sumRandom = 0
                for randomSummary in randomSummaries:
                    pathToRandomSummary = os.path.join(pathToBudget, randomSummary)
                    #print ("Parsing Random Summary at: ", pathToRandomSummary)
                    with open(pathToRandomSummary) as f:
                        data = json.load(f)
                        randomSummaryScore = data["score"]
                        sumRandom += randomSummaryScore
                        if (randomSummaryScore < minRandom):
                            minRandom = randomSummaryScore
                        if(randomSummaryScore > maxRandom):
                            maxRandom = randomSummaryScore
                avgRandom = sumRandom / len(randomSummaries)
                normalisedMinRandom = normaliseScore(minRandom, video)
                normalisedMaxRandom = normaliseScore(maxRandom, video)
                normalisedAvgRandom = normaliseScore(avgRandom, video)
                gtSummaryPath = gt_summaries_with_score + "/" + domain + "/" + video + "/" + budget
                with open(os.path.join(gtSummaryPath, os.listdir(gtSummaryPath)[0])) as f:
                    data = json.load(f)
                    normalisedGtScore = normaliseScore(data["score"], video)
                minLoss = normalisedGtScore - normalisedMinRandom
                maxLoss = normalisedGtScore - normalisedMaxRandom
                avgLoss = normalisedGtScore - normalisedAvgRandom
                print ("Results: " , [domain, video, budget, minLoss, maxLoss, avgLoss])
                wr.writerow([domain, video, budget, minLoss, maxLoss, avgLoss])
                if(video in trainVideos):
                    perBudget[budget][0] += minLoss
                    perBudget[budget][1] += maxLoss
                    perBudget[budget][2] += avgLoss
                else:
                    perBudget[budget][3] += minLoss
                    perBudget[budget][4] += maxLoss
                    perBudget[budget][5] += avgLoss
                perBudget[budget][6] += minLoss
                perBudget[budget][7] += maxLoss
                perBudget[budget][8] += avgLoss
        for key,value in perBudget.iteritems():
            value[0] = value[0] / len(trainVideos)
            value[1] = value[1] / len(trainVideos)
            value[2] = value[2] / len(trainVideos)
            value[3] = value[3] / len(testVideos)
            value[4] = value[4] / len(testVideos)
            value[5] = value[5] / len(testVideos)
            value[6] = value[6] / (len(trainVideos) + len(testVideos))
            value[7] = value[7] / (len(trainVideos) + len(testVideos))
            value[8] = value[8] / (len(trainVideos) + len(testVideos))
        results[domain] = perBudget
        print results

with open('uniformTrainTestAvg.csv', 'wb') as csv_file:
    writer = csv.writer(csv_file)
    for key, value in results.items():
        for key2, value2 in value.items():
            toAppend = [key]
            toAppend.append(key2)
            toAppend += value2
            print toAppend
            writer.writerow(toAppend)
myfile.close()
