#! /usr/bin/python3

import json
import argparse
import os
import numpy as np
from utils import *

ALPHA = 1
BETA = 6
PENALTY = -5


def get_min_score(annotation, loss_params):
   return sum(
        [
            (x[1] - x[0]) * loss_params['penalty']
            for x in annotation if x[2] < 0
        ]
    )

def get_max_score(annotation, loss_params):
    return sum(
        [
            (x[1] - x[0]) * 2 * np.exp(loss_params['alpha'] * x[2])
            for x in annotation if x[2] >= 0
        ]
    )

def get_min_max_scores(annotation_json_file, loss_params):
    annotation = annotation_json_file_to_list(annotation_json_file)
    annotation = normalize_annotation(annotation, loss_params['beta'])

    return (
        get_min_score(annotation, loss_params),
        get_max_score(annotation, loss_params)
    )

def process_dataset(dataset_json_file, loss_params):
    dataset_json = json.load(open(dataset_json_file, 'r'))
    root_path = dataset_json['root']
    result = dict()

    for category in dataset_json['categories']:
        for video in dataset_json['categories'][category]:
            if video not in {'total_duration', 'num_videos'}:
                metadata = dataset_json['categories'][category][video]
                json_file = metadata['annotation']
                dir_prefix = video.split('_')[0]
                json_file_path =  os.path.join(root_path, dir_prefix, json_file)
                min_score, max_score = get_min_max_scores(
                        json_file_path, loss_params)
                result[video] = {
                    'min_score': min_score,
                    'max_score': max_score,
                    #'mode': loss_params['mode']
                }
    return result

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
        Process dataset.json to obtain min and max scores.''')

    parser.add_argument('--dataset', required=True, help='''
        Path to dataset.json.''')
    parser.add_argument('--alpha', default=ALPHA, type=float, help='''
        Alpha value for score function.''')
    parser.add_argument('--beta', default=BETA, type=float, help='''
        Beta factor for score function.''')
    parser.add_argument('--penalty', default=PENALTY, type=float, help='''
        Penalty. Must be negative.''')
    #parser.add_argument('--mode', default=1, type=int, choices={1, 2}, help='''
    #    Mode for score calculation. 1: linear, 2: squared.''')
    parser.add_argument('--out-file', required=True, help='''
        Path to store output file.''')

    args = parser.parse_args()

    loss_params = {
    #   'mode': args.mode,
        'alpha': args.alpha,
        'beta': args.beta,
        'penalty': args.penalty
    }

    print("Loss params: %s." % str(loss_params))

    result = process_dataset(args.dataset, loss_params)

    json.dump(result, open(args.out_file, 'w'))
