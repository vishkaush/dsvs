#!/usr/bin/python3

# arg 0: mode in {loss, weights, score}
#       loss :- plot train loss
#       weights :- plot weights
#       score :- report scores
# arg 1: json file
# arg 2: out_dir

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import json

def generate_plots(data, out_dir):
    losses = data['training_losses']
    epoch_num = list(range(1, len(losses) + 1))

    budget = data['budget']
    category = data['category']

    plt.plot(epoch_num, losses)
    plt.title('Epoch vs Losses: ' + category + ' ' + budget)
    plt.ylabel('Train loss')
    plt.xlabel('Epoch')
    plt.tight_layout()
    plt.savefig(out_dir + '/' + category + '_' + budget + '_losses.png')

def genreate_weight_plot(data, out_dir):
    weight_dict = dict()
    budget = data['budget']
    category = data['category']
    for i, val in enumerate(data['component_lookup'][:-1]):
        if 'Mod:' in val:
            val = val.split(':')[1]
            if val in weight_dict:
                weight_dict[val] += data['weights'][i]
            else:
                weight_dict[val] = data['weights'][i]
        weight_dict[val] = data['weights'][i]

    components = []
    weights =[]
    for key in weight_dict:
        components.append(key)
        weights.append(weight_dict[key])

    plt.plot(range(len(components)), weights)
#    plt.figure(figsize=(20, 10))
    plt.title('Components vs Weight: ' + category + ' ' + budget)
    plt.ylabel('Weight')
    plt.xlabel('Component')
    plt.xticks(range(len(components)), components, rotation=90)
    plt.tight_layout()
    plt.savefig(out_dir + '/' + category + '_' + budget + '_weights.png')

def generate_score_csv(data, out_dir):
    generate_score_loss_csv(data, out_dir)
    generate_submod_loss_csv(data, out_dir)
    generate_snippet_csv(data, out_dir)

def generate_snippet_csv(data, out_dir):
    header = "category,split,feature,video,snippet_id_1,snippet_id_2,snippet_id_3,snippet_id_4,snippet_id_5"
    category = data['category']
    budget = data['budget']
    file_contents = header

    for vid in data['train_mod_values']:
        for mod in data['train_mod_values'][vid]:
            snippet_ids = sorted(range(len(data['train_mod_values'][vid][mod])), key=lambda i: data['train_mod_values'][vid][mod][i], reverse=True)[:5]
            snippet_str = ""
            for s_id in snippet_ids:
                snippet_str += "," + str(s_id)
            file_contents += '\n' + category + ",train," + mod + "," + vid + snippet_str

    for vid in data['train_concept_values']:
        for mod in data['train_concept_values'][vid]:
            snippet_ids = sorted(range(len(data['train_concept_values'][vid][mod])), key=lambda i: data['train_concept_values'][vid][mod][i], reverse=True)[:5]
            snippet_str = ""
            for s_id in snippet_ids:
                snippet_str += "," + str(s_id)
            file_contents += '\n' + category + ",train," + mod + "," + vid + snippet_str

    for vid in data['test_mod_values']:
        for mod in data['test_mod_values'][vid]:
            snippet_ids = sorted(range(len(data['test_mod_values'][vid][mod])), key=lambda i: data['test_mod_values'][vid][mod][i], reverse=True)[:5]
            snippet_str = ""
            for s_id in snippet_ids:
                snippet_str += "," + str(s_id)
            file_contents += '\n' + category + ",test," + mod + "," + vid + snippet_str


    for vid in data['test_concept_values']:
        for mod in data['test_concept_values'][vid]:
            snippet_ids = sorted(range(len(data['test_concept_values'][vid][mod])), key=lambda i: data['test_concept_values'][vid][mod][i], reverse=True)[:5]
            snippet_str = ""
            for s_id in snippet_ids:
                snippet_str += "," + str(s_id)
            file_contents += '\n' + category + ",test," + mod + "," + vid + snippet_str


    with open(out_dir + '/' + category + '_' + budget + '_best_snippets.csv', 'w') as f:
        f.write(file_contents)


def generate_submod_loss_csv(data, out_dir):
    header = "category,submod_fn,score_loss"

    category = data['category']
    budget = data['budget']

    train_dict = dict()
    test_dict = dict()

    for vid in data['train_submod_scores']:
        gt_score = data['train_gt_scores'][vid]
        for fn in data['train_submod_scores'][vid]:
            if fn in train_dict:
                train_dict[fn] += (gt_score - data['train_submod_scores'][vid][fn])
            else:
                train_dict[fn] = (gt_score - data['train_submod_scores'][vid][fn])

    file_contents = header
    for key in train_dict:
        val = train_dict[key] / len(data['train_submod_scores'])
        file_contents += '\n' + category + "," + key + "," + str(val)

    with open(out_dir + '/' + category + '_' + budget + '_submod_score_loss_train.csv', 'w') as f:
        f.write(file_contents)


    for vid in data['test_submod_scores']:
        gt_score = data['test_gt_scores'][vid]
        for fn in data['test_submod_scores'][vid]:
            if fn in test_dict:
                test_dict[fn] += (gt_score - data['test_submod_scores'][vid][fn])
            else:
                test_dict[fn] = (gt_score - data['test_submod_scores'][vid][fn])

    file_contents = header
    for key in test_dict:
        val = test_dict[key] / len(data['test_submod_scores'])
        file_contents += '\n' + category + "," + key + "," + str(val)


    with open(out_dir + '/' + category + '_' + budget + '_submod_score_loss_test.csv', 'w') as f:
        f.write(file_contents)

    file_contents = header
    for key in test_dict:
        val = (test_dict[key] + train_dict[key]) / (len(data['test_submod_scores']) + len(data['train_submod_scores']))
        file_contents += '\n' + category + "," + key + "," + str(val)


    with open(out_dir + '/' + category + '_' + budget + '_submod_score_loss_all.csv', 'w') as f:
        f.write(file_contents)


def generate_score_loss_csv(data, out_dir):
    file_contents = ""
    header = "category,split,avg_score_loss,max_score_loss,max_video,min_score_loss,min_video"
    file_contents += header

    category = data['category']
    budget = data['budget']

    max_score = 0
    min_score = 1
    max_train_score = 0
    min_train_score = 1
    max_test_score = 0
    min_test_score = 1

    max_video = ""
    min_video = ""
    max_train_video = ""
    min_train_video = ""
    max_test_video = ""
    min_test_video = ""

    train_score_tot = 0
    for vid in data['train_candidate_scores']:
        gt_score = data['train_gt_scores'][vid]
        cand_score = data['train_candidate_scores'][vid]

        score_loss = gt_score - cand_score;

        if score_loss > max_train_score:
            max_train_score = score_loss
            max_train_video = vid
        if score_loss < min_train_score:
            min_train_score = score_loss
            min_train_video = vid

        train_score_tot += score_loss

    train_avg_score = train_score_tot / len(data['train_candidate_scores'])

    file_contents += '\n' + category + ",train," + str(train_avg_score) + "," + str(max_train_score) + "," + max_train_video + "," + str(min_train_score) + "," + min_train_video

    test_score_tot = 0
    for vid in data['test_candidate_scores']:
        gt_score = data['test_gt_scores'][vid]
        cand_score = data['test_candidate_scores'][vid]

        score_loss = gt_score - cand_score

        if score_loss > max_test_score:
            max_test_score = score_loss
            max_test_video = vid
        if score_loss < min_test_score:
            min_test_score = score_loss
            min_test_video = vid

        test_score_tot += score_loss

    test_avg_score = test_score_tot / len(data['test_candidate_scores'])

    file_contents += '\n' + category + ",test," + str(test_avg_score) + "," + str(max_test_score) + "," + max_test_video + "," + str(min_test_score) + "," + min_test_video

    min_score = min_test_score
    min_video = min_test_video
    max_score = max_test_score
    max_video = max_test_video

    if min_train_score < min_score:
        min_score = min_train_score
        min_video = min_train_video
    if max_train_score > max_score:
        max_score = max_train_score
        max_video = max_train_video

    tot_avg = (train_score_tot + test_score_tot) / ( len(data['test_candidate_scores']) + len(data['train_candidate_scores']))

    file_contents += '\n' + category + ",all," + str(tot_avg) + "," + str(max_score) + "," + max_video + "," + str(min_score) + "," + min_video


    with open(out_dir + '/' + category + '_' + budget + '_losses.csv', 'w') as f:
        f.write(file_contents)



if __name__ == '__main__':
    mode = sys.argv[1]
    json_file = sys.argv[2]
    out_dir = sys.argv[3]

    json_data = json.load(open(json_file, 'r'))

    if mode == 'loss':
        generate_plots(json_data, out_dir)
    elif mode == 'weights':
        genreate_weight_plot(json_data, out_dir)
    elif mode == 'score':
        generate_score_csv(json_data, out_dir)
    else:
        print('Unrecognised mode. Exiting.')
