import json
import random

# Run in a folder with dataset.json to produce test-train split files for all categories

def get_best_comb(d, dur):
    vids = [ x for x in d.keys() if x not in {'num_videos', 'total_duration'}]
    tot_dur = sum([d[vid]['duration']  for vid in vids])
    assert tot_dur == dur

    print("Processing cat", vids[0])
    count = 0
    while True:
        random.shuffle(vids)
        cum_sum = 0
        count += 1
        i = 0
        for vid in vids:
            i += 1
            cum_sum += d[vid]['duration']
            if cum_sum <= 0.72 * dur and cum_sum >= 0.68 * dur:
                print('done')
                print(cum_sum / dur)
                return vids[0:i], vids[i:]
            if cum_sum >= 0.72*dur:
                print('Failed', count, cum_sum / dur, (cum_sum -  d[vid]['duration']) / dur)
                break

def get_split(d):
    vids = [ x for x in d.keys() if x not in {'num_videos', 'total_duration'}]
    random.shuffle(vids)
    split_point = int(round(0.7 * len(vids), 0))
    return vids[:split_point], vids[split_point:]

d = json.load(open('dataset.json', 'r'))
res = dict()
for cat in d['categories']:
    cat_res = dict()
    dur = d['categories'][cat]['total_duration']
    #train, test = get_best_comb(d['categories'][cat], dur)
    train, test = get_split(d['categories'][cat])
    cat_res = {
            'train_videos': train,
            'test_videos': test,
            'train_category': cat,
            'test_category': cat
    }
    print(cat_res)
    json.dump(cat_res, open(str.lower(cat) + '_train_test.json', 'w'))
    res[cat] = cat_res

#json.dump(res, open('dataset_train_test.json', 'w'))

