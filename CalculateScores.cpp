#include "../../src/utils/json.hpp"
#include "../../src/dependencies/datk/src/datk.h"

int main(int argc, char* argv[]) {
    if(argc < 2) {
        cout << "Usage: ./CalculateScore <candidate_summary_json_file> <dataset.json> <ratings.json>|none" << endl;
        return 0;
    }
    nlohmann::json candidate;
    std::ifstream candidateJson;
    candidateJson.open(argv[1]);
    candidateJson >> candidate;
    cout << "Candidate loaded" << endl;

    nlohmann::json dataset;
    std::ifstream datasetJson;
    datasetJson.open(argv[2]);
    datasetJson >> dataset;
    cout << "Dataset JSON loaded" << endl;

    std::string category = candidate["category"];
    //std::string video = candidate["video"];
    //for GT summary it is called "name"
    std::string video = candidate["name"];

    nlohmann::json annotation;
    std::string annotationJsonFile;
    if(strcmp(argv[3], "none")!=0) {
        cout << "Annotation file specified, picking that" << endl;
        annotationJsonFile = argv[3];
    } else {
        cout << "Annotation file not specified, reading from dataset JSON" << endl;
        annotationJsonFile = dataset["root"].get<std::string>() + category + "/" + dataset["categories"][category][video]["annotation"].get<std::string>();
    }
    std::ifstream annotationJson;
    annotationJson.open(annotationJsonFile);
    annotationJson >> annotation;

    int groundSetSize = ((int)dataset["categories"][category][video]["duration"])/2;
    //int groundSetSize = 31;
    std::vector<datk::Set> segments;
    std::vector<int> ratings;
    std::vector<bool> isRedundant;
    double alpha = 1;
    double beta = 6;
    double penalty = 2;
    int segIndex = 0;
    int maxSnippetId = -1;
    for(auto seg : annotation) {
        datk::Set snippets;
        string line = seg["begin"];
        std::vector <string> tokens;
        stringstream check1(line);
        string intermediate;
        while(getline(check1, intermediate, ':')) {
            tokens.push_back(intermediate);
        }
        int hrs = stoi(tokens[0]);
        int mins = stoi(tokens[1]);
        int secs = stoi(tokens[2]);
        float beginSeconds=3600*hrs+60*mins+secs;
        line = seg["end"];
        stringstream check2(line);
        tokens.clear();
        while(getline(check2, intermediate, ':')) {
            tokens.push_back(intermediate);
        }
        hrs = stoi(tokens[0]);
        mins = stoi(tokens[1]);
        secs = stoi(tokens[2]);
        float endSeconds=3600*hrs+60*mins+secs;
        //add snippet ids to this set
        int beginSnippetId = ceil(beginSeconds/2.0);
        int endSnippetId =floor((endSeconds-1)/2.0);
        //for last end, if odd
        if (endSnippetId == groundSetSize) endSnippetId --;
        if (endSnippetId > maxSnippetId) maxSnippetId = endSnippetId;
        for(int id = beginSnippetId; id <= endSnippetId; id++) {
            snippets.insert(id);
            // cout << id << " ";
        }
        segments.push_back(snippets);
        ratings.push_back(seg["rating"]);
        bool isRepetitive = false;
        if(seg.find("repetitive") == seg.end()) {
            isRepetitive = false;
        } else {
            if(seg["repetitive"] == true) isRepetitive = true;
            else isRepetitive = false;
        }
        isRedundant.push_back(isRepetitive);
        // cout << endl;
    }
    cout << "Max snippet id = " << maxSnippetId << endl;
    datk::VideoSummLoss vsl1(groundSetSize, segments, ratings, isRedundant, alpha, beta, penalty, 1);
    datk::Set candidateIndices;
    int tmp;
    for(auto seg : candidate["summary"]) {
        tmp = (int)seg["begin"];
        candidateIndices.insert(tmp/2);
    }
    //cout << "Candidate:" << endl;
    //for(auto i: candidateIndices) {
    //    cout << i << " ";
    //}
    //cout << endl;
    double candidateScore = vsl1.eval(candidateIndices);
    cout << "Score = " << candidateScore << endl;
}

