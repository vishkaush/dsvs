#include "MixtureModel.h"

int main(int argc, char* argv[]) {
    if(argc != 20) {
        cout << "Usage: ./Training <num_of_epochs_to_train> <training_json_file> <features_folder> <norm_json_file> <shots_json_folder> <dataset_json_file> <ground_truth_folder> {<model_file>} | {<components_json_file>} <budget> <name> <marginWeight> <useRandomGt(0==false)> <allowNegWeightForModComponents(0==False)> <modelParamsJson> <useRandomGreedy(0==false)> <lambda1> <lambda2> <useAda(0==false)> <calculateSubModScores(0==false)>" << endl;
        return 1;
    }
    int num_epochs = atoi(argv[1]);
    cout << "Number of epochs for training = " << num_epochs << endl;

    std::string trainingDataJsonFile;
    trainingDataJsonFile = argv[2];

    std::string featuresFolder;
    featuresFolder = argv[3];

    std::string normJsonFile;
    normJsonFile = argv[4];

    std::string shotsFolder;
    shotsFolder = argv[5];

    std::string datasetJsonFile;
    datasetJsonFile = argv[6];

    std::string gtFolder = argv[7];

    std::string modelFile = "";

    std::string componentsJsonFile = "";

    std::string budget;

    if(strstr(argv[8], "_model_")) { //if model is specified
        modelFile = argv[8];
        budget = argv[9];
        cout << "Model specified: " << modelFile << endl;
    } else {
        componentsJsonFile = argv[8];
        budget = argv[9];
    }

    std::string name = argv[10];
    double marginWeight = stod(argv[11]);
    bool useRandomGt = bool(stoi(argv[12]));
    bool allowNegWeightForMod = bool(stoi(argv[13]));
    std::string modelParamsJson = argv[14];
    bool useRandomGreedy = bool(stoi(argv[15]));
    double lambda1 = atof(argv[16]);
    double lambda2 = atof(argv[17]);
    bool useAda = bool(stoi(argv[18]));
    bool calculateSubModScores = bool(stoi(argv[19]));
    cout<< "Parsing model params JSON"<<endl;
    nlohmann::json modelParams;
    std::ifstream modelParamsFile;
    modelParamsFile.open(modelParamsJson);
    modelParamsFile >> modelParams;

    cout << "Parsing training data JSON" << endl;
    nlohmann::json trainingData;
    std::ifstream trainingDataJson;
    trainingDataJson.open(trainingDataJsonFile);
    trainingDataJson >> trainingData;

    cout << "Parsing norms JSON" << endl;
    nlohmann::json normData;
    std::ifstream normDataJson;
    normDataJson.open(normJsonFile);
    normDataJson >> normData;

    cout << "Parsing dataset JSON" << endl;
    nlohmann::json dataset;
    std::ifstream datasetJson;
    datasetJson.open(datasetJsonFile);
    datasetJson >> dataset;

    MixtureModel* model;
    if(modelFile != "") {
        model = new MixtureModel(modelFile, budget);
    } else {
        cout << "Parsing components JSON" << endl;
        nlohmann::json components;
        std::ifstream componentsJson;
        componentsJson.open(componentsJsonFile);
        componentsJson >> components;

        model = new MixtureModel(components, budget);
    }
    model->train(num_epochs, trainingData, featuresFolder, normData, shotsFolder, dataset, gtFolder, name, marginWeight, useRandomGt, allowNegWeightForMod, modelParams, useRandomGreedy, lambda1, lambda2, useAda, calculateSubModScores);
}
