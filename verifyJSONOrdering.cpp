#include "../../src/utils/json.hpp"
#include "../../src/dependencies/datk/src/datk.h"

int main(int argc, char* argv[]) {

    std::string componentsJsonFile = argv[1];
    cout << "Parsing JSON" << endl;
    nlohmann::json components;
    std::ifstream componentsJson;
    componentsJson.open(componentsJsonFile);
    componentsJson >> components;
    for (int i = 0; i < 10; i++) {
        cout << "\n\n****** " << i << " ********\n\n" <<endl;
        for (auto it = components.begin(); it != components.end(); ++it)
        {
            std::cout << it.key() << " | " << it.value() << "\n";
            if(it.key() == "Mod"){
                for (auto subit = components[it.key()].begin(); subit != components[it.key()].end(); ++subit) {
                    cout << "Sub key " << subit.key()<< endl;
                }
            } else if(it.key()=="FL" || it.key()=="SC" || it.key()=="GC" || it.key()=="DM" || it.key()=="SeC" || it.key()=="PSC" || it.key()=="PS" || it.key()=="FB") {
                for (auto subit = components[it.key()].begin(); subit != components[it.key()].end(); ++subit) {
                    cout << "Sub key " << subit.key()<< endl;
                }
            } else {
                cout << "Unknown key in components JSON file" << endl;
            }
        }
    }
}
