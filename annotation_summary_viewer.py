import cv2
import numpy as np
import sys
import json
import re
from pprint import pprint
import glob
import argparse
import os
from moviepy.editor import VideoFileClip
import math

waitTime=50
description=[]
def time_in_seconds_length(numbers):
    #print(numbers)
    seconds=int(numbers[5])+10*int(numbers[4])+60*int(numbers[3])+600*int(numbers[2])+3600*int(numbers[1])+36000*int(numbers[0])
    return seconds

def getLength(filename):
    clip = VideoFileClip(filename)
    return math.ceil(clip.duration)

def time_in_seconds_annotation(numbers):
    #print(numbers)
    seconds=int(numbers[4])+10*int(numbers[3])+60*int(numbers[2])+600*int(numbers[1])+3600*int(numbers[0])
    return seconds

def set_rating(length,data):
    rating=np.zeros(length)
    repetitive=np.zeros(length)

    for block in data:
        begins=block["begin"].replace(':','')
        end=block["end"].replace(':','')
        begin=time_in_seconds_annotation(block["begin"].replace(':',''))
        end=time_in_seconds_annotation(block["end"].replace(':',''))
        # print(block)
        rating[begin:end]=block["rating"]
        if "repetitive" in block:
            if block["repetitive"]==True:
                repetitive[begin:end]=1
    return rating,repetitive

def set_candidate(length,data):
    rating=np.zeros(length)
    for block in data:
        begin=block["begin"]
        end=block["end"]
        rating[begin:end]=1
    return rating

def candidate_array(length,data):
    candidate=np.zeros(length)
    blocks=data.split('{')

    for i in range(1,len(blocks)):

            begins=str(blocks[i]).split('"begin":"')
            ends=str(blocks[i]).split('"end":"')
            for j in range(0,len(begins)):
                if filter(str.isdigit, str(begins[j][0:7])):

                    begin=time_in_seconds_annotation(str(filter(str.isdigit, str(begins[j][0:7]))))
                    #print(str(ends[j][0:7]))
                    end=time_in_seconds_annotation(str(filter(str.isdigit, str(ends[j][0:7]))))
                    candidate[begin:end]=1
    return candidate

def encoding(rat,rep):
    if rat==0 and rep==0:
        return [0,0,200]
    elif rat==1 and rep==0:
        return [0,200,0]
    elif rat==2 and rep==0:
        return [200,0,0]
    elif rat==3 and rep==0:
        return [200,200,0]
    elif rat==-2 and rep==0:
        return [200,0,200]
    elif rat==-1 and rep==0:
        return [0,200,200]
    if rep==1:
        return encoding(rat,0)

def view_summary(dataset_json, ideal, repetitive, candidate, category, video_name, components, summaryFileName):
    originaldur = dataset_json["categories"][category][video_name]["duration"]
    cam=cv2.VideoCapture(dataset_json["root"]+category+"/"+dataset_json["categories"][category][video_name]["file_name"])
    fps = cam.get(cv2.CAP_PROP_FPS)
    print("Frames per second = " + str(fps))
    ret,img=cam.read()
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    height , width , layers =  img.shape
    out = cv2.VideoWriter(args.output_path+"/" +summaryFileName + "_annotation_summary.avi",fourcc, fps, (width+20,height+20))
    while(cam.isOpened()):
        ret,img=cam.read()
        if ret==True:
            sec = int(cam.get(cv2.CAP_PROP_POS_MSEC)/1000)
            borderedFrame = cv2.copyMakeBorder(img,10,10,10,10,cv2.BORDER_CONSTANT,value=encoding(ideal[sec],repetitive[sec]))
            font = cv2.FONT_HERSHEY_SIMPLEX
            bottomLeftCornerOfText = (10,50)
            fontScale              = 0.5
            fontColor              = (255,255,255)
            lineType               = 2
            if candidate[sec]==1:
                text = 'Rating:'+str(ideal[sec])+' Repetitive:'+str(repetitive[sec]==1)+' Time:'+str(sec) + ' IN SUMMARY'
            else:
                text = 'Rating:'+str(ideal[sec])+' Repetitive:'+str(repetitive[sec]==1)+' Time:'+str(sec)
            cv2.putText(borderedFrame,text,bottomLeftCornerOfText, font, fontScale,fontColor,lineType)
            height , width , layers =  borderedFrame.shape
            out.write(borderedFrame)
            #print ".",
            #if cv2.waitKey(1) & 0xFF==ord('q'):
             #   break
        else:
            break
    cam.release()
    out.release()
    print("** Verifying duration **")
    writtenDur = getLength(args.output_path+"/" +summaryFileName + "_annotation_summary.avi")
    print("Written duration = " + str(writtenDur))
    print("Expected duration = " + str(originaldur))

def view_annotation(dataset_json, category, video_name, summaries_path):
    length=10000
    videos=dataset_json["categories"][category]
    annotation_file=""
    annotation_file=dataset_json["root"]+category+"/"+dataset_json["categories"][category][video_name]["annotation"]
    print("Loading " + annotation_file)
    with open(annotation_file) as data_file:
        gt_json = json.loads(data_file.read())
    ideal,repetitive=set_rating(length,gt_json)
    originaldur = dataset_json["categories"][category][video_name]["duration"]

    if args.summaries_path!="gt":
        print("Generating annotated summary video")
        #it must be path to the folder containing all summaries
        #identify summaries that belong to this particular video
        summaryFiles = glob.glob(args.summaries_path + '**/*.json', recursive=True)
        for file in summaryFiles:
            fileName = os.path.basename(file)
            summaryName = os.path.splitext(fileName)[0]
            components = summaryName.split("_")
            summaryCategory = components[0]
            summaryVideo = components[1] + "_" + components[2]
            if summaryCategory == category and summaryVideo == video_name:
                #for each identified summary, get the sumary json file and call view_summary
                if os.path.exists(args.output_path+"/" +summaryName + "_annotation_summary.avi"):
                    print("Annotation summary video already exists, skipping. Still verifying the duration")
                    dur = getLength(args.output_path+"/" +summaryName + "_annotation_summary.avi")
                    print("***** Diff = " + str(originaldur - dur) + " ******")
                    continue
                with open(file) as data_file:
                    cand_json = json.loads(data_file.read())
                candidate=set_candidate(length + 1, cand_json["summary"])
                view_summary(dataset_json, ideal, repetitive, candidate, category, video_name, components, summaryName)
    else:
        if os.path.exists(args.output_path +"/"+category + "_" + video_name + "_ratings.avi"):
            print("Ratings video already exists, skipping. Still verifying duration")
            dur = getLength(args.output_path +"/"+category + "_" + video_name + "_ratings.avi")
            print("***** Diff = " + str(originaldur - dur) + " ******")
            return
        print("Opening " + dataset_json["root"]+category+"/"+dataset_json["categories"][category][video_name]["file_name"] + " for reading")
        cam=cv2.VideoCapture(dataset_json["root"]+category+"/"+dataset_json["categories"][category][video_name]["file_name"])
        fps = cam.get(cv2.CAP_PROP_FPS)
        print("Frames per second = " + str(fps))
        ret,img=cam.read()
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        height , width , layers =  img.shape
        out = cv2.VideoWriter(args.output_path +"/"+category + "_" + video_name + "_ratings.avi",fourcc, fps, (width+20,height+20))
        while(cam.isOpened()):
            ret,img=cam.read()
            if ret==True:
                sec = int(cam.get(cv2.CAP_PROP_POS_MSEC)/1000)
                borderedFrame = cv2.copyMakeBorder(img,10,10,10,10,cv2.BORDER_CONSTANT,value=encoding(ideal[sec],repetitive[sec]))
                font                   = cv2.FONT_HERSHEY_SIMPLEX
                bottomLeftCornerOfText = (10,50)
                fontScale              = 0.5
                fontColor              = (255,255,255)
                lineType               = 2

                text = 'Rating:'+str(ideal[sec])+' Repetitive:'+str(repetitive[sec]==1)+' Time:'+str(sec)
                cv2.putText(borderedFrame,text,bottomLeftCornerOfText, font, fontScale,fontColor,lineType)
                height , width , layers =  borderedFrame.shape
                out.write(borderedFrame)
                #print ".",
                #if cv2.waitKey(1) & 0xFF==ord('q'):
                 #   break
            else:
                break
        cam.release()
        out.release()
        print("** Verifying duration **")
        writtenDur = getLength(args.output_path +"/"+category + "_" + video_name + "_ratings.avi")
        print("Written duration = " + str(writtenDur))
        print("Expected duration = " + str(originaldur))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to view annotations and summaries in context of original videos")
    parser.add_argument('--dataset', required=True, help='''Full path to metadata JSON of the dataset''')
    parser.add_argument('--category', help='''Specify a particular category. If not specified, the script will run for all categories''')
    parser.add_argument('--video_name', help='''Specify a particular video for the specified category. Otherwise the script will run for all videos in the specified category. This argument makes sense only when category is specified''')
    parser.add_argument('--summaries_path', required=True, help='''Either path to the root folder containing all summaries or gt. When specified as gt, only annotations are overlayed on top of original video. When path is specified, corresponding summary informtion is also overlayed on top of original video''')
    parser.add_argument('--output_path', required=True, help='''Path of the output folder''')
    parser.add_argument('--mode', required=True, choices=['summary','annotation'], help='''summary will produce only summary video, annotation will produce annotated video''')

    args = parser.parse_args()
    dataset_json=json.load(open(args.dataset))

    if args.mode=='summary':
        print("Summary mode called")
        #iterate over every json in summaries path
        summaryFiles = glob.glob(args.summaries_path + '**/*.json', recursive=True)
        print(summaryFiles)
        for file in summaryFiles:
            summary_json=json.load(open(file))
            originaldur = len(summary_json["summary"]) * 2
            fileName = os.path.basename(file)
            summaryName = os.path.splitext(fileName)[0]
            print("Processing " + summaryName)
            if os.path.exists(args.output_path+"/" +summaryName + "_summary.avi"):
                print("Skipping, as summary video already exists. Still, verifying duration...")
                dur = getLength(args.output_path+"/" +summaryName + "_summary.avi")
                print("Duration of summary video in file system = " + str(dur))
                print("Duration of summary video in file system = " + str(originaldur))
                print("***** Diff = " + str(originaldur - dur) + " ******")
                continue
            components = summaryName.split("_")
            category=components[0]
            video_name=components[1] + "_" + components[2]
            #extract category name and video name from the summary json file name
            #from cateogry and video, get video file
            video_file=dataset_json["categories"][category][video_name]["file_name"]
            length=dataset_json["categories"][category][video_name]["duration"]
            #from summary json get candidate array
            # summary_json=json.load(open(file))
            candidate_array=set_candidate(length + 1,summary_json["summary"])
            #read frames of video and write only those which are in candidate, create output video correponding to candidate array - nme of the video should be same as name of the json
            cam=cv2.VideoCapture(dataset_json["root"]+category+"/"+video_file)
            fps = cam.get(cv2.CAP_PROP_FPS)
            print("Frames per second = " + str(fps))
            ret,img=cam.read()
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            height , width , layers =  img.shape
            out = cv2.VideoWriter(args.output_path+"/" +summaryName + "_summary.avi",fourcc, fps, (width,height))
            while(cam.isOpened()):
                ret,img=cam.read()
                if ret==True:
                    sec = int(cam.get(cv2.CAP_PROP_POS_MSEC)/1000)
                    if candidate_array[sec]==1:
                        out.write(img)
                    #print ".",
                    #if cv2.waitKey(1) & 0xFF==ord('q'):
                     #   break
                else:
                    break
            cam.release()
            out.release()
            print("** Verifying duration **")
            writtenDur = getLength(args.output_path+"/" +summaryName + "_summary.avi")
            print("Written duration = " + str(writtenDur))
            print("Expected duration = " + str(originaldur))
        exit()


    if args.category is None:
        #run for all categories and all videos as specified in the dataset metadata JSON
        for key1 in dataset_json["categories"]:
            if key1 != "name" and key1 != "num_categories" and key1 != "root":
                for key2 in dataset_json["categories"][key1]:
                    if key2 != "num_videos" and key2 != "total_duration":
                        print("Processing category " + key1 + " video " + key2)
                        view_annotation(dataset_json, key1, key2, args.summaries_path)
    else:
        if args.video_name is None:
            for key3 in dataset_json["categories"][args.category]:
                if key3 != "num_videos" and key3 != "total_duration":
                        print("Processing category " + args.category + " video " + key3)
                        view_annotation(dataset_json, args.category, key3, args.summaries_path)
        else:
            view_annotation(dataset_json, args.category, args.video_name, args.summaries_path)
