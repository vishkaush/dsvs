from __future__ import division
import sys
import os
import json
import glob
from scipy import stats
import io

#Pass argument as: <summary_file_path> <ground_truth_summary_path>

def calculate_f1_score(summary_path, gt_path):
    gt_list = []
    gt_root = []
    print "Collecting all GTs with id 0 .."
    for root, directories, filenames in os.walk(gt_path):
        for filename in filenames:
            print "Processing " + os.path.join(root,filename)
            l = os.path.basename(filename).split('_')
            #print l
            if(l[5] != "0.json"):
                print("Skipping since not 0")
                continue
            gt_root.append(root)
            gt_list.append(filename)
    #gt_list = os.listdir(gt_path)
    print "..done"
    for t, gt_summary in enumerate(gt_list):
        l = gt_summary.split('_')
        print l
        category = l[0]
        video = '_'.join(l[1:3])
        budget = l[4]
        print("Considering GT for category " + category + ", video " + video + ", budget " + budget + " ..")
        with open(gt_root[t] + '/' + gt_summary, 'r') as f:
            gt_json = json.load(f)

        file = category + '_' + video + '_*_' + budget + '*.json'
        f_names = glob.glob(summary_path + '/' + category + '/' + video + '/' + budget + '/' + file)

        for f_name in f_names:
            print("Computing F1 score for summary " + f_name)
            with open(f_name, 'r') as f:
                s_json = json.load(f)
            precision, recall = calculate_precision_recall(gt_json, s_json)
            if(precision > 0 and recall > 0):
                f1_s = f1_score(precision, recall)
            else:
                f1_s = 0
            s_json['f1_score'] = f1_s
            with io.open(f_name,'w',encoding="utf-8") as f:
                f.write(unicode(json.dumps(s_json, ensure_ascii=False)))

def calculate_precision_recall(gt_json, s_json):
    number_of_gt_segemnts_present = 0
    gt_summ = gt_json['summary']
    c_summ = s_json['summary']
    len_c_summ = len(c_summ)
    len_gt_summ = len(gt_summ)
    for each_begin_end in gt_summ:
        if each_begin_end in c_summ:
            number_of_gt_segemnts_present += 1
    precision = number_of_gt_segemnts_present/len_c_summ
    recall = number_of_gt_segemnts_present/len_gt_summ
    return precision, recall

def f1_score(pre, rec):
    return stats.hmean([pre,rec])


def main():
    if (len(sys.argv) != 3):
        print("------------------------------------------")
        print(" Pass argument as: <summary_file_path> <ground_truth_summary_path>")
        print("------------------------------------------")
        sys.exit(0)

    if (os.path.exists(sys.argv[1])):
        summary_path = sys.argv[1]
    else:
        print("------------------------------------------")
        print("Directory " + sys.argv[1] + " does not exists. Enter a valid directory.")
        print("------------------------------------------")
        sys.exit(0)

    if (os.path.exists(sys.argv[2])):
        gt_path = sys.argv[2]
    else:
        print("------------------------------------------")
        print("Directory " + sys.argv[2] + " does not exists. Enter a valid directory.")
        print("------------------------------------------")
        sys.exit(0)
    calculate_f1_score(summary_path, gt_path)

if __name__ == '__main__':
    main()
