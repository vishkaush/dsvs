/*
	Copyright (C) Rishabh Iyer
 *	Header file for defining a disparity sum set function: f(X) = \sum_{i, j \in X} (1 - s_{ij})
	Assumes that the kernel S is symmetric.
	Author: Rishabh Iyer
 *
 */

#include <algorithm>
#include <iostream>
#include "../../utils/error.h"
#include "../../utils/totalOrder.h"
using namespace std;

#include "PairwiseSum.h"


namespace datk {

PairwiseSum::PairwiseSum(int n, vector<vector <float> > kernel) : SetFunctions(n), kernel(kernel){
	sizepreCompute = n;
	preComputeVal = 0;
}

PairwiseSum::PairwiseSum(const PairwiseSum& f): SetFunctions(f.n), kernel(f.kernel){
	sizepreCompute = n;
	preComputeVal = 0;
}

PairwiseSum & PairwiseSum::operator=(const PairwiseSum & f){
    return *this;
}

PairwiseSum * PairwiseSum::clone() const{
    return new PairwiseSum(*this);
}

PairwiseSum::~PairwiseSum(){}

double PairwiseSum::eval(const Set& sset) const{
// Evaluation of function valuation.
	double sum = 0;
	for( Set::const_iterator it = sset.begin(); it != sset.end(); it++ ) {
		for( Set::const_iterator it2 = sset.begin(); it2!= sset.end(); it2++){
			sum+=kernel[*it][*it2];
		}
	}
    	return sum;
}

double PairwiseSum::evalFast(const Set& sset) const{
	return preComputeVal;
}

double PairwiseSum::evalGainsadd(const Set& sset, int item) const{
	if(sset.contains(item)){
		cout<<"Warning: the provided item belongs to the subset\n";
		return 0;
	}
	double gains = 0;
	Set::const_iterator it;
	for (it = sset.begin(); it != sset.end(); it++){
		gains+= (kernel[item][*it] + kernel[*it][item]);
	}
	gains += (kernel[item][item]);
	return gains;
}

double PairwiseSum::evalGainsremove(const Set& sset, int item) const{
	if(!sset.contains(item)){
		cout<<"Warning: the provided item does not belong to the subset\n";
		return 0;
	}
	double gains = 0;
	Set::const_iterator it;
	for (it = sset.begin(); it != sset.end(); it++){
		if(*it != item)
			gains += (kernel[item][*it] + kernel[*it][item]);
	}
	gains+= (kernel[item][item]);
	return gains;
}

double PairwiseSum::evalGainsaddFast(const Set& sset, int item) const{
// Fast evaluation of Adding gains using the precomputed statistics. This is used, for example, in the implementation of the forward greedy algorithm.
// For the sake of speed, we do not check if item does not belong to the subset. You should check this before calling this function.
	double gains = 0;
	Set::const_iterator it;
	for (it = sset.begin(); it != sset.end(); it++){
		gains+= (kernel[item][*it] + kernel[*it][item]);
	}
	gains += (kernel[item][item]);
	return gains;
}

double PairwiseSum::evalGainsremoveFast(const Set& sset, int item) const{
// Fast evaluation of Removing gains using the precomputed statistics. This is used, for example, in the implementation of the reverse greedy algorithm.
// For the sake of speed, we do not check if item belong to the subset. You should check this before calling this function.
	double gains = 0;
	Set::const_iterator it;
	for (it = sset.begin(); it != sset.end(); it++){
		if(*it != item)
			gains += (kernel[item][*it] + kernel[*it][item]);
	}
	gains+= (kernel[item][item]);
	return gains;
}

void PairwiseSum::updateStatisticsAdd(const Set& sset, int item) const{
	preComputeVal+=evalGainsaddFast(sset, item);
}

void PairwiseSum::updateStatisticsRemove(const Set& sset, int item) const{
	preComputeVal-=evalGainsaddFast(sset, item);
}

void PairwiseSum::setpreCompute(const Set& sset) const{
	preComputeVal = eval(sset);
}

void PairwiseSum::clearpreCompute() const{
	preComputeVal = 0;
}

void PairwiseSum::resetData(std::vector<int> Rset){}

}
