// Copyright (C) Rishabh Iyer 2015

#ifndef DISC_FUNCTIONS
#define DISC_FUNCTIONS

#include "SetFunctions.h"
#include "SubmodularFunctions.h"
#include "FeatureBasedFunctions.h"
#include "FeatureBasedFunctionsDense.h"
#include "FacilityLocation.h"
#include "SaturateCoverage.h"
#include "GraphCutFunctions.h"
#include "FacilityLocationSparse.h"
#include "DisparitySum.h"
#include "DisparityMin.h"
#include "MMR.h"
#include "SetCover.h"
#include "ProbabilisticSetCover.h"
#include "PairwiseSum.h"
#include "VideoSummLoss.h"

/*#include "FacilityLocationSparseDB.h"
#include "FeatureBasedFunctionsTwoLayerDense.h"
#include "ModularFunctions.h"
#include "BipartiteNeighborhoodFunctions.h"
#include "ComplementSubmodularFunctions.h"
#include "IntersectSubmodularFunctions.h"
#include "AddSubmodularFunctions.h"
#include "CombineSubmodularFunctions.h"
#include "ScaleSubmodularFunctions.h"
#include "DifferenceSubmodularFunctions.h"
#include "ConcaveSubmodularFunctions.h"
#include "OperationsSubmodularFunctions.h"
#include "DifferenceSubmodularModularFunctions.h"
#include "FeatureBasedFunctionsDB.h"*/

#endif
