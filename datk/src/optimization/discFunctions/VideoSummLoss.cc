/*
	Copyright (C) Rishabh Iyer
 *	Header file for defining a disparity sum set function: f(X) = \sum_{i, j \in X} (1 - s_{ij})
	Assumes that the kernel S is symmetric.
	Author: Rishabh Iyer
 *
 */

#include <algorithm>
#include <cassert>
#include <iostream>
#include "../../utils/error.h"
#include "../../utils/totalOrder.h"
using namespace std;

#include "VideoSummLoss.h"

namespace datk {

int setInterSection(const Set& A, const Set&B) {
	Set::const_iterator it;
	int length = 0;
	for (it = A.begin(); it != A.end(); it++){
		if (B.contains(*it)) {
			length++;
		}
	}
	return length * 2; //for number of seconds instead of number of snippets
}

double min(double a, double b) {
	if (a < b) {
		return a;
	}
	else {
		return b;
	}
}
VideoSummLoss::VideoSummLoss(int n, std::vector<Set> Segments, std::vector<int> ratings, std::vector<bool> isRedundunt, double alpha, double beta, double penalty, double max_score):
SetFunctions(n), Segments(Segments), ratings(ratings), isRedundunt(isRedundunt), alpha(alpha), beta(beta), penalty(penalty), max_score(max_score) {
	preCompute = std::vector<double>(Segments.size(), 0);
    assert(penalty > 0);
}

VideoSummLoss::VideoSummLoss(const VideoSummLoss& f): SetFunctions(f.n), Segments(f.Segments), ratings(f.ratings), isRedundunt(f.isRedundunt), alpha(f.alpha), beta(f.beta), penalty(f.penalty), max_score(f.max_score) {
	preCompute = std::vector<double>(Segments.size(), 0);
    assert(penalty > 0);
}

VideoSummLoss & VideoSummLoss::operator=(const VideoSummLoss & f){
    return *this;
}

VideoSummLoss * VideoSummLoss::clone() const{
    return new VideoSummLoss(*this);
}

VideoSummLoss::~VideoSummLoss(){}

double _videoSumLossReward(double rating, double alpha, double penalty) {
    return (rating >= 0) ? exp(alpha * rating) : -1.0 * penalty;
}

double _intersectionFactor(double intersection, double segSize, double rating) {
    if(rating >= 0) {
        return intersection * ( 1 + intersection / segSize);
    } else {
        return intersection;
    }
}

double VideoSummLoss::eval(const Set& sset) const{
// Evaluation of function valuation.
	double sum = 0;
	for (int i = 0; i < Segments.size(); i++) {
        double intersection = setInterSection(sset, Segments[i]);
        double segSize = Segments[i].size() * 2;
        if(segSize <= 0) continue;
	    if ((ratings[i] >= 0) && isRedundunt[i]){
			intersection = min(intersection, beta);
            segSize = min(segSize, beta);
		}
        sum += _intersectionFactor(intersection, segSize, ratings[i]) * _videoSumLossReward(ratings[i], alpha, penalty);
	}
    assert(max_score >= sum);
    return max_score - sum;
}

double VideoSummLoss::evalFast(const Set& sset) const{
	// Evaluation of function valuation.
		double sum = 0;
		for (int i = 0; i < Segments.size(); i++) {
            double intersection = preCompute[i];
            double segSize = Segments[i].size() * 2;
            if(segSize <= 0) continue;
            if ((ratings[i] >= 0) && isRedundunt[i]){
                intersection = min(intersection, beta);
                segSize = min(segSize, beta);
            }
            sum += _intersectionFactor(intersection, segSize, ratings[i]) * _videoSumLossReward(ratings[i], alpha, penalty);

		}
        assert(max_score >= sum);
	    return max_score - sum;
}

double VideoSummLoss::evalGainsadd(const Set& sset, int item) const{
	// Evaluation of function valuation.
		double value = 0;
		for (int i = 0; i < Segments.size(); i++) {
			if (Segments[i].contains(item)) {
                double intersection_with = setInterSection(sset, Segments[i]) + 2;
                double intersection_without = setInterSection(sset, Segments[i]);
                double segSize = Segments[i].size() * 2;
                if(segSize <= 0) continue;
                if ((ratings[i] >= 0) && isRedundunt[i]){
                    intersection_with = min(intersection_with, beta);
                    intersection_without = min(intersection_without, beta);
                    segSize = min(segSize, beta);
                }
                value += (
                    _intersectionFactor(intersection_with, segSize, ratings[i])
                    - _intersectionFactor(intersection_without, segSize, ratings[i])
                ) * _videoSumLossReward(ratings[i], alpha, penalty);
                break;

			}
		}
		return -1 * value;
}

double VideoSummLoss::evalGainsremove(const Set& sset, int item) const{}

double VideoSummLoss::evalGainsaddFast(const Set& sset, int item) const{
	// Evaluation of function valuation.
		double value = 0;
		for (int i = 0; i < Segments.size(); i++) {
			if (Segments[i].contains(item)) {
                double intersection_with = preCompute[i] + 2;
                double intersection_without = preCompute[i];
                double segSize = Segments[i].size() * 2;
                if(segSize <= 0) continue;
                if ((ratings[i] >= 0) && isRedundunt[i]){
                    intersection_with = min(intersection_with, beta);
                    intersection_without = min(intersection_without, beta);
                    segSize = min(segSize, beta);
                }
                value += (
                    _intersectionFactor(intersection_with, segSize, ratings[i])
                    - _intersectionFactor(intersection_without, segSize, ratings[i])
                ) * _videoSumLossReward(ratings[i], alpha, penalty);
                break;

			}
		}
		return -1 * value;
}

double VideoSummLoss::evalGainsremoveFast(const Set& sset, int item) const{
}

void VideoSummLoss::updateStatisticsAdd(const Set& sset, int item) const{
	double value = 0;
	for (int i = 0; i < Segments.size(); i++) {
		if (Segments[i].contains(item)) {
			preCompute[i] = preCompute[i] + 2;
            break;
		}
	}
}

void VideoSummLoss::updateStatisticsRemove(const Set& sset, int item) const{}

void VideoSummLoss::setpreCompute(const Set& sset) const{
	for (int i = 0; i < Segments.size(); i++) {
			preCompute[i] = setInterSection(sset, Segments[i]);
	}
}

void VideoSummLoss::clearpreCompute() const{
	for (int i = 0; i < Segments.size(); i++) {
			preCompute[i] = 0;
	}
}

void VideoSummLoss::resetData(std::vector<int> Rset){}

}
