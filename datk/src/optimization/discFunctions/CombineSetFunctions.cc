/*
    Copyright (C) Rishabh Iyer 2015

 *	Combining two submodular functions h(X) = \sum_{i = 1}^k \lambda_i f_i(X), for two submodular functions f and g.
	Melodi Lab, University of Washington, Seattle
 *
 */

#include <iostream>
#include <cmath>
#include <assert.h>
using namespace std;

#include "CombineSetFunctions.h"

namespace datk {
	CombineSetFunctions::CombineSetFunctions(int n, vector<SetFunctions*> fvec_in, vector<double>& lambda): SetFunctions(n), lambda(lambda){
		//cout << "Constructor called" << endl;
        assert(lambda.size() == fvec_in.size());
		fvec = vector<SetFunctions*>();
        //cout << "Lambda size = " << lambda.size() << endl;
        //cout << "Fvec size = " << fvec_in.size();
		for (int i = 0; i < fvec_in.size(); i++)
		{
			fvec.push_back(fvec_in[i]->clone());
		}

        for (int i = 0; i < lambda.size(); i++) {
            assert(!isnan(lambda[i]));
            assert(!isinf(lambda[i]));
        }
	}
	CombineSetFunctions::CombineSetFunctions(const CombineSetFunctions &c): SetFunctions(c), lambda(c.lambda){
		fvec = vector<SetFunctions*>();
		for (int i = 0; i < c.fvec.size(); i++)
		{
			fvec.push_back(c.fvec[i]->clone());
		}

        for (int i = 0; i < lambda.size(); i++) {
            assert(!isnan(lambda[i]));
            assert(!isinf(lambda[i]));
        }
	}

    CombineSetFunctions* CombineSetFunctions::clone() const{
    	return new CombineSetFunctions(*this);
    }

    void CombineSetFunctions::resetData(vector<int> Rset){}

	CombineSetFunctions::~CombineSetFunctions(){
		for (int i =0; i< fvec.size();i++){
		     delete (fvec[i]);
		}
		fvec.clear();
	}

	double CombineSetFunctions::eval(const Set& sset) const{
		double sumtot = 0;
		for (int i = 0; i < fvec.size(); i++){
			sumtot += lambda[i] * fvec[i]->eval(sset);
		}
		return sumtot;
	}

	double CombineSetFunctions::evalFast(const Set& sset) const{
		double sumtot = 0;
		for (int i = 0; i < fvec.size(); i++){
			sumtot += lambda[i]*fvec[i]->evalFast(sset);
		}
		return sumtot;
	}

	double CombineSetFunctions::evalGainsadd(const Set& sset, int item) const{
		double gainsadd = 0;
		for (int i = 0; i < fvec.size(); i++){
			gainsadd += lambda[i]*fvec[i]->evalGainsadd(sset, item);
		}
		return gainsadd;
	}

	double CombineSetFunctions::evalGainsremove(const Set& sset, int item) const{
		double gainsremove = 0;
		for (int i = 0; i < fvec.size(); i++){
			gainsremove += lambda[i]*fvec[i]->evalGainsremove(sset, item);
		}
		return gainsremove;
	}

	double CombineSetFunctions::evalGainsaddFast(const Set& sset, int item) const{
		double gainsadd = 0;
		for (int i = 0; i < fvec.size(); i++){
			gainsadd += lambda[i]*fvec[i]->evalGainsaddFast(sset, item);
		}
		return gainsadd;
	}

	double CombineSetFunctions::evalGainsremoveFast(const Set& sset, int item) const{
		double gainsadd = 0;
		for (int i = 0; i < fvec.size(); i++){
			gainsadd += lambda[i]*fvec[i]->evalGainsremoveFast(sset, item);
		}
		return gainsadd;
	}

	void CombineSetFunctions::updateStatisticsAdd(const Set& sset, int item) const{
		for (int i = 0; i < fvec.size(); i++){
			fvec[i]->updateStatisticsAdd(sset, item);
		}
	}

	void CombineSetFunctions::updateStatisticsRemove(const Set& sset, int item) const{
		for (int i = 0; i < fvec.size(); i++){
			fvec[i]->updateStatisticsRemove(sset, item);
		}
	}

	void CombineSetFunctions::clearpreCompute() const{
		for (int i = 0; i < fvec.size(); i++){
			fvec[i]->clearpreCompute();
		}
	}

	void CombineSetFunctions::setpreCompute(const Set& sset) const{
		for (int i = 0; i < fvec.size(); i++){
			fvec[i]->setpreCompute(sset);
		}
	}
}
