/*
 *	Scaling a submodular function h(X) = \lambda f(X) -- f is submodular and lambda > 0.
	Melodi Lab, University of Washington, Seattle
 *
 */
#include <vector>
#include <cassert>
#include "ScaleSetFunctions.h"
#include "../discFunctions/VideoSummLoss.h"
#include <iostream>

using namespace std;

namespace datk {
ScaleSetFunctions::ScaleSetFunctions(const SetFunctions* in_f,  const double lambda):
SetFunctions(in_f->size()), lambda(lambda){
	assert(lambda > 0);
    assert(!isnan(lambda));
    assert(!isinf(lambda));
    //if(isinf(lambda)) {
    //    cout<<"WARN: inf lambda found"<<endl;
    //    this->lambda = 1;
    //}

    //if(lambda <= 0) {
    //    std::cout << "\n*** Found leq 0 lambda. CROSS CHECK. OK for VSL ***\n" << std::endl;
    //    if(dynamic_cast<const VideoSummLoss*>(in_f) == nullptr) {
    //        std::cout<<"Not VSL."<<std::endl;
    //        assert(lambda > 0);
    //    } else {
    //        std::cout<<"VSL verified."<<std::endl;
    //    }
    //}
	f = in_f->clone();
}

ScaleSetFunctions::ScaleSetFunctions(const ScaleSetFunctions& s):
SetFunctions(s), lambda(s.lambda){
	assert(lambda > 0);
    assert(!isnan(lambda));
    assert(!isinf(lambda));
    //if(isinf(lambda)) {
    //    cout<<"WARN: inf lambda found"<<endl;
    //    this->lambda = 1;
    //}
	f = s.f->clone();
}

ScaleSetFunctions::~ScaleSetFunctions(){
	delete f;
}

ScaleSetFunctions* ScaleSetFunctions::clone() const{
	return new ScaleSetFunctions(*this);
}

void ScaleSetFunctions::resetData(vector<int> Rset){}

double ScaleSetFunctions::eval(const Set& sset) const{
	return lambda*f->eval(sset);
}

double ScaleSetFunctions::evalFast(const Set& sset) const{
	return lambda*f->evalFast(sset);
}

double ScaleSetFunctions::evalGainsadd(const Set& sset, int item) const{
	return lambda*f->evalGainsadd(sset, item);
}

double ScaleSetFunctions::evalGainsremove(const Set& sset, int item) const{
	return lambda*f->evalGainsremove(sset, item);
}

double ScaleSetFunctions::evalGainsaddFast(const Set& sset, int item) const{
	return lambda*f->evalGainsaddFast(sset, item);
}

double ScaleSetFunctions::evalGainsremoveFast(const Set& sset, int item) const{
	return lambda*f->evalGainsremoveFast(sset, item);
}

void ScaleSetFunctions::updateStatisticsAdd(const Set& sset, int item) const{
	f->updateStatisticsAdd(sset, item);
}

void ScaleSetFunctions::updateStatisticsRemove(const Set& sset, int item) const{
	f->updateStatisticsRemove(sset, item);
}

void ScaleSetFunctions::clearpreCompute() const{
	f->clearpreCompute();
}

void ScaleSetFunctions::setpreCompute(const Set& sset) const{
	f->setpreCompute(sset);
}
}
