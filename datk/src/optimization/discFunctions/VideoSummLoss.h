/*
	Copyright (C) Rishabh Iyer 2015
  Header file for defining a disparity sum set function: f(X) = \sum_{i, j \in X} (1 - s_{ij})
	Assumes that the kernel S is symmetric.
	Note: This function is *not* submodular!
	Author: Rishabh Iyer
 *
 */

#ifndef VIDSUMMLOSS_SF
#define VIDSUMMLOSS_SF

#include <vector>
#include "SetFunctions.h"

#include "../../representation/Set.h"

namespace datk {
class VideoSummLoss: public SetFunctions{
protected:
	std::vector<Set> Segments; // The matrix s_{ij}_{i \in V, j \in V} of similarity (the assumption is it is normalized between 0 and 1 -- max similarity is 1 and min similarity is 0)
	std::vector<int> ratings;
	std::vector<bool> isRedundunt;
	mutable std::vector<double> preCompute;
	const double alpha;
	const double beta;
	const double penalty;
	const double max_score;
	// Functions
public:
	VideoSummLoss(int n, std::vector<Set> Segments, std::vector<int> ratings, std::vector<bool> isRedundunt, double alpha, double beta, double penalty, double max_score);
  VideoSummLoss(const VideoSummLoss& f);
  VideoSummLoss & operator=(const VideoSummLoss & f);
  VideoSummLoss * clone() const;
	~VideoSummLoss();
	double eval(const Set& sset) const;
	double evalFast(const Set& sset) const;
	double evalGainsadd(const Set& sset, int item) const;
	double evalGainsremove(const Set& sset, int item) const;
	double evalGainsaddFast(const Set& sset, int item) const;
	double evalGainsremoveFast(const Set& sset, int item) const;
	void updateStatisticsAdd(const Set& sset, int item) const;
	void updateStatisticsRemove(const Set& sset, int item) const;
	void setpreCompute(const Set& sset) const;
	void clearpreCompute() const;

	void resetData(std::vector<int> Rset);
};
}
#endif
