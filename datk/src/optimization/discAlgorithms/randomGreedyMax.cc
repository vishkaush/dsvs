/*
	Copyright (C) Rishabh Iyer 2015

 *	The naive greedy algorithm for set function maximization. This algorithm has guarantees if the function is monotone submodular, under cardinality/knapsack or matroid constraints. In general, though, it is a useful heuristic, which seems to work well in practice.
	Solves the problem \max_{|X| \leq B} f(X), where f is a monotone submodular function.
	Author: Rishabh Iyer
	Melodi Lab, University of Washington, Seattle

	Note: This algorithm is considerably slower than the accelerated greedy, so if you have a reason to believe your function is submodular (or approximately so), 		please use the lazy greedy algorithm.
 *
 */

#include <vector>
#include <algorithm>
#include <cassert>
#include <limits>
#include <iostream>
using namespace std;

#include "../../utils/error.h"
#include  "randomGreedyMax.h"


namespace datk {

struct ValIdPair {
    double val;
    int id;

    ValIdPair() {
        id = -1;
		val = -1 * std::numeric_limits<double>::max();
    }

    ValIdPair(int _id, double _val) {
        id = _id;
        val = _val;
    }

};

bool compValIdPair(const ValIdPair& a, const ValIdPair& b) {
    return a.val > b.val;
}


void randomGreedyMax(SetFunctions& f, double budget, Set& greedySet, int verbosity, bool precomputeSet,
bool isEquality, Set groundSet, bool grSetn)
{
	if (grSetn){
		groundSet = Set(f.size(), true); // GroundSet constructor
	}
    assert(budget <= f.size());
	if(verbosity > 0) cout<<"Starting the random greedy algorithm\n"<<flush;
	double currentCost;
	int iter = 0;
    int k = (int)budget;
	if (!precomputeSet){
		f.setpreCompute(greedySet); // clear the precomputed statistics, in case it is already not cleared, and set it to the greedySet.
	}
	// naive greedy algorithm implementation
	Set::iterator it;
	while (1) {
		iter++;
        std::vector<ValIdPair> currvalbest(k);
        std::make_heap(currvalbest.begin(), currvalbest.end(), compValIdPair);
		for( it = groundSet.begin(); it != groundSet.end(); ++it ) {
			// cout << "Considering " << *it << " to add next" << endl;
			if( !greedySet.contains(*it) ){
                double it_val = f.evalGainsaddFast(greedySet, *it);
                auto heapVal = currvalbest.front();
                if(it_val > heapVal.val) {
                    std::pop_heap(currvalbest.begin(), currvalbest.end(), compValIdPair);
                    currvalbest.pop_back();
                    currvalbest.push_back(ValIdPair(*it, it_val));
                    std::push_heap(currvalbest.begin(), currvalbest.end(), compValIdPair);
                    if(verbosity > 0) cout<<"From heap: removed "<<heapVal.val<<" in favour of "<<it_val<<endl;
                } else {
                    if(verbosity > 0) cout<<"In heap: kept "<<heapVal.val<<" in favour of "<<it_val<<endl;
                }
				// cout << "Element " << curridbest << "is new best and gain = " << currvalbest << endl;
			} else {
				// cout << "Either it is already picked up or its value addition is less than currbest = " << currvalbest << endl;
			}
		}
		// cout << "One pass over all elements done" << endl;
        int randomBestInd = rand() % k;
        assert(currvalbest[randomBestInd].id >= 0);
		if (verbosity > 0) cout<<"Current best value is "<<currvalbest[randomBestInd].val<<"\n"<<flush;
		if(currentCost + 1 <= budget){
            double prevCost = 0;
			if( (currvalbest[randomBestInd].val < 0) && (!isEquality))
				break;
            if(verbosity > 0) {
                prevCost = f(greedySet);
            }

			f.updateStatisticsAdd(greedySet, currvalbest[randomBestInd].id);
			currentCost+=1;
			greedySet.insert(currvalbest[randomBestInd].id);
			if(verbosity > 0) {
                double newCost = f(greedySet);
                cout<<"Delta score: "<<newCost - prevCost<<"."<<endl;
                cout<<"Adding element "<< currvalbest[randomBestInd].id<<" in iteration "<<iter<<" and the objective value is "<<f(greedySet)<<"\n"<<flush;
            }
		}
		else{
			break;
		}

	}
}

}
