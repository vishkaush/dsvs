----------------- README ------------------------

DATK: A toolkit with API support for Data Analytics

(Featuring algorithms for classification, regression, ranking, recommendation and summarization)

Authors: Rishabh Iyer, John Halloran and Kai Wei
Copyright 2015

Features Supported

1) Convex Function API
2) Convex Optimization Algorithms API
3) Set Function API
4) Discrete Optimization Algorithms API
5) ML Classification API -- Logistic Regression, SVMs (linear and kernel), Decision trees, Boosting
6) ML Regression API -- Linear Regression, SVRs (linear and kernel)
7) ML Ranking API 
7) Summarization API (Image Summarization, Text Summarization, Video Summarization)
8) Recommendation System API (Matrix Factorization, Item-Similarity models)
9) Feature Extraction API (Image deep feature extractor, text feature extractors)
10) Graphical Models API (CRFs, HMMs)

 

