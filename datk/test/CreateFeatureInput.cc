#include <iostream>
#include <fstream>
#include <stdlib.h> 
#include <time.h>

using namespace std;

int main(int argc, char* argv[]) {
    if(argc < 2) {
        cout << "Usage: ./CreateFeatureInput <output_file_path>" << endl;
        return 0;
    }
    srand (time(NULL));
    ofstream myfile;
    std::string str(argv[1]);
    myfile.open (str + "/groundSetFeature.txt");
    for(int i=0; i<50; i++) {
        for(int j=0; j<30; j++) {
            myfile << (float)(rand()%10)/5 << " ";
        } 
        myfile << "\n";
    }
    myfile.close();
    return 0;    
}
