#include "time.h"
#include <fstream>
#include <vector>
#include "../src/datk.h"
#include "../src/optimization/discFunctions/SetFunctions.h"
#include "../src/optimization/discFunctions/ScaleSetFunctions.h"
#include "../src/optimization/discFunctions/CombineSetFunctions.h"
#include <sstream>
#include <string>
#include "../src/utils/json.hpp"

int equals(double a, double b) {
    if(abs(a - b) < 0.001) return 1;
    else  return 0;
}

void computeKernel(char* inputFile, datk::Set& groundSet, int& groundSetSize, std::vector<std::vector<float>>& kernel) {
    std::vector<double> features;
    std::ifstream infile(inputFile);
    int min = std::numeric_limits<int>::max();
    int max = std::numeric_limits<int>::min();
    int a;
    std::vector<int> dataSet;
    groundSetSize = 0;
    cout << "Preparing data set of items" << endl;
    while(infile >> a) {
        dataSet.push_back(a);
        groundSet.insert(groundSetSize);
        groundSetSize++;
        if(a < min) min = a;
        if(a > max) max = a;
    }
    //std::cout << "Data set:" << endl;
    //for (auto i: dataSet)
    //    std::cout << i << ' ';
    //cout << endl;
    double elem;
    cout << "Preparing features" << endl;
    for(auto i: dataSet) {
        elem = i;
        elem = (elem - min)/(max - min);
        features.push_back(elem);
    }
    datk::Set::const_iterator it;
    //cout << "Ground set: " << endl;
    //for( it = groundSet.begin(); it != groundSet.end(); ++it) {
    //    cout << *it << " ";
    //}
    //cout << endl;
    //std::cout << "Features vector:" << endl;
    //for (auto i: features)
    //    std::cout << i << ' ';
    //cout << endl;

    cout << "Computing kernel" << endl;
    double sim = 0;
    double mind = std::numeric_limits<double>::max();
    double maxd = std::numeric_limits<double>::min();
    for (int i = 0; i < features.size(); i++) {
        std::vector<float> currvector;
        for (int j = 0; j < features.size(); j++) {
            sim = 1 - abs(features.at(i) - features.at(j));
            if (sim < mind) mind = sim;
            if (sim > maxd) maxd = sim;
            currvector.push_back(sim);
        }
        kernel.push_back(currvector);
    }
    //0-1 normalize using min and max - this becomes the kernel
    double range = maxd - mind;
    for (int i = 0; i < features.size(); i++) {
        for (int j = 0; j < features.size(); j++) {
            kernel[i][j] = (kernel[i][j] - mind) / range;
        }
    }
    return;
}

void runTest(datk::SetFunctions& f, int budget, int groundSetSize, datk::Set groundSet) {
    //call lazygreedymax with this facility location fuction
    /*datk::Set indices;
      datk::lazyGreedyMax((datk::SubmodularFunctions&)f, budget, indices);
      std::cout << "Selected " << indices.size() << " elements out of a total of " << groundSetSize << std::endl;
      for (auto i: indices)
      std::cout << "Index: " << i << ", Element: " << groundSet.at(i) << endl;
     */
    //testing of equivalence between functions
    cout << "************* Starting Tests ****************" << endl;
    cout << "Eval on groundSet = " << f.eval(groundSet) << endl;
    datk::Set subSet;
    subSet.insert(1);
    subSet.insert(10);
    subSet.insert(13);
    cout << "Subset has 1, 10 and 13" << endl;
    double simpleVal;
    simpleVal = f.eval(subSet);
    cout << "Subset score from eval = " << simpleVal << endl;
    double fastVal;
    cout << "Precompute" << endl;
    f.setpreCompute(subSet);
    fastVal = f.evalFast(subSet);
    cout << "Subset score from evalFast = " << fastVal << endl;
    double oldVal;
    if(equals(simpleVal, fastVal)) {
        cout << "\n*************PASSED**********\n" << endl;
        oldVal = simpleVal;
    } else
        cout << "\n*************FAILED**********\n" << endl;
    double naiveMargin;
    cout << "Adding 15 to subset" << endl;
    subSet.insert(15);
    naiveMargin = f.eval(subSet) - simpleVal;
    cout << "Naive marginal gain of adding 15 = " << naiveMargin << endl;
    cout << "Removing 15 from subset" << endl;
    subSet.remove(15);
    double simpleMargin;
    simpleMargin = f.evalGainsadd(subSet, 15);
    cout << "Simple marginal gain of adding 15 = " << simpleMargin << endl;
    double fastMargin;
    fastMargin = f.evalGainsaddFast(subSet, 15);
    cout << "Fast marginal gain of adding 15 = " << fastMargin << endl;
    double marginalGain;
    if(equals(naiveMargin, simpleMargin) && equals(simpleMargin, fastMargin)) {
        cout << "\n*************PASSED**********\n" << endl;
        marginalGain = naiveMargin;
    } else
        cout << "\n************FAILED**********\n" << endl;
    cout << "Updating precompute for adding 15" << endl;
    f.updateStatisticsAdd(subSet, 15);
    cout << "Adding 15 to the subset" << endl;
    subSet.insert(15);
    simpleVal = f.eval(subSet);
    cout << "New subset score from eval = " << simpleVal << endl;
    fastVal = f.evalFast(subSet);
    cout << "New subset score from evalFast = " << fastVal << endl;
    double newVal;
    if(equals(simpleVal, fastVal)){
        cout << "\n*************PASSED**********\n" << endl;
        newVal = simpleVal;
    } else
        cout << "\n*************FAILED**********\n" << endl;
    cout << "Verifying whether newVal = oldVal + marginalGain or not" << endl;
    if(newVal == oldVal + marginalGain) {
        cout << "\n*************PASSED**********\n" << endl;
    } else
        cout << "\n*************FAILED**********\n" << endl;
    cout << "Adding 18 to subset" << endl;
    subSet.insert(18);
    naiveMargin = f.eval(subSet) - simpleVal;
    cout << "Naive marginal gain of adding 18 = " << naiveMargin << endl;
    cout << "Removing 18 from subset" << endl;
    subSet.remove(18);
    simpleMargin = f.evalGainsadd(subSet, 18);
    cout << "Simple marginal gain for adding 18 = " << simpleMargin << endl;
    fastMargin = f.evalGainsaddFast(subSet, 18);
    cout << "Fast marginal gain of adding 18 = " << fastMargin << endl;
    if(equals(naiveMargin, simpleMargin) && equals(simpleMargin, fastMargin))
        cout << "\n*************PASSED**********\n" << endl;
    else
        cout << "\n*************FAILED**********\n" << endl;
    return;
}

void computeProbabilityConcepts(char* inputFile, datk::Set& groundSet, int& groundSetSize, int& numConcepts, std::vector<std::vector<double>>& probVectors) {
    std::ifstream infile(inputFile);
    cout << "Processing input" << endl;
    groundSetSize = 0;
    for(std::string line; getline(infile, line);) {
        std::vector<double> currvector;
        numConcepts = 0;
        std::stringstream iss(line);
        double a;
        while(iss >> a) {
            currvector.push_back(a);
            numConcepts++;
        }
        cout << "Num concepts = " << numConcepts << endl;
        probVectors.push_back(currvector);
        for (auto i: currvector)
            std::cout << i << ' ';
        std::cout << endl;
        groundSet.insert(groundSetSize);
        groundSetSize++;
    }
    //cout << "Ground set: " << endl;
    //for (auto i: groundSet)
    //    std::cout << i << ' ';
    //std::cout << endl;
}

void computeConcepts(char* inputFile, int& groundSetSize, std::vector<datk::Set>& conceptVectors, datk::Set& groundSet) {
    std::ifstream infile(inputFile);
    cout << "Processing input" << endl;
    groundSetSize = 0;
    for(std::string line; getline(infile, line);) {
        cout << groundSetSize << ": ";
        datk::Set currset;
        std::stringstream iss(line);
        double a;
        int j = 0;
        while(iss >> a) {
            if(a > 0.1) currset.insert(j);
            j++;
        }
        conceptVectors.push_back(currset);
        for (auto i: currset)
            std::cout << i << ' ';
        std::cout << endl;
        groundSet.insert(groundSetSize);
        groundSetSize++;
    }
    //cout << "Ground set: " << endl;
    //for (auto i: groundSet)
    //    std::cout << i << ' ';
    //std::cout << endl;

}

void computeSparseFeatures(char* inputFile, int& groundSetSize, vector<datk::SparseFeature>& features, datk::Set& groundSet, int& numFeatures) {
    numFeatures = 0;
    std::ifstream infile(inputFile);
    cout << "Processing input" << endl;
    groundSetSize = 0;
    for(std::string line; getline(infile, line);) {
        datk::SparseFeature s;
        std::stringstream iss(line);
        double a;
        numFeatures = 0;
        while(iss >> a) {
            if(a != 0) {
                s.featureIndex.push_back(numFeatures);
                s.featureVec.push_back(a);
            }
            numFeatures++;
        }
        s.numFeatures = numFeatures;
        s.numUniqueFeatures = s.featureIndex.size();
        cout << "Sparse feature constructed as: ";
        for (auto i: s.featureIndex)
            std::cout << i << ' ';
        std::cout << endl;
        for (auto i: s.featureVec)
            std::cout << i << ' ';
        std::cout << endl;
        features.push_back(s);
        groundSet.insert(groundSetSize);
        groundSetSize++;
    }
    cout << "Ground set size: " << groundSetSize << endl;
    cout << "Num features = " << numFeatures << endl;

}

int main(int argc, char *argv[]) {
    if(argc < 4) {
        cout << "Usage: ./TestSubmodularOptimization <budget> <algo> <input_file> <shots_json_file_for_PS> <annotation_file_for_VSL>" << endl;
        return 0;
    }
    int groundSetSize;
    int budget = atoi(argv[1]);
    datk::Set groundSet;
    if(strcmp(argv[2], "FL") == 0 || strcmp(argv[2], "DM") == 0 || strcmp(argv[2], "GC") == 0 || strcmp(argv[2], "SC") == 0) {
        std::vector<std::vector<float>> kernel;
        computeKernel(argv[3], groundSet, groundSetSize, kernel);
        if(strcmp(argv[2], "FL") == 0) {
            datk::FacilityLocation facLoc(groundSetSize, kernel);
            runTest(facLoc, budget, groundSetSize, groundSet);
        } else if(strcmp(argv[2], "SC") == 0) {
            datk::SaturateCoverage satCov(groundSetSize, kernel, 0.1);
            runTest(satCov, budget,  groundSetSize, groundSet);
        } else if(strcmp(argv[2], "GC") == 0) {
            datk::GraphCutFunctions graphCut(groundSetSize, kernel, 0.5);
            runTest(graphCut, budget,  groundSetSize, groundSet);
        } else if(strcmp(argv[2], "DM") == 0) {
            datk::DisparityMin dispMin(groundSetSize, kernel);
            runTest(dispMin, budget,  groundSetSize, groundSet);
        } else {
            cout << "Not yet supported";
            return 0;
        }
    } else if(strcmp(argv[2], "PSC") == 0) {
        std::vector<std::vector<double>> probVectors;
        int numConcepts = 0;
        computeProbabilityConcepts(argv[3], groundSet, groundSetSize, numConcepts, probVectors);
        datk::ProbabilisticSetCover probSC(groundSetSize, numConcepts, probVectors);
        runTest(probSC, budget,  groundSetSize, groundSet);

    } else if(strcmp(argv[2], "SeC") == 0) {
        std::vector<datk::Set> conceptVectors;
        computeConcepts(argv[3], groundSetSize, conceptVectors, groundSet);
        datk::SetCover setCover(groundSetSize, conceptVectors);
        runTest(setCover, budget,  groundSetSize, groundSet);
    } else if(argv[2][0] == 'F' && argv[2][1] == 'B') {
        int type = argv[2][2] - '0';
        int numFeatures = 0;
        cout << "Mode desired = " << type << endl;
        vector<datk::SparseFeature> features;
        computeSparseFeatures(argv[3], groundSetSize, features, groundSet, numFeatures);
        vector<double> featureWeights(numFeatures, 1);
        datk::FeatureBasedFunctions ff(groundSetSize, type, features, featureWeights, 0.2);
        runTest(ff, budget,  groundSetSize, groundSet);
    } else if(strcmp(argv[2], "PS") == 0) {
        nlohmann::json shotsJson;
        std::ifstream shotsJsonFile;
        shotsJsonFile.open(argv[4]);
        shotsJsonFile >> shotsJson;
        groundSetSize = shotsJson.size();
        cout << "Ground set size = " << groundSetSize << endl;
        std::vector<std::vector<float>> kernel;
        datk::Set groundSet;
        for(int i=0; i<groundSetSize; i++) {
            groundSet.insert(i);
        }
        cout << "Computing kernel" << endl;
        double sim = 0;
        for (int i = 0; i < groundSetSize; i++) {
            std::vector<float> currvector;
            for (int j = 0; j < groundSetSize; j++) {
                if(i==j) sim = 1;
                else {
                    if(shotsJson[std::to_string(i)] == shotsJson[std::to_string(j)]) sim = 1.0/abs(j - i);
                    else sim = 0;
                }
                currvector.push_back(sim);
            }
            kernel.push_back(currvector);
        }
        cout << "Unnormalized kernel" << endl;
        //0-1 normalize using min and max - this becomes the kernel
        double max = 1;
        double min = 1/(groundSetSize - 1);
        double range = max - min;
        for (int i = 0; i < groundSetSize; i++) {
            for (int j = 0; j < groundSetSize; j++) {
                cout << kernel[i][j] << " ";
                kernel[i][j] = (kernel[i][j] - min) / range;
            }
            cout << endl;
        }
        datk::PairwiseSum pair(groundSetSize, kernel);
        runTest(pair, budget,  groundSetSize, groundSet);
        cout << "One off test for PS: ********** This assumes a particular input ************" << endl;
        cout << "Score of two closeby elements from same shot should be > score of two far away elements from same shot" << endl;
        datk::Set check11;
        datk::Set check12;
        check11.insert(8);
        check11.insert(9);
        check12.insert(8);
        check12.insert(12);
        if(pair.eval(check11) > pair.eval(check12)) cout << "\n****** Passed ******\n" << endl;
        else cout << "\n**** Failed ****\n" << endl;
        cout << "Score of a set which has several elements from same shot should be more than score of a subset with elements from different shots" << endl;
        datk::Set check21;
        datk::Set check22;
        check21.insert(4);
        check21.insert(5);
        check21.insert(6);
        check21.insert(7);
        check22.insert(2);
        check22.insert(6);
        check22.insert(8);
        check22.insert(13);
        if(pair.eval(check21) > pair.eval(check22)) cout << "\n****** Passed ******\n" << endl;
        else cout << "\n**** Failed ****\n" << endl;
    } else if (strcmp(argv[2], "VSL1") == 0 ) {
        nlohmann::json annotation;
        std::string annotationJsonFile = argv[5];
        std::ifstream annotationJson;
        annotationJson.open(annotationJsonFile);
        annotationJson >> annotation;
        groundSetSize = 31;
        datk::Set groundSet;
        for(int i=0; i<groundSetSize; i++) {
            groundSet.insert(i);
        }
        std::vector<datk::Set> segments;
        std::vector<int> ratings;
        std::vector<bool> isRedundant;
        double alpha = 1;
        double beta = 6;
        double penalty = 10;
        int segIndex = 0;
        int maxSnippetId = -1;
        for(auto seg : annotation) {
            datk::Set snippets;
            string line = seg["begin"];
            std::vector <string> tokens;
            stringstream check1(line);
            string intermediate;
            while(getline(check1, intermediate, ':')) {
                tokens.push_back(intermediate);
            }
            int hrs = stoi(tokens[0]);
            int mins = stoi(tokens[1]);
            int secs = stoi(tokens[2]);
            float beginSeconds=3600*hrs+60*mins+secs;
            line = seg["end"];
            stringstream check2(line);
            tokens.clear();
            while(getline(check2, intermediate, ':')) {
                tokens.push_back(intermediate);
            }
            hrs = stoi(tokens[0]);
            mins = stoi(tokens[1]);
            secs = stoi(tokens[2]);
            float endSeconds=3600*hrs+60*mins+secs;
            //add snippet ids to this set
            int beginSnippetId = ceil(beginSeconds/2.0);
            int endSnippetId =floor((endSeconds-1)/2.0);
            //for last end, if odd
            if (endSnippetId == groundSetSize) endSnippetId --;
            if (endSnippetId > maxSnippetId) maxSnippetId = endSnippetId;
            for(int id = beginSnippetId; id <= endSnippetId; id++) {
                snippets.insert(id);
            }
            segments.push_back(snippets);
            ratings.push_back(seg["rating"]);
            bool isRepetitive = false;
            if(seg.find("repetitive") == seg.end()) {
                isRepetitive = false;
            } else {
                if(seg["repetitive"] == true) isRepetitive = true;
                else isRepetitive = false;
            }
            isRedundant.push_back(isRepetitive);
        }
        cout << "Max snippet id = " << maxSnippetId << endl;

        for(auto segment: segments) {
            for (auto snip: segment) {
                cout << snip << " ";
            }
            cout << endl;
        }
        datk::VideoSummLoss vsl(groundSetSize, segments, ratings, isRedundant, alpha, beta, penalty, 10000000);
        runTest(vsl, budget,  groundSetSize, groundSet);
        cout << "One off test for VSL to check optimum value:" << endl;
        datk::Set candidateIndices;
        datk::naiveGreedyMax(vsl, 20, candidateIndices);
        double candidateScore = vsl.eval(candidateIndices);
        cout << "Optimal subset of size 20 on a particular data is: " << endl;
        for(auto tmp: candidateIndices) {
            cout << tmp << " ";
        }
        cout << endl;
        cout << "... and its score is: " << candidateScore << endl;
        cout << "Now running for 10..." << endl;
        candidateIndices.clear();
        datk::naiveGreedyMax(vsl, 10, candidateIndices);
        candidateScore = vsl.eval(candidateIndices);
        cout << "Optimal subset of size 10 is: " << endl;
        for(auto tmp: candidateIndices) {
            cout << tmp << " ";
        }
        cout << endl;
        cout << "... and its score is: " << candidateScore << endl;
    } else if(strcmp(argv[2], "VSL2") == 0 ) {
        nlohmann::json annotation;
        std::string annotationJsonFile = argv[5];
        std::ifstream annotationJson;
        annotationJson.open(annotationJsonFile);
        annotationJson >> annotation;
        groundSetSize = 31;
        datk::Set groundSet;
        for(int i=0; i<groundSetSize; i++) {
            groundSet.insert(i);
        }
        std::vector<datk::Set> segments;
        std::vector<int> ratings;
        std::vector<bool> isRedundant;
        double alpha = 1;
        double beta = 6;
        double penalty = 10;
        int segIndex = 0;
        int maxSnippetId = -1;
        for(auto seg : annotation) {
            datk::Set snippets;
            string line = seg["begin"];
            std::vector <string> tokens;
            stringstream check1(line);
            string intermediate;
            while(getline(check1, intermediate, ':')) {
                tokens.push_back(intermediate);
            }
            int hrs = stoi(tokens[0]);
            int mins = stoi(tokens[1]);
            int secs = stoi(tokens[2]);
            float beginSeconds=3600*hrs+60*mins+secs;
            line = seg["end"];
            stringstream check2(line);
            tokens.clear();
            while(getline(check2, intermediate, ':')) {
                tokens.push_back(intermediate);
            }
            hrs = stoi(tokens[0]);
            mins = stoi(tokens[1]);
            secs = stoi(tokens[2]);
            float endSeconds=3600*hrs+60*mins+secs;
            //add snippet ids to this set
            int beginSnippetId = ceil(beginSeconds/2.0);
            int endSnippetId =floor((endSeconds-1)/2.0);
            //for last end, if odd
            if (endSnippetId == groundSetSize) endSnippetId --;
            if (endSnippetId > maxSnippetId) maxSnippetId = endSnippetId;
            for(int id = beginSnippetId; id <= endSnippetId; id++) {
                snippets.insert(id);
            }
            segments.push_back(snippets);
            ratings.push_back(seg["rating"]);
            bool isRepetitive = false;
            if(seg.find("repetitive") == seg.end()) {
                isRepetitive = false;
            } else {
                if(seg["repetitive"] == true) isRepetitive = true;
                else isRepetitive = false;
            }
            isRedundant.push_back(isRepetitive);
        }
        cout << "Max snippet id = " << maxSnippetId << endl;
        for(auto segment: segments) {
            for (auto snip: segment) {
                cout << snip << " ";
            }
            cout << endl;
        }
        datk::VideoSummLoss vsl(groundSetSize, segments, ratings, isRedundant, alpha, beta, penalty, 20000000);
        runTest(vsl, budget,  groundSetSize, groundSet);

        cout << "One off test for VSL2 to check optimum value:" << endl;
        datk::Set candidateIndices;
        datk::naiveGreedyMax(vsl, 20, candidateIndices);
        double candidateScore = vsl.eval(candidateIndices);
        cout << "Optimal subset of size 20 on a particular data is: " << endl;
        for(auto tmp: candidateIndices) {
            cout << tmp << " ";
        }
        cout << endl;
        cout << "... and its score is: " << candidateScore << endl;
        cout << "Now running for 10..." << endl;
        candidateIndices.clear();
        datk::naiveGreedyMax(vsl, 10, candidateIndices);
        candidateScore = vsl.eval(candidateIndices);
        cout << "Optimal subset of size 10 is: " << endl;
        for(auto tmp: candidateIndices) {
            cout << tmp << " ";
        }
        cout << endl;
        cout << "... and its score is: " << candidateScore << endl;
    } else if(strcmp(argv[2], "Mix") == 0) {
        vector<datk::SetFunctions*> fvec;
        int numComponents = 11;
        vector<double> lambdaVec(numComponents, 1);
        {
            std::vector<std::vector<float>> kernel;
            computeKernel("/media/hdd1/home/vishal/groundSet.txt", groundSet, groundSetSize, kernel);

            double lambda = 0;

            datk::FacilityLocation* facLoc = new datk::FacilityLocation(groundSetSize, kernel);
            lambda = 1/facLoc->eval(groundSet);
            cout << "F(V) for FL = " << 1/lambda << endl;
            datk::ScaleSetFunctions* facLocScaled = new datk::ScaleSetFunctions(facLoc, lambda);
            fvec.push_back(facLocScaled);

            datk::SaturateCoverage* satCov = new datk::SaturateCoverage(groundSetSize, kernel, 0.1);
            lambda = 1/satCov->eval(groundSet);
            cout << "F(V) for SC = " << 1/lambda << endl;
            datk::ScaleSetFunctions* satCovScaled = new datk::ScaleSetFunctions(satCov, lambda);
            fvec.push_back(satCovScaled);

            datk::GraphCutFunctions* graphCut = new datk::GraphCutFunctions(groundSetSize, kernel, 0.5);
            lambda = 1/graphCut->eval(groundSet);
            cout << "F(V) for GC = " << 1/lambda << endl;
            datk::ScaleSetFunctions* graphCutScaled = new datk::ScaleSetFunctions(graphCut, lambda);
            fvec.push_back(graphCutScaled);

            datk::DisparityMin* dispMin = new datk::DisparityMin(groundSetSize, kernel);
            lambda = 1/dispMin->eval(groundSet);
            cout << "F(V) for DM = " << 1/lambda << endl;
            datk::ScaleSetFunctions* dispMinScaled = new datk::ScaleSetFunctions(dispMin, 1);
            fvec.push_back(dispMinScaled);

            datk::PairwiseSum* pair = new datk::PairwiseSum(groundSetSize, kernel);
            lambda = 1/pair->eval(groundSet);
            cout << "F(V) for PS = " << 1/lambda << endl;
            datk::ScaleSetFunctions* pairScaled = new datk::ScaleSetFunctions(pair, lambda);
            fvec.push_back(pairScaled);

            std::vector<std::vector<double>> probVectors;
            int numConcepts = 0;
            computeProbabilityConcepts("/media/hdd1/home/vishal/groundSetCover.txt", groundSet, groundSetSize, numConcepts, probVectors);

            datk::ProbabilisticSetCover* probSC = new datk::ProbabilisticSetCover(groundSetSize, numConcepts, probVectors);
            lambda = 1/probSC->eval(groundSet);
            cout << "F(V) for PSC = " << 1/lambda << endl;
            datk::ScaleSetFunctions* probSCScaled = new datk::ScaleSetFunctions(probSC, lambda);
            fvec.push_back(probSCScaled);

            std::vector<datk::Set> conceptVectors;
            computeConcepts("/media/hdd1/home/vishal/groundSetCover.txt", groundSetSize, conceptVectors, groundSet);

            datk::SetCover* setCover = new datk::SetCover(groundSetSize, conceptVectors);
            lambda = 1/setCover->eval(groundSet);
            cout << "F(V) for SeC = " << 1/lambda << endl;
            datk::ScaleSetFunctions* setCoverScaled = new datk::ScaleSetFunctions(setCover, lambda);
            fvec.push_back(setCoverScaled);

            int numFeatures = 0;
            vector<datk::SparseFeature> features;
            computeSparseFeatures("/media/hdd1/home/vishal/groundSetFeature.txt", groundSetSize, features, groundSet, numFeatures);
            vector<double> featureWeights(numFeatures, 1);

            datk::FeatureBasedFunctions* ff1 = new datk::FeatureBasedFunctions(groundSetSize, 1, features, featureWeights, 0.2);
            lambda = 1/ff1->eval(groundSet);
            cout << "F(V) for FF1 = " << 1/lambda << endl;
            datk::ScaleSetFunctions* ff1Scaled = new datk::ScaleSetFunctions(ff1, lambda);
            fvec.push_back(ff1Scaled);

            datk::FeatureBasedFunctions* ff2 = new datk::FeatureBasedFunctions(groundSetSize, 2, features, featureWeights, 0.2);
            datk::ScaleSetFunctions* ff2Scaled = new datk::ScaleSetFunctions(ff2, lambda);
            lambda = 1/ff2->eval(groundSet);
            cout << "F(V) for FF2 = " << 1/lambda << endl;
            fvec.push_back(ff2Scaled);

            datk::FeatureBasedFunctions* ff3 = new datk::FeatureBasedFunctions(groundSetSize, 3, features, featureWeights, 0.2);
            lambda = 1/ff3->eval(groundSet);
            cout << "F(V) for FF3 = " << 1/lambda << endl;
            datk::ScaleSetFunctions* ff3Scaled = new datk::ScaleSetFunctions(ff3, lambda);
            fvec.push_back(ff3Scaled);

            datk::FeatureBasedFunctions* ff4 = new datk::FeatureBasedFunctions(groundSetSize, 4, features, featureWeights, 0.2);
            lambda = 1/ff4->eval(groundSet);
            cout << "F(V) for FF4 = " << 1/lambda << endl;
            datk::ScaleSetFunctions* ff4scaled = new datk::ScaleSetFunctions(ff4, lambda);
            fvec.push_back(ff4scaled);
        }
        datk::CombineSetFunctions fComb(groundSetSize, fvec, lambdaVec);

        runTest(fComb, budget,  groundSetSize, groundSet);

        datk::Set candidateIndices;
        cout << "Begin naiveGreedyMax" << endl;
        datk::naiveGreedyMax(fComb, 5, candidateIndices, 1);
        cout << "End naiveGreedyMax" << endl;

    } else {
        cout << "Not yet supported" << endl;
        return 0;
    }
}
