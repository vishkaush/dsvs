#include <iostream>
#include <fstream>
#include <stdlib.h> 
#include <time.h>

using namespace std;

int main(int argc, char* argv[]) {
    if(argc < 2) {
        cout << "Usage: ./CreateCoverageInput <output_file_path>" << endl;
        return 0;
    }
    srand (time(NULL));
    ofstream myfile;
    std::string str(argv[1]);
    myfile.open (str + "/groundSetCover.txt");
    int temp;
    float probs[10];
    for(int i=0; i<50; i++) {
        int sum = 0;
        for(int j=0; j<10; j++) {
            probs[j] = rand()%100 + 1;      
            sum += probs[j];
        } 
        for(int j=0; j<10; j++) {
            probs[j] = probs[j]/sum;
            myfile << probs[j] << " ";
        }
        myfile << "\n";
    }
    myfile.close();
    return 0;    
}
