#Arguments are, in order:
#Path of Dataset JSON
#Path of random JSONs folder
#Path of GT JSONs folder
#Path of the file which has square normalization constants
#Path of output dir

import json
import numpy as np
import time
import sys
import datetime
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import glob

data=json.load(open(sys.argv[1]))
norm_json=json.load(open(sys.argv[4]))
# random_files=glob.glob(sys.argv[2])
# gt_files=glob.glob(sys.argv[3]+'*')

for fraction in [0.05]:
    for category in data["categories"]:
        n=0
        gt_min_list=[]
        gt_avg_list=[]
        gt_max_list=[]
        random_min_list=[]
        random_avg_list=[]
        random_max_list=[]
        x_axis = []
        min_score_loss = 0
        avg_score_loss = 0
        max_score_loss = 0
        for video_name in data["categories"][category]:
            videoCount = 0
            if video_name != "num_videos" and video_name != "total_duration":
            # if video_name=="birthday1":
                print(video_name)
                parts = video_name.split("_")
                gt_min=float('inf')
                gt_avg=0
                gt_max=-float('inf')
                gt_files=glob.glob(sys.argv[3]+category+"/"+video_name+"/"+str(fraction)+"/*")
                print(len(gt_files))
                if len(gt_files) == 0:
                    print("No gt files found for " + video_name + " and " + str(fraction) + " hence discarding this video")
                    continue
                #length=data["categories"][category][video_name]["duration"]
                #normalization=length*length
                normalization = norm_json[video_name]['max_score'] - norm_json[video_name]['min_score']

                for file in gt_files:
                    #print(file)
                    gt_json = json.loads(open(file).read())
                    #gt_score=compute(data,category,video_name,file,"1 1 1 1")
                    gt_score = gt_json["score"]
                    #print("score",gt_score)
                    gt_score = gt_score/normalization
                    #print("normalized score", gt_score)
                    # print(gt_score)
                    if gt_score<gt_min:
                        gt_min=gt_score
                    if gt_score>gt_max:
                        gt_max=gt_score
                    gt_avg+=gt_score

                random_files=glob.glob(sys.argv[2]+category+"/"+video_name+"/"+str(fraction)+"/*")
                print(len(random_files))
                if len(random_files) == 0:
                    print("No random files found for " + video_name + " and " + str(fraction) + " hence discarding this video")
                    continue
                videoCount = videoCount + 1
                random_min=float('inf')
                random_avg=0
                random_max=-float('inf')
                # print(random_files)
                for file in random_files:
                    #print(file)
                    random_json = json.loads(open(file).read())
                    #random_score=compute(data,category,video_name,file,"1 1 1 1")
                    random_score = random_json["score"]
                    random_score = random_score/normalization
                    # print(random_score)
                    if random_score<random_min:
                        random_min=random_score
                    if random_score>random_max:
                        random_max=random_score
                    random_avg+=random_score
                #print("GT's values:")
                #print(gt_min,gt_avg/len(gt_files),gt_max)
                #print("Random's values:")
                #print(random_min,random_avg/len(random_files),random_max)
                video_id = int(parts[1])
                gt_min_list.append((video_id, gt_min))
                gt_avg_list.append((video_id, gt_avg/len(gt_files)))
                gt_max_list.append((video_id, gt_max))
                #print(video_name, random_min, random_avg/len(random_files), random_max, gt_min, gt_max)
                min_score_loss += gt_max - random_min
                max_score_loss += gt_max - random_max

                random_min_list.append((video_id, random_min))
                random_avg_list.append((video_id, random_avg/len(random_files)))
                random_max_list.append((video_id, random_max))
                x_axis.append(video_id)
                n+=1
            print(category, min_score_loss, max_score_loss, n)
            if n!=0 :
                print(category, min_score_loss/n, max_score_loss/n)

        gt_min_list = [y for x, y in sorted(gt_min_list)]
        gt_max_list = [y for x, y in sorted(gt_max_list)]
        gt_avg_list = [y for x, y in sorted(gt_avg_list)]
        random_min_list = [y for x, y in sorted(random_min_list)]
        random_max_list = [y for x, y in sorted(random_max_list)]
        random_avg_list = [y for x, y in sorted(random_avg_list)]
        x_axis = [str(x) for x in sorted(x_axis)]
        #print(gt_min_list)
        #print(gt_avg_list)
        #print(gt_max_list)
        #print(random_min_list)
        #print(random_avg_list)
        #print(random_max_list)
        #print(x_axis)
#plt.plot(range(n),gt_min_list,range(n),gt_avg_list,range(n),gt_max_list,range(n),random_min_list,range(n),random_avg_list,range(n),random_max_list)
#plt.show()


        fig = plt.figure()
        ax1 = fig.add_subplot(111)
#ax2 = fig.add_subplot(212)
        ax1.plot(random_min_list,'k.-',label='randomMin',color='blue')
        ax1.plot(random_avg_list,'k.--',label='randomAvg',color='blue')
        ax1.plot(random_max_list,'k.:',label='randomMax',color='blue')

        ax1.plot(gt_min_list,'k.-',label='gtMin',color='green')
        ax1.plot(gt_avg_list,'k.--',label='gtAvg',color='green')
        ax1.plot(gt_max_list,'k.:',label='gtMax',color='green')

        plt.xticks(range(n), x_axis, size='x-small')
        plt.setp(ax1.get_xticklabels(), visible=True)
#plt.setp(ax2.get_xticklabels(), visible=True)
        fig.suptitle('Comparison of scores of random and GT for ' + category + ' videos')
        plt.ylabel('Normalized scores')
        plt.xlabel('Videos')
        plt.legend()
        fig.savefig(sys.argv[5]+'/'+category+'_Random_GT.png')
"""
print(gt_min_list)
print(gt_avg_list)
print(gt_max_list)
print(random_min_list)
print(random_avg_list)
print(random_max_list)
print(x_axis)
#plt.plot(range(n),gt_min_list,range(n),gt_avg_list,range(n),gt_max_list,range(n),random_min_list,range(n),random_avg_list,range(n),random_max_list)
#plt.show()


fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(gt_min_list,'k.-',label='gtMin',color='green')
ax1.plot(gt_avg_list,'k.--',label='gtAvg',color='green')
ax1.plot(gt_max_list,'k.:',label='gtMax',color='green')
#ax2 = fig.add_subplot(212)
ax1.plot(random_min_list,'k.-',label='randomMin',color='blue')
ax1.plot(random_avg_list,'k.--',label='randomAvg',color='blue')
ax1.plot(random_max_list,'k.:',label='randomMax',color='blue')

plt.xticks(range(n), x_axis, size='small')
plt.setp(ax1.get_xticklabels(), visible=True)
#plt.setp(ax2.get_xticklabels(), visible=True)
fig.suptitle('Comparison of scores of random summaries and GT summaries for ' + sys.argv[5] + ' videos')
plt.ylabel('Normalized scores')
plt.xlabel('Videos')
fig.savefig(sys.argv[5]+'_Random_GT.png')
"""
