#!/usr/bin/python2

import sys
import os
import logging
import numpy as np
import cPickle as pickle

import learning_framework

logging.basicConfig(level=logging.DEBUG)


def rating_to_annotation_list(ratings):
    annotation_list = []
    for i, r in enumerate(ratings):
        annotation_list.append(((i * 2, (i+1)*2), r, False))
    return annotation_list

def get_train_data(train_file, ratings_path, feature_path):
    training_examples = []
    with open(train_file, 'r') as f:
        vid_files = [line.strip() for line in f]

    for vid in vid_files:
        logging.info("Processing train_file %s" % vid)
        vid = os.path.splitext(os.path.basename(vid))[0]
        ratings_pkl_path = ratings_path + '/' + vid + '.ratings.pkl'
        feature_base_path =  feature_path + '/' + vid

        vgg_feature = pickle.load(open(feature_base_path + '.vgg_features.pkl', 'rb'))
        googlenet_feature = pickle.load(open(feature_base_path + '.googlenet_features.pkl', 'rb'))
        yolo_voc_feature = pickle.load(open(feature_base_path + '.yolo_voc_features.pkl', 'rb'))
        yolo_coco_feature = pickle.load(open(feature_base_path + '.yolo_coco_features.pkl', 'rb'))
        color_hist_feature = pickle.load(open(feature_base_path + '.color_hist_features.pkl', 'rb'))

        feat_len_dict = dict()

        feature_list = [vgg_feature, googlenet_feature, yolo_voc_feature,
                        yolo_coco_feature, color_hist_feature]

        for feat in feature_list:
            for item in feat:
                if not isinstance(feat[item], int):
                    feat_len_dict[item] = len(feat[item][0])

        # Fix later for shot boundary
        vid_len = 2 * vgg_feature['num_features']
        Y = vgg_feature['num_features']
        snippet_timestamp = [(x * 2, x * 2 + 2) for x in range(Y)]
        gaussian_sigma = 10
        alpha = 1
        beta = 6
        penalty = -2

        ratings = pickle.load(open(ratings_pkl_path, 'rb'))

        print("Vid %s, Vid len %d, rating len %d" % (vid, vid_len,
            len(ratings["ratings"][0]) * 2))

        assert vid_len == len(ratings["ratings"][0]) * 2

        for rater in range(len(ratings["budget"])):
            for sum_size in {0.1, 0.2, 0.3}:
                annotation_list = rating_to_annotation_list(
                        ratings["ratings"][rater])
                budget = max(1, int(vid_len * sum_size))
                y_gt = list(ratings["ratings"][rater].argsort()[::-1][:max(1, int(budget / 2))])
                max_score = budget * np.exp(3 * alpha)
                min_score = budget * penalty
                S = learning_framework.VideoElem(budget=budget, Y=Y, y_gt = y_gt, snippet_timestamp=snippet_timestamp,
                        annotation=annotation_list, max_score=max_score, min_score=min_score,
                        gaussian_sim_sigma=gaussian_sigma, alpha=alpha, beta=beta, penalty=penalty,
                        vgg_concepts = vgg_feature['vgg_concepts'],
                        googlenet_concepts = googlenet_feature['googlenet_concepts'],
                        yolo_voc_concepts = yolo_voc_feature['yolo_voc_concepts'],
                        yolo_coco_concepts = yolo_coco_feature['yolo_coco_concepts'],
                        vgg_p_concepts = vgg_feature['vgg_p_concepts'],
                        googlenet_p_concepts = googlenet_feature['googlenet_p_concepts'],
                        vgg_features = vgg_feature['vgg_features'],
                        googlenet_features = googlenet_feature['googlenet_features'],
                        color_hist_r_features = color_hist_feature['color_hist_r_features'],
                        color_hist_g_features = color_hist_feature['color_hist_g_features'],
                        color_hist_b_features = color_hist_feature['color_hist_b_features'],
                        color_hist_h_features = color_hist_feature['color_hist_h_features'],
                        color_hist_s_features = color_hist_feature['color_hist_s_features']
                    )


                training_examples.append(S)

    return training_examples, feat_len_dict


    '''
    with open(train_file, 'r') as f:
        vid_files = [line.strip() for line in f]

    for vid in vid_files:
        logging.info("Processing train_file %s" % vid)
        feature_file_path =  FEATURE_PATH + vid + '.features.pkl'
        ratings_file_path = RATING_PATH + vid + '.ratings.pkl'
        feature = pickle.load(open(feature_file_path, 'rb'))
        ratings = pickle.load(open(ratings_file_path, 'rb'))

        if len(feature["feature_vector"]) != len(ratings["ratings"][0]):
            logging.warn("Feature length mismatch. Skipping %s" % vid)
            continue

        for rater in range(len(ratings["budget"])):
            for sum_size in {0.1, 0.2, 0.3}:
                S = VideoElem()
                S.rating = list(ratings["ratings"][rater])
                S.Y = list(range(len(ratings["ratings"][rater])))
                S.feature = feature["feature_vector"]
                S.budget = max(1, int(len(S.Y) * sum_size))
                S.y_gt = list(ratings["ratings"][rater].argsort()[::-1][:S.budget])

                S.max_score = get_summary_score(S.Y, S.rating, S.y_gt)
                worst_selection = list(ratings["ratings"][rater].argsort()[:S.budget])
                S.min_score = get_summary_score(S.Y, S.rating, worst_selection)

                training_examples.append(S)

    return training_examples
    '''

if __name__ == "__main__":
    train_file = sys.argv[1]
    annotation_path = sys.argv[2]
    feature_path = sys.argv[3]
    out_weight = sys.argv[4]

    sat_cov_c = 1
    training_examples, feat_len_dict = get_train_data(train_file, annotation_path, feature_path)
    weights = learning_framework.learn(training_examples, feat_len_dict, sat_cov_c, out_weight)
    weights = np.array(weights)

    with open(out_weight, 'wb') as f:
        pickle.dump(weights, f, protocol=2)
    '''
    train_file = sys.argv[1]
    out_weight = sys.argv[2]

    training_examples = get_train_data(train_file)
    weights = learn(training_examples)
    weights = np.array(weights)

    with open(out_weight, 'wb') as f:
        pickle.dump(weights, f, protocol=2)
    '''
