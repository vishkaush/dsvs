#include "./datk/src/utils/json.hpp"
#include "./datk/src/datk.h"

class MixtureModel {
public:
    MixtureModel(nlohmann::json components, std::string budget);
    MixtureModel(std::string modelFile, std::string budget);
    ~MixtureModel();
    int numComponents;
    int numModComponents;
    nlohmann::json components;
    std::vector<double> weightVecWithoutLoss;
    std::vector<std::string> componentLookup;
    int epochNum;
    std::string train_category;
    std::string budget;
    void test(nlohmann::json testData, std::string featuresFolder);
    void train(int num_epochs, nlohmann::json trainingData, std::string featuresFolder, nlohmann::json normData, std::string shotsFolder, nlohmann::json dataset, std::string gtFolder, std::string name, double marginWeight, bool useRandomGt, bool allowNegWeightForMod, nlohmann::json modelParams, bool useRandomGreedy, double lambda1, double lambda2, bool useAda, bool calculateSubModScores);
};

struct WeightAndComponent {
    double weight;
    std::string componentName;
    // bool operator<(const a& rhs) const { weight < rhs.weight; }
    bool operator() (WeightAndComponent i, WeightAndComponent j) { return (i.weight > j.weight);}
};
