# ARGUMENTS
# 1. Dataset path
# 2. Output folder path
import json
import sys
import numpy as np
import datetime
import argparse

# if __name__ == "__main__":
#     parser = argparse.ArgumentParser(description="Script to view annotations and summaries in context of original videos")
#     parser.add_argument('--dataset', required=True, help='''Full path to metadata JSON of the dataset''')
#     parser.add_argument('--category', help='''Specify a particular category. If not specified, the script will run for all categories''')
#     parser.add_argument('--video_name', help='''Specify a particular video for the specified category. Otherwise the script will run for all videos in the specified category. This argument makes sense only when category is specified''')
#     parser.add_argument('--summaries_path', required=True, help='''Either path to the root folder containing all summaries or gt. When specified as gt, only annotations are overlayed on top of original video. When path is specified, corresponding summary informtion is also overlayed on top of original video''')

#     args = parser.parse_args()


def time_in_seconds_annotation(numbers):
	#print(numbers)
	seconds=int(numbers[4])+10*int(numbers[3])+60*int(numbers[2])+600*int(numbers[1])+3600*int(numbers[0])
	return seconds

def set_rating(length,data):
	rating=np.zeros(length)

	for block in data:
		# begins=block["begin"].replace(':','')
		# end=block["end"].replace(':','')
		begin=time_in_seconds_annotation(block["begin"].replace(':',''))
		end=time_in_seconds_annotation(block["end"].replace(':',''))
		rating[begin:end]=1
	return rating


def make_json(data,summary,title):
	started=0
	i=0
	while i <len(summary):
		if summary[i]==1 and started==0:
			started=1
			segment={}
			segment["begin"]=i
			while i<len(summary) and summary[i]==1:
				i=i+1

			segment["end"]=i
			started=0
			data.append(segment)
		i=i+1

	# with open(title+'.json', 'w') as outfile:
	# 	json.dump(data, outfile)
	return data


data = json.load(open(sys.argv[1]))
for category in data["categories"]:
	for video in data["categories"][category]:
		# if category.lower() in video:
		if video != "num_videos" and video != "total_duration":
			print("Processing video " + video)
			annotation_file=data["categories"][category][video]["annotation"]
			# print(annotation_file)
			length=data["categories"][category][video]["duration"]
			# print(data["root"]+category+"/modified JSONs/"+annotation_file)
			annot = json.load(open(data["root"]+category+"/"+annotation_file))
			# print(annot)
			json_data=np.zeros(length)
			rating=set_rating(length,annot)
			# annot=make_json(annot,rating,video)
			i=0
			while i <length:
				if rating[i]==0:
					begin=i
					while i<length and rating[i]==0:
						i=i+1
					end=i
					seg={}
					seg["begin"]=str(datetime.timedelta(seconds=begin))
					seg["end"]=str(datetime.timedelta(seconds=end))
					seg["rating"]=0
					seg["description"]="Automatically generated for default segments"
					annot.append(seg)

				i=i+1



			newlist = sorted(annot, key=lambda k: time_in_seconds_annotation(k["begin"].replace(':','')))

			with open(sys.argv[2]+video+'.json', 'w') as outfile:
				json.dump(newlist, outfile)

