#include "MixtureModel.h"
#include <ctime>
#include <sys/types.h>
#include <dirent.h>
#include "./datk/src/optimization/discFunctions/ScaleSetFunctions.h"
#include "./datk/src/optimization/discFunctions/ModularFunctions.h"
#include "./datk/src/optimization/discFunctions/CombineSetFunctions.h"
#include "./datk/src/optimization/discAlgorithms/randomGreedyMax.h"
#include "LargeMarginMixtureLearningL2Regularized.h"
#include <math.h>

double clockInSec() {
    return ((double) clock()) / CLOCKS_PER_SEC;
}

MixtureModel::MixtureModel(nlohmann::json components, std::string budget) {
    cout << "\nMixtureModel constructor called" << endl;
    this->components = components;
    this->budget = budget;
    this->epochNum = 0;
    numComponents = 0;
    numModComponents = 0;
    int subNum = 0;
    for (auto it = components.begin(); it != components.end(); ++it)
    {
        std::cout << it.key() << " | " << it.value() << "\n";
        if(it.key() == "Mod"){
            subNum = 0;
            for (auto subit = components[it.key()].begin(); subit != components[it.key()].end(); ++subit) {
                cout << "Sub key " << subit.key()<< endl;
                subNum += (int)components[it.key()][subit.key()];
                cout << components[it.key()][subit.key()] << " components added for Mod:" << subit.key()<< endl;
            }
            numComponents += subNum;
            numModComponents += subNum;
        } else if(it.key()=="FL" || it.key()=="SC" || it.key()=="GC" || it.key()=="DM" || it.key()=="SeC" || it.key()=="PSC" || it.key()=="PS" || it.key()=="FB") {
            subNum = 1;
            bool enteredLoop = false;
            for (auto subit = components[it.key()].begin(); subit != components[it.key()].end(); ++subit) {
                enteredLoop = true;
                cout << "Sub key " << subit.key()<< endl;
                subNum *= components[it.key()][subit.key()].size();
                cout << "Found " << components[it.key()][subit.key()].size() << " components for " << it.key()<< ":" << subit.key()<< endl;
            }
            if(it.key() == "PS") {
                assert(1 == subNum);
                enteredLoop = true;
            }
            if(!enteredLoop) {
                cout<<"Empty component "<<it.key()<<endl;
                exit(0);
            }
            numComponents += subNum;
        } else {
            cout << "Unknown key in components JSON file" << endl;
            exit(0);
        }
    }
    cout << "\nTotal number of components desired = " << numComponents << endl;
    cout << "\nTotal number of modular components = " << numModComponents << endl;
    //cout << "Initializing weights to random numbers between 0 and 1" << endl;
    //for(int k = 0; k < numComponents; k++) {
    //    weightVecWithoutLoss.push_back(0);
    //}
    //initialize random weights between 0 and 1
    double r;
    srand (time(NULL));
    for(int k = 0; k < numComponents; k++) {
        //weightVecWithoutLoss.push_back(((double)rand())/(RAND_MAX));
        weightVecWithoutLoss.push_back(1.0);
    }
    // Verifying initial weight vector
    cout << "Verifying initial weight vector" << endl;
    for(int k = 0; k < numComponents; k++) {
        cout << weightVecWithoutLoss[k] << endl;
    }

    componentLookup = std::vector<std::string> (numComponents + 1, "");
    //cout << "Weight vector without loss is initialized to: ";
    //for (std::vector<char>::const_iterator i = weightVecWithoutLoss.begin(); i != weightVecWithoutLoss.end(); ++i)
    //    std::cout << *i << ' ';
    //cout << endl;
}

MixtureModel::MixtureModel(std::string modelFile, std::string b) {
    cout << "Loading from stored model" << endl;
    nlohmann::json model;
    std::ifstream modelJson;
    modelJson.open(modelFile);
    if(modelJson.fail()) { cout<<"Error opening file: "<< modelFile<<endl; exit(0);}
    modelJson >> model;
    components = model["components"];
    numComponents = 0;
    numModComponents = 0;
    int subNum = 0;
    for (auto it = components.begin(); it != components.end(); ++it)
    {
        std::cout << it.key() << " | " << it.value() << "\n";
        if(it.key()== "Mod") {
            subNum = 0;
            for (auto subit = components[it.key()].begin(); subit != components[it.key()].end(); ++subit) {
                cout << "Sub key " << subit.key()<< endl;
                subNum += (int)components[it.key()][subit.key()];
                cout << components[it.key()][subit.key()] << " components added for Mod:" << subit.key()<< endl;
            }
            numComponents += subNum;
            numModComponents += subNum;
        } else if(it.key()=="FL" || it.key()=="SC" || it.key()=="GC" || it.key()=="DM" || it.key()=="SeC" || it.key()=="PSC" || it.key()=="PS" || it.key()=="FB") {
            subNum = 1;
            bool enteredLoop = false;
            for (auto subit = components[it.key()].begin(); subit != components[it.key()].end(); ++subit) {
                enteredLoop = true;
                cout << "Sub key " << subit.key()<< endl;
                subNum *= components[it.key()][subit.key()].size();
                cout << "Found " << components[it.key()][subit.key()].size() << " components for " << it.key()<< ":" << subit.key()<< endl;
            }
            if(it.key() == "PS") {
                assert(1 == subNum);
                enteredLoop = true;
            }
            if(!enteredLoop) {
                cout<<"Empty component "<<it.key()<<endl;
                exit(0);
            }
            numComponents += subNum;
        } else {
            cout << "Unknown key in components JSON file " << it.key() << endl;
        }
    }
    cout << "Total number of components as per model = " << numComponents << endl;
    cout << "\nTotal number of modular components = " << numModComponents << endl;
    budget = b;
    cout << "Budget as per model = " << budget << endl;
    weightVecWithoutLoss = model["weights"].get<std::vector<double>>();
    componentLookup = model["component_lookup"].get<std::vector<std::string> >();
    assert(componentLookup.size() == numComponents + 1);
    assert(weightVecWithoutLoss.size() == numComponents);
    // cout << "Weight vector without loss is loaded as: ";
    // for (std::vector<char>::const_iterator i = weightVecWithoutLoss.begin(); i != weightVecWithoutLoss.end(); ++i)
    //     std::cout << *i << ' ';
    // cout << endl;
    epochNum = model["epoch_num"];
    train_category = model["category"];
    int numSamples = model["num_samples"]; //not doing anything with it right now
}

MixtureModel::~MixtureModel() {}

void MixtureModel::train(int num_epochs, nlohmann::json trainingData, std::string featuresFolder, nlohmann::json normData, std::string shotsFolder, nlohmann::json dataset, std::string gtFolder, std::string name, double marginWeight, bool useRandomGt, bool allowNegWeightForMod, nlohmann::json modelParams, bool useRandomGreedy, double lambda1, double lambda2, bool useAda, bool calculateSubModScores) {
    std::string outDirCommand = "mkdir " + name;
    system(outDirCommand.c_str());
    cout<<"Using a random GT per video: "<<useRandomGt<<endl;
    cout<<"Using randomGreedyMax: "<<useRandomGreedy<<endl;
    cout<<"Allow negative weights for modular componets: "<<allowNegWeightForMod<<endl;
    cout<<"Model params-> alpha: "<<modelParams["alpha"]<<", beta: "<<modelParams["beta"]<<", penalty: "<<modelParams["penalty"]<<endl;
    vector<vector<datk::SetFunctions*>> mixtureForAllVideos;
    vector<std::string> videoNames;
    vector<int> groundSetSizes;
    vector<double> deltaNorms;
    vector<double> testLosses;
    vector<double> trainingLosses;
    vector<double> scoreLosses;
    vector<double> marginLosses;
    int trainCalledBegin = 0;
    int trainCalledEnd = 0;
    int trainingBegin = 0;
    int trainingEnd = 0;
    int testingBegin = 0;
    int testingEnd = 0;
    int epochBegin = 0;
    int epochEnd = 0;
    int exampleBegin = 0;
    int exampleEnd = 0;

    int featuresAndMixturesForAllVideosBegin = 0;
    int featuresForThisVideoBegin = 0;
    int mixtureForThisVideoBegin = 0;
    int featuresAndMixturesForAllVideosEnd = 0;
    int featuresForThisVideoEnd = 0;
    int mixtureForThisVideoEnd = 0;
    int componentIndex = 0;

    int indexOfLossFunction;
    std::string cat = trainingData["train_category"];
    string train_category_bak = train_category;
    if(train_category == "") {
        train_category = cat;
    } else {
        if(train_category != cat) {
            cout << "WARNING!!! This model has been trained for " << train_category << " and you want me to now continue training on " << cat  << endl;
            assert(num_epochs == 0);
            train_category = cat;
        }
    }
    std::string trainFeaturesPath = featuresFolder + "/" + train_category;
    trainCalledBegin = clockInSec();
    vector<bool> negWeightsAllowed(numComponents, false);
    vector<bool> isLambdaOne(numComponents, false);
    double lambda;

    featuresAndMixturesForAllVideosBegin = clockInSec();
    for (auto it = trainingData["train_videos"].begin(); it != trainingData["train_videos"].end(); ++it) {//for each video in training data
        featuresForThisVideoBegin = clockInSec();
        nlohmann::json masterFeaturesJson;
        componentIndex = 0;
        int groundSetSize = 0;
        datk::Set groundSet;
        std::string video_name = *it;
        cout << "\nProcessing training video: " << video_name << " of category " << train_category << endl;
        cout << "\nLoading its features from " << trainFeaturesPath << endl;
        //TODO:see which features are needed based on desired components and load only them in memory
        //For now, loading all features in memory - create a master json for all features of this video in memory
        DIR* dirFile = opendir(trainFeaturesPath.c_str());
        //bool featureFileFound = false;
        if (dirFile) {
            //cout << "Opening " << featuresFolder << " looking for features" << endl;
            struct dirent* hFile;
            errno = 0;
            while (( hFile = readdir( dirFile )) != NULL )
            {
                //cout << "Checking " << hFile ->d_name << endl;
                //if(featureFileFound) continue;
                if ( !strcmp( hFile->d_name, "."  )) continue;
                if ( !strcmp( hFile->d_name, ".." )) continue;
                // in linux hidden files all start with '.'
                if (( hFile->d_name[0] == '.' )) continue;
                if ( strstr( hFile->d_name, (video_name+".").c_str() ) && strstr( hFile->d_name, "json" ) ) {
                    cout << "Found " << hFile->d_name << endl;
                    nlohmann::json currJson;
                    std::ifstream currJsonFile;
                    currJsonFile.open(trainFeaturesPath + "/" + hFile->d_name);
                    if(currJsonFile.fail()) { cout<<"Error opening file: "<< trainFeaturesPath + "/" + hFile->d_name <<endl; exit(0);}
                    currJsonFile >> currJson;
                    //masterFeaturesJson.insert(currJson.begin(), currJson.end());
                    //featureFileFound = true;
                    for (auto iter = currJson.begin(); iter != currJson.end(); ++iter) {
                        masterFeaturesJson[iter.key()] = iter.value();
                        cout << "Added " << iter.key() << " to master JSON of features" << endl;
                    }

                }
            }
            closedir( dirFile );
        }
        //if(!featureFileFound) {
        //    cout<<"No features found for video: "<<video_name<<endl;
        //    exit(0);
        //}
        featuresForThisVideoEnd = clockInSec();
        cout << "\nTime taken for loading features for this video: " << (featuresForThisVideoEnd-featuresForThisVideoBegin)<< endl;

        groundSetSize = masterFeaturesJson["num_features"];
        for(int i=0; i<groundSetSize; i++) {
            groundSet.insert(i);
        }

        double normalizationConstantMin = normData[video_name]["min_score"];
        double normalizationConstantMax = normData[video_name]["max_score"];
        double normalizationConstant = normalizationConstantMax - normalizationConstantMin;
        cout << "\nNormalization constant of this video = " << normalizationConstant << endl;
        mixtureForThisVideoBegin = clockInSec();
        //prepare the mixture for this video
        cout << "\nPreparing mixture\n" << endl;
        vector<datk::SetFunctions*> fvec;

        for (auto compit = components.begin(); compit != components.end(); ++compit) {
            std::cout << "Found: " << compit.key() << " | " << compit.value() << "\n";
            if(compit.key()=="SeC") {
                nlohmann::json object = components[compit.key()];
                cout << "SeC is present in components" << endl;
                std::vector<std::string> all_concepts = object["Concepts"];
                for (auto concept: all_concepts) { //for each required concept
                    std::cout << "Concept: " << concept << endl;
                    //look for that concept in master features json of this video
                    std::vector<std::vector<double>> concepts = masterFeaturesJson[concept];
                    //create appropriate data
                    // groundSetSize = 0;
                    std::vector<datk::Set> conceptVectors;
                    // datk::Set groundSet;
                    for (auto conceptVector: concepts) {
                        datk::Set currset;
                        for (int index = 0; index < conceptVector.size(); index++) {
                            if(conceptVector[index] > 0) currset.insert(index);
                        }
                        conceptVectors.push_back(currset);
                        // groundSet.insert(groundSetSize);
                        // groundSetSize++;
                    }
                    cout << "Ground set size for SeC " << concept << " for video " << video_name << " = " << groundSetSize << endl;
                    //create this component and add in the mixture for this video
                    datk::SetCover* setCover = new datk::SetCover(groundSetSize, conceptVectors);
                    double funVal = setCover->eval(groundSet);
                    lambda = abs(funVal) > 0  ? 1/funVal : 1;
                    cout << "F(V) for SeC for " << concept << " = " << 1/lambda << endl;
                    datk::ScaleSetFunctions* scaleSetFunction = new datk::ScaleSetFunctions(setCover, lambda);
                    fvec.push_back(scaleSetFunction);
                    cout << "\nAdded component SeC " << concept << " to mixture of video " << video_name << endl;
                    if(componentLookup[componentIndex]=="") {
                        cout << "*** First training video, componentLookup is empty, adding it's entry\n" << endl;
                        componentLookup[componentIndex] = "SeC:" + concept;
                        componentIndex++;
                    } else {
                        if(componentLookup[componentIndex]==("SeC:" + concept)) {
                            cout << "Order verified\n" << endl;
                            componentIndex++;
                        } else {
                            cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                            exit(0);
                        }
                    }
                }
            } else if(compit.key()=="PSC") {
                nlohmann::json object = components[compit.key()];
                cout << "PSC is present in components" << endl;
                std::vector<std::string> all_concepts = object["Concepts"];
                for (auto concept: all_concepts) { //for each required concept
                    std::cout << "Concept: " << concept << endl;
                    //look for that concept in master features json of this video
                    std::vector<std::vector<double>> probVectors = masterFeaturesJson[concept];
                    //create appropriate data
                    // groundSetSize = probVectors.size();
                    int numConcepts = probVectors[0].size();
                    // datk::Set groundSet;
                    // for(int i=0; i<groundSetSize; i++) {
                    //     groundSet.insert(i);
                    // }
                    cout << "Ground set size for PSC " << concept << " for video " << video_name << " = " << groundSetSize << endl;
                    //create this component and add in the mixture for this video
                    datk::ProbabilisticSetCover* probSC = new datk::ProbabilisticSetCover(groundSetSize, numConcepts, probVectors);
                    double funVal = probSC->eval(groundSet);
                    lambda = abs(funVal) > 0 ? 1/funVal : 1;
                    cout << "F(V) for PSC for " << concept << " = " << 1/lambda << endl;
                    datk::ScaleSetFunctions* probSCScaled = new datk::ScaleSetFunctions(probSC, lambda);
                    fvec.push_back(probSCScaled);
                    cout << "\nAdded component PSC " << concept << " to mixture of video " << video_name << endl;
                    if(componentLookup[componentIndex]=="") {
                        cout << "*** First training video, componentLookup is empty, adding it's entry\n" << endl;
                        componentLookup[componentIndex] = "PSC:" + concept;
                        componentIndex++;
                    } else {
                        if(componentLookup[componentIndex]==("PSC:" + concept)) {
                            cout << "Order verified\n" << endl;
                            componentIndex++;
                        } else {
                            cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                            exit(0);
                        }
                    }
                }
            } else if(compit.key()=="FL" || compit.key()=="SC" || compit.key()=="GC" || compit.key()=="DM") {
                nlohmann::json object = components[compit.key()];
                cout << compit.key()<< " is present in components" << endl;
                std::vector<std::string> all_features = object["Features"];
                std::vector<std::string> similarities = object["Kernels"];
                for (auto feature: all_features) { //for each required concept
                    for (auto similarity: similarities ) { //for each kernel type
                        if(similarity=="dot") {
                            std::cout << "Feature: " << feature << endl;
                            std::cout << "Similarity kernel: dot" << endl;
                            //look for this feature in master features json of this video
                            std::vector<std::vector<double>> features = masterFeaturesJson[feature];
                            //create appropriate data
                            std::vector<std::vector<float>> kernel;
                            // groundSetSize = features.size();
                            // datk::Set groundSet;
                            // for(int i=0; i<groundSetSize; i++) {
                            //     groundSet.insert(i);
                            // }
                            cout << "Computing kernel" << endl;
                            double sim = 0;
                            double a_norm = 0, b_norm = 0;
                            double min = std::numeric_limits<double>::max();
                            double max = std::numeric_limits<double>::min();
                            for (int i = 0; i < features.size(); i++) {
                                std::vector<float> currvector;
                                a_norm = sqrt(datk::innerProduct(features.at(i), features.at(i)));
                                for (int j = 0; j < features.size(); j++) {
                                    sim = datk::innerProduct(features.at(i), features.at(j));
                                    // Use true dot product
                                    b_norm = sqrt(datk::innerProduct(features.at(j), features.at(j)));
                                    sim =  (a_norm * b_norm) > 0 ? sim / (a_norm * b_norm): 0;
                                    sim = (1.0 + sim) / 2.0;
                                    if (sim < min) min = sim;
                                    if (sim > max) max = sim;
                                    currvector.push_back(sim);
                                }
                                kernel.push_back(currvector);
                            }
                            /*
                            //0-1 normalize using min and max - this becomes the kernel
                            double range = max - min;
                            for (int i = 0; i < features.size(); i++) {
                                for (int j = 0; j < features.size(); j++) {
                                    kernel[i][j] = (kernel[i][j] - min) / range;
                                }
                            }
                            */
                            cout << "Ground set size for FL/SC/GC/DM " << feature << " for video " << video_name << " = " << groundSetSize << endl;
                            if(compit.key()=="FL") {
                                datk::FacilityLocation* facLoc = new datk::FacilityLocation(groundSetSize, kernel);
                                double funVal = facLoc->eval(groundSet);
                                lambda = abs(funVal) > 0 ? 1/funVal : 1;
                                cout << "F(V) for FL " << feature << " and type dot = " << 1/lambda << endl;
                                datk::ScaleSetFunctions* facLocScaled = new datk::ScaleSetFunctions(facLoc, lambda);
                                fvec.push_back(facLocScaled);
                                cout << "\nAdded component FL " << feature << ":dot: to mixture of video " << video_name << endl;
                                if(componentLookup[componentIndex]=="") {
                                    cout << "*** First training video, componentLookup is empty, adding it's entry\n" << endl;
                                    componentLookup[componentIndex] = "FL:" + feature;
                                    componentIndex++;
                                } else {
                                    if(componentLookup[componentIndex]==("FL:" + feature)) {
                                        cout << "Order verified\n" << endl;
                                        componentIndex++;
                                    } else {
                                        cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                                        exit(0);
                                    }
                                }
                            } else if (compit.key()=="SC") {
                                datk::SaturateCoverage* satCov = new datk::SaturateCoverage(groundSetSize, kernel, 0.1); //TODO: is 0.1 OK?
                                double funVal  = satCov->eval(groundSet);
                                lambda = abs(funVal) > 0 ? 1/funVal : 1;
                                cout << "F(V) for SC " << feature << " and type dot = " << 1/lambda << endl;
                                datk::ScaleSetFunctions* satCovScaled = new datk::ScaleSetFunctions(satCov, lambda);
                                fvec.push_back(satCovScaled);
                                cout << "\nAdded component SC " << feature << ":dot: to mixture of video " << video_name << endl;
                                if(componentLookup[componentIndex]=="") {
                                    cout << "*** First training video, componentLookup is empty, adding it's entry\n" << endl;
                                    componentLookup[componentIndex] = "SC:" + feature;
                                    componentIndex++;
                                } else {
                                    if(componentLookup[componentIndex]==("SC:" + feature)) {
                                        cout << "Order verified\n" << endl;
                                        componentIndex++;
                                    } else {
                                        cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                                        exit(0);
                                    }
                                }
                            } else if(compit.key()=="GC") {
                                datk::GraphCutFunctions* graphCut = new datk::GraphCutFunctions(groundSetSize, kernel, 0.5); //TODO: is 0.5 ok?
                                double funVal = graphCut->eval(groundSet);
                                lambda = abs(funVal) > 0 ? 1/funVal : 1;
                                cout << "F(V) for GC " << feature << " and type dot = " << 1/lambda << endl;
                                datk::ScaleSetFunctions* graphCutScaled = new datk::ScaleSetFunctions(graphCut, lambda);
                                fvec.push_back(graphCutScaled);
                                cout << "\nAdded component GC " << feature << ":dot: to mixture of video " << video_name << endl;
                                if(componentLookup[componentIndex]=="") {
                                    cout << "*** First training video, componentLookup is empty, adding it's entry\n" << endl;
                                    componentLookup[componentIndex] = "GC:" + feature;
                                    componentIndex++;
                                } else {
                                    if(componentLookup[componentIndex]==("GC:" + feature)) {
                                        cout << "Order verified\n" << endl;
                                        componentIndex++;
                                    } else {
                                        cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                                        exit(0);
                                    }
                                }
                            } else if(compit.key()=="DM") {
                                datk::DisparityMin* dispMin = new datk::DisparityMin(groundSetSize, kernel);
                                //lambda = 1/dispMin.eval(groundSet);
                                //lambda = 1;
                                //cout << "F(V) for DM " << feature << " and type dot = " << 1/lambda << endl;
                                datk::ScaleSetFunctions* dispMinScaled = new datk::ScaleSetFunctions(dispMin, 1);
                                fvec.push_back(dispMinScaled);
                                cout << "\nAdded component DM " << feature << ":dot: to mixture of video " << video_name << endl;
                                if(componentLookup[componentIndex]=="") {
                                    cout << "*** First training video, componentLookup is empty, adding it's entry at " << componentIndex << "\n" << endl;
                                    componentLookup[componentIndex] = "DM:" + feature;
                                    componentIndex++;
                                } else {
                                    cout << "Looking at " << componentIndex << endl;
                                    cout << "Found " << componentLookup[componentIndex] << endl;
                                    if(componentLookup[componentIndex]==("DM:" + feature)) {
                                        cout << "Order verified\n" << endl;
                                        componentIndex++;
                                    } else {
                                        cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                                        exit(0);
                                    }
                                }
                            } else {
                                cout << "This is impossible - something other than FL/GC/DM/SC within FL/GC/DM/SC!!!!" << endl;
                                exit(0);
                            }
                        } else{
                            cout << "Similarity kernel " << similarity << " is not yet implemented";
                            exit(0);
                            //TODO: numComponents--; but then how about creating new vs load?
                        }
                    }
                }
            } else if (compit.key()=="PS") {
                nlohmann::json object = components[compit.key()];
                cout << compit.key()<< " is present in components" << endl;
                //create appropriate data
                cout << "Browsing shots folder to get shots and snippets information" << endl;
                nlohmann::json shotsJson;
                DIR* dirFile = opendir(shotsFolder.c_str());
                if (dirFile) {
                    cout << "Opening " << shotsFolder << " looking for shots info of this video" << endl;
                    struct dirent* hFile;
                    errno = 0;
                    bool shotsFileFound = false;
                    while (( hFile = readdir( dirFile )) != NULL )
                    {
                        //cout << "Checking " << hFile ->d_name << endl;
                        if(shotsFileFound) continue;
                        if ( !strcmp( hFile->d_name, "."  )) continue;
                        if ( !strcmp( hFile->d_name, ".." )) continue;
                        // in linux hidden files all start with '.'
                        if (   ( hFile->d_name[0] == '.' )) continue;
                        if ( strstr( hFile->d_name, (video_name+"_").c_str() ) && strstr( hFile->d_name, "shots" ) && strstr( hFile->d_name, "json" ) ) {
                            //cout << "This shots file belongs to this video, opening and parsing it" << endl;
                            cout << "Found " << hFile->d_name << endl;
                            std::ifstream shotsJsonFile;
                            shotsJsonFile.open(shotsFolder + hFile->d_name);
                            if(shotsJsonFile.fail()) { cout<<"Error opening file: "<< shotsFolder + hFile->d_name <<endl; exit(0);}
                            shotsFileFound = true;
                            shotsJsonFile >> shotsJson;
                        }
                    }
                    if(!shotsFileFound) {
                        cout<<"Shots json not found for video: "<<video_name<<endl;
                        exit(0);
                    }
                    closedir( dirFile );
                }
                // groundSetSize = shotsJson.size();
                std::vector<std::vector<float>> kernel;
                // datk::Set groundSet;
                // for(int i=0; i<groundSetSize; i++) {
                //     groundSet.insert(i);
                // }
                cout << "Number of snippets for video " << video_name << " according to shots JSON = " << shotsJson.size() << endl;
                cout << "Computing kernel" << endl;
                double sim = 0;
                double min = std::numeric_limits<double>::max();
                double max = std::numeric_limits<double>::min();
                for (int i = 0; i < groundSetSize; i++) {
                    std::vector<float> currvector;
                    for (int j = 0; j < groundSetSize; j++) {
                        if(i==j) sim = 1;
                        else {
                            if(shotsJson[std::to_string(i)] == shotsJson[std::to_string(j)]) {
                                sim = 1.0 - (abs(j - i) / groundSetSize);
                            } else sim = 0;
                        }
                        currvector.push_back(sim);
                    }
                    kernel.push_back(currvector);
                }
                //0-1 normalize using min and max - this becomes the kernel
                //max = 1;
                //min = 1/(groundSetSize - 1);
                //double range = max - min;
                //for (int i = 0; i < groundSetSize; i++) {
                //    for (int j = 0; j < groundSetSize; j++) {
                //        kernel[i][j] = (kernel[i][j] - min) / range;
                //    }
                //}
                cout << "Ground set size for PS for video " << video_name << " = " << groundSetSize << endl;
                datk::PairwiseSum* pair = new datk::PairwiseSum(groundSetSize, kernel);
                double funVal = pair->eval(groundSet);
                lambda = abs(funVal) > 0 ? 1/funVal : 1;
                cout << "F(V) for PS = " << 1/lambda << endl;
                datk::ScaleSetFunctions* pairScaled = new datk::ScaleSetFunctions(pair, lambda);
                fvec.push_back(pairScaled);
                cout << "\nAdded component PS to mixture of video " << video_name << endl;
                if(componentLookup[componentIndex]=="") {
                    cout << "*** First training video, componentLookup is empty, adding it's entry\n" << endl;
                    componentLookup[componentIndex] = "PS";
                    componentIndex++;
                } else {
                    if(componentLookup[componentIndex]=="PS") {
                        cout << "Order verified\n" << endl;
                        componentIndex++;
                    } else {
                        cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                        exit(0);
                    }
                }
            } else if(compit.key()=="FB") {
                nlohmann::json object = components[compit.key()];
                cout << compit.key()<< " is present in components" << endl;
                std::vector<std::string> all_features = object["Features"];
                std::vector<int> types = object["Types"];
                for (auto feature: all_features) { //for each required feature
                    std::vector<std::vector<double>> features = masterFeaturesJson[feature];
                    vector<datk::SparseFeature> features_input;
                    int numFeatures = features[0].size();
                    for (auto featureVector: features) {
                        datk::SparseFeature s;
                        for (int index = 0; index < featureVector.size(); index++) {
                            if(featureVector[index] != 0) {
                                s.featureIndex.push_back(index);
                                s.featureVec.push_back(featureVector[index]);
                            }
                        }
                        s.numFeatures = numFeatures;
                        s.numUniqueFeatures = s.featureIndex.size();
                        features_input.push_back(s);
                    }
                    vector<double> featureWeights(numFeatures, 1);
                    //vector<datk::SparseFeature> features_input;
                    //int numFeatures = 0;
                    //groundSetSize = 0;
                    //for (auto featureVector: features) {
                    //    datk::SparseFeature s;
                    //    double a;
                    //    numFeatures = 0;
                    //    for (int index = 0; index < featureVector.size(); index++) {
                    //        if(featureVector[index] != 0) {
                    //            s.featureIndex.push_back(numFeatures);
                    //            s.featureVec.push_back(featureVector[index]);
                    //        }
                    //        numFeatures++;
                    //    }
                    //    s.numFeatures = numFeatures;
                    //    s.numUniqueFeatures = s.featureIndex.size();
                    //    cout << "Sparse feature constructed as: ";
                    //    for (auto i: s.featureIndex)
                    //        std::cout << i << ' ';
                    //    std::cout << endl;
                    //    for (auto i: s.featureVec)
                    //        std::cout << i << ' ';
                    //    std::cout << endl;
                    //    features_input.push_back(s);
                    //    groundSet.insert(groundSetSize);
                    //    groundSetSize++;
                    //}
                    //vector<double> featureWeights(numFeatures, 1);
                    cout << "Ground set size for FB " << feature << " for video " << video_name << " = " << groundSetSize << endl;
                    for (auto type: types ) { //for each type of concave function
                        //1:sqrt, 2:inverse, 3:log, 4:min
                        //create this component and add in the mixture for this video
                        cout << "Adding component FB type " << type << endl;
                        datk::FeatureBasedFunctions* fbx = new datk::FeatureBasedFunctions(groundSetSize, type, features_input, featureWeights, 0.2);
                        double tmp = fbx->eval(groundSet);
                        cout << "F(V) for FB DIRECT" << feature << " and type " << type << " = " << tmp << endl;
                        double funVal = fbx->eval(groundSet);
                        lambda = abs(funVal) > 0 ? 1/funVal : 1;
                        cout << "F(V) for FB " << feature << " and type " << type << " = " << 1/lambda << endl;
                        datk::ScaleSetFunctions* fbxScaled = new datk::ScaleSetFunctions(fbx, lambda);
                        fvec.push_back(fbxScaled);
                        cout << "\nAdded component FB " << feature << " of type " << type << " to mixture of video " << video_name << endl;
                        if(componentLookup[componentIndex]=="") {
                            cout << "*** First training video, componentLookup is empty, adding it's entry at " << componentIndex << "\n" << endl;
                            componentLookup[componentIndex] = "FB:" + feature + ":" + to_string(type);
                            componentIndex++;
                        } else {
                            cout << "Found " << componentLookup[componentIndex] << " at " << componentIndex <<  endl;
                            if(componentLookup[componentIndex]==("FB:" + feature + ":" + to_string(type))) {
                                cout << "Order verified\n" << endl;
                                componentIndex++;
                            } else {
                                cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                                exit(0);
                            }
                        }
                    }
                }
            } else if(compit.key()=="Mod") {
                nlohmann::json object = components[compit.key()];
                cout << compit.key()<< " is present in components" << endl;
                for (auto compsubit = object.begin(); compsubit != object.end(); ++compsubit) {
                    cout << compsubit.key()<< " is present in Mod" << endl;
                    std::vector<std::vector<double>> features = masterFeaturesJson[compsubit.key()];
                    // groundSetSize = features.size();
                    int numFeatures = compsubit.value();
                    // datk::Set groundSet;
                    // for(int i=0; i<groundSetSize; i++) {
                    //     groundSet.insert(i);
                    // }
                    for(int i=0; i<numFeatures; i++) {
                        vector<double> weight;
                        double fv = 0;
                        for(int j=0; j<groundSetSize; j++) {
                            weight.push_back(features[j][i]);
                            fv += abs(features[j][i]);
                        }
                        //cout << "Ground set size for Mod " << compsubit.key()<< " and index " << i << " for video " << video_name << " = " << groundSetSize << endl;
                        datk::ModularFunctions* mf = new datk::ModularFunctions(weight, 0);
                        //lambda = 1/mf->eval(groundSet);
                        //cout << "F(V) for Mod " << compsubit.key()<< " and index " << i << " = " << fv << endl;
                        lambda = ((fv == 0)?1:1/fv);
                        datk::ScaleSetFunctions* mfScaled = new datk::ScaleSetFunctions(mf, lambda);
                        fvec.push_back(mfScaled);
                        if(allowNegWeightForMod) {
                            negWeightsAllowed[componentIndex] = true;
                        }
                        isLambdaOne[componentIndex] = true;
                        //cout << "\nAdded component Mod " << compsubit.key()<< " of index " << i << " to mixture of video " << video_name << endl;
                        if(componentLookup[componentIndex]=="") {
                            //cout << "*** First training video, componentLookup is empty, adding it's entry\n" << endl;
                            componentLookup[componentIndex] = "Mod:" + compsubit.key()+ ":" + to_string(i);
                            componentIndex++;
                        } else {
                            if(componentLookup[componentIndex]==("Mod:" + compsubit.key()+ ":" + to_string(i))) {
                                //cout << "Order verified\n" << endl;
                                componentIndex++;
                            } else {
                                cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                                exit(0);
                            }
                        }
                    }
                }
            } else {
                cout << "Unrecognized key in components JSON file" << endl;
                exit(0);
            }
        }
        //create loss function component, look up normalization constant and add in the mixture for this video
        cout<<"All normal components added."<<endl;
        assert(numComponents == componentIndex);
        nlohmann::json annotation;
        std::string annotationJsonFile = dataset["root"].get<string>() + train_category + "/" + dataset["categories"][train_category][video_name]["annotation"].get<string>();
        std::ifstream annotationJson;
        annotationJson.open(annotationJsonFile);
        if(annotationJson.fail()) { cout<<"Error opening file:"<<annotationJsonFile <<endl; exit(0);}
        annotationJson >> annotation;
        cout<<"opened"<<endl;

        // groundSetSize = masterFeaturesJson["vgg_features"].size();
        std::vector<datk::Set> segments;
        std::vector<int> ratings;
        std::vector<bool> isRedundant;
        double alpha = modelParams["alpha"];//1;
        double beta = modelParams["beta"];//6;
        double penalty = modelParams["penalty"];//2;
        int segIndex = 0;
        int maxSnippetId = -1;
        for(auto seg : annotation) {
            datk::Set snippets;
            string line = seg["begin"];
            std::vector <string> tokens;
            stringstream check1(line);
            string intermediate;
            while(getline(check1, intermediate, ':')) {
                tokens.push_back(intermediate);
            }
            int hrs = stoi(tokens[0]);
            int mins = stoi(tokens[1]);
            int secs = stoi(tokens[2]);
            float beginSeconds=3600*hrs+60*mins+secs;
            line = seg["end"];
            stringstream check2(line);
            tokens.clear();
            while(getline(check2, intermediate, ':')) {
                tokens.push_back(intermediate);
            }
            hrs = stoi(tokens[0]);
            mins = stoi(tokens[1]);
            secs = stoi(tokens[2]);
            float endSeconds=3600*hrs+60*mins+secs;
            //add snippet ids to this set
            int beginSnippetId = ceil(beginSeconds/2.0);
            int endSnippetId =floor((endSeconds-1)/2.0);
            //for last end, if odd
            if (endSnippetId == groundSetSize) endSnippetId --;
            if (endSnippetId > maxSnippetId) maxSnippetId = endSnippetId;
            for(int id = beginSnippetId; id <= endSnippetId; id++) {
                snippets.insert(id);
            }
            segments.push_back(snippets);
            ratings.push_back(seg["rating"]);
            bool isRepetitive = false;
            if(seg.find("repetitive") == seg.end()) {
                isRepetitive = false;
            } else {
                if(seg["repetitive"] == true) isRepetitive = true;
                else isRepetitive = false;
            }
            isRedundant.push_back(isRepetitive);
        }
        cout << "Max snippet id = " << maxSnippetId << endl;
        datk::VideoSummLoss* vsl = new datk::VideoSummLoss(groundSetSize, segments, ratings, isRedundant, alpha, beta, penalty, normalizationConstantMax);
        lambda = 1/normalizationConstant;
        //lambda = 1/normalizationConstant;
        cout << "Greedy max of VSL for video " << video_name << " = " << normalizationConstant << endl;
        datk::ScaleSetFunctions* vslScaled = new datk::ScaleSetFunctions(vsl, lambda);
        fvec.push_back(vslScaled);
        cout << "\nAdded component VSL to mixture of video " << video_name << endl;
        if(componentLookup[componentIndex]=="") {
            cout << "*** First training video, componentLookup is empty, adding it's entry\n" << endl;
            componentLookup[componentIndex] = "VSL";
            indexOfLossFunction = componentIndex;
            cout << "Index of loss function = " << indexOfLossFunction << endl;
            componentIndex++;
        } else {
            if(componentLookup[componentIndex]=="VSL") {
                cout << "Order verified\n" << endl;
                indexOfLossFunction = componentIndex;
                cout << "Index of loss function = " << indexOfLossFunction << endl;
                componentIndex++;
            } else {
                cout << "This video's component order seems to be different from previous video's component order!!! WARNING!!!" << endl;
                exit(0);
            }
        }
        cout << "\nAdding the complete mixture for this video " << video_name << " to the vector of mixtures of all videos" << endl;
        mixtureForAllVideos.push_back(fvec);
        videoNames.push_back(video_name);
        groundSetSizes.push_back(groundSetSize);
        mixtureForThisVideoEnd = clockInSec();
        //cout << "Component lookup array: " << endl;
        //for(int i =0; i< componentIndex; i++) {
        //    cout << componentLookup[i] << endl;
        //}
        cout << "Time taken for creating mixture for this video: " << (mixtureForThisVideoEnd-mixtureForThisVideoBegin)<< endl;
    }
    featuresAndMixturesForAllVideosEnd = clockInSec();
    cout << "Time taken for loading features and creating mixture for all training videos: " << (featuresAndMixturesForAllVideosEnd-featuresAndMixturesForAllVideosBegin)<< endl;

    cout << "\n****Assertions****\n" << endl;
    cout << "Size of mixtureForAllVideos: " << mixtureForAllVideos.size() << endl;
    cout << "Size of videoNames: " << videoNames.size() << endl;
    cout << "Size of componentLookup: " << componentIndex << endl;
    cout << "Size of groundSetSizes: " << groundSetSizes.size() << endl;

    if(num_epochs <=0 ) goto TestLabel;

    //count number of ground truths per video
    //cout << "\nComputing the number of samples that will take part in training..." << endl;
    int numSamples = 0;
    int currentCount = 0;
    std::string video_name;
    std::vector<int> numSamplesPerVideo;
    for (int index = 0; index < videoNames.size(); index++) {//for each video in training data
        currentCount = 0;
        video_name = videoNames[index];
        cout << "Scanning ground truths of video " << video_name << endl;
        std::string video_gt_path = gtFolder + "/" + train_category + "/" + video_name + "/" + budget;
        DIR* dirFile = opendir(video_gt_path.c_str());
        if (dirFile) {
            cout << "Opening " << video_gt_path << " looking for ground truth summaries of " << video_name << " for budget " << budget << endl;
            struct dirent* hFile;
            errno = 0;
            while (( hFile = readdir( dirFile )) != NULL )
            {
              //cout << "File name " << hFile ->d_name << endl;
              if ( !strcmp( hFile->d_name, "."  )) continue;
              if ( !strcmp( hFile->d_name, ".." )) continue;
              // in linux hidden files all start with '.'
              if (   ( hFile->d_name[0] == '.' )) continue;
              if ( strstr( hFile->d_name, video_name.c_str() ) && strstr( hFile->d_name, "gt" ) && strstr(hFile->d_name, "json") ) {
                    //cout << "This gt file belongs to this video, counting it" << endl;
                    cout << "Found " << hFile->d_name << endl;
                    numSamples++;
                    currentCount++;
                }
            }
            closedir( dirFile );
        } else {
            cout << "Not able to count ground truth summaries for " << video_name << endl;
            exit(0);
        }
        cout << "\nNumber of GTs for video " << video_name << " = " << currentCount << endl;
        if(currentCount == 0) {
            cout<< "Zero GTs found. Exiting.";
            exit(0);
        }
        numSamplesPerVideo.push_back(currentCount);
    }
    assert(numSamplesPerVideo.size() == videoNames.size());
    //cout << "\n\nTotal number of training samples = " << numSamples << endl;
    trainingBegin = clockInSec();
    cout << "Begin training ..." << endl;
    int epoch = 1;
    int budgetNumSnippets = 0;
    int duration = 0;
    std::vector<double> weightVecWithLoss;
    std::vector<double> tempWeightVecWithoutLoss;
    std::vector<int> randomVideoIndices(videoNames.size());
    std::iota(std::begin(randomVideoIndices), std::end(randomVideoIndices), 0);
    int tmp;

    // unsigned int randomVideoInd = rand() % videoNames.size();
    // std::string randomVideoName = videoNames[randomVideoInd];
    // unsigned int randomVideoDur = int(dataset["categories"][train_category][randomVideoName]["duration"]);
    // randomVideoDur = randomVideoDur % 2 == 0 ? randomVideoDur : randomVideoDur - 1;
    // unsigned int randomSnippetInd = rand() % (randomVideoDur / 2);
    // datk::Set randomCandidateSet;
    // randomCandidateSet.insert(randomSnippetInd);

    LargeMarginMixtureLearningL2Regularized largeMargin(numComponents, videoNames.size(), num_epochs, weightVecWithoutLoss, lambda1, lambda2, useAda);
    //double largeMarginLambda = largeMargin.get_lambda();
    vector<int> gtIndexToPick(videoNames.size(), 0);
    for(int epoch = 1; epoch <= num_epochs; epoch++) { //for each epoch
        cout << "\n********** Epoch no. " << epoch << " **********\n" << endl;
        epochBegin = clockInSec();
        double epochTrainLoss = 0;
        double epochScoreLoss = 0;
        double epochDeltaNorm = 0;
        double epochMarginLoss = 0;
        std::shuffle(randomVideoIndices.begin(), randomVideoIndices.end(), std::mt19937{std::random_device{}()});
        for (int indexPos = 0; indexPos < videoNames.size(); indexPos++) {//for each video in training data
            exampleBegin = clockInSec();
            int index = randomVideoIndices[indexPos];
            video_name = videoNames[index];
            cout << "Beginning training with video: " << video_name << endl;
            duration = dataset["categories"][train_category][video_name]["duration"];
            tmp = int(std::stof(budget)*duration);
            if(tmp%2 == 1) tmp = tmp - 1;
            budgetNumSnippets = tmp/2;
            cout << "Video duration = " << duration << " hence number of snippets expected = " << budgetNumSnippets << endl;
            //for each ground truth of that video of this budget
            if(1 == epoch || useRandomGt) {
                gtIndexToPick[index] = rand() % numSamplesPerVideo[index];
            }
            cout<< "Using random gt number: "<<gtIndexToPick[index]<<" for video "<<video_name<<" at epoch: "<<epoch<<endl;
            std::string video_gt_path = gtFolder + "/" + train_category + "/" + video_name + "/" + budget;
            std::string gt_file_name = video_gt_path + "/" + train_category + "_" + video_name + "_" + "gt" + "_" + budget + "_" + std::to_string(gtIndexToPick[index]) + ".json";
            cout << "Opening " << gt_file_name << endl;

            nlohmann::json gtJson;
            std::ifstream gtJsonFile;
            gtJsonFile.open(gt_file_name);
            if(gtJsonFile.fail()) { cout<<"Error opening file:"<<gt_file_name <<endl; exit(0);}
            gtJsonFile >> gtJson;
            cout << "\nTraining example no. " << epoch << ":" << index << ":" << gtIndexToPick[index]<< endl;

            //create the weighted mixture as per current weights
            weightVecWithLoss = weightVecWithoutLoss;
            weightVecWithLoss.push_back(marginWeight); //for loss component
            //cout << "weightVecWithoutLoss:" << endl;
            //for (auto i : weightVecWithoutLoss) {
            //    cout << i << " ";
            //}
            //cout << endl;

            //cout << "weightVecWithLoss:" << endl;
            //for (auto i : weightVecWithLoss) {
            //    cout << i << " ";
            //}
            //cout << endl;
            datk::CombineSetFunctions fComb(groundSetSizes[index], mixtureForAllVideos[index], weightVecWithLoss);
            //compute y_cand using naive greedy maximization of this mixture
            datk::Set candidateIndices;
            nlohmann::json gtSnippets = gtJson["summary"];
            datk::Set gtIndices;
            for(auto snippet : gtSnippets) {
                gtIndices.insert(snippet["begin"].get<int>()/2);
            }
            //cout << "Begin naiveGreedyMax" << endl;
            budgetNumSnippets = gtIndices.size();
            if(useRandomGreedy) {
                datk::randomGreedyMax(fComb, budgetNumSnippets, candidateIndices, 0 , false, true);
            } else {
                datk::naiveGreedyMax(fComb, budgetNumSnippets, candidateIndices, 0, false, true);
            }
            cout << "Size of optimum set = " << candidateIndices.size() << endl;
            if(budgetNumSnippets != candidateIndices.size()) {
                cout << "**** CAUTION - we expected a different size!" << endl;
            }
            //cout << "End naiveGreedyMax" << endl;
            //compute our score of y_cand and y_gt, store loss, update plot
            double candidateVSLScore = mixtureForAllVideos[index][indexOfLossFunction]->eval(candidateIndices);
            double gtVSLScore = mixtureForAllVideos[index][indexOfLossFunction]->eval(gtIndices);

            double normalizedCandidateScore = 1.0 - candidateVSLScore;
            // double normalizedCandidateScore = (1/normalization[video_name])*candidateScore;
            //cout << "Normalized candidate score = " << normalizedCandidateScore << endl;
            double normalizedGtScore = 1.0 - gtVSLScore;
            // double normalizedGtScore = (1/normalization[video_name])*gtScore;
            cout << "Normalized gt score = " << normalizedGtScore << endl;
            cout << "Normalized candidate score = " << normalizedCandidateScore << endl;
            //cout << "***Training loss = " << normalizedGtScore - normalizedCandidateScore << endl;
            //compute y_gt array as values of y_gt by each component, multiple by the corresponding weights
            std::vector<double> y_max;
            std::vector<double> y_gt;
            double trainLoss = 0;
            double diffLoss = 0;
            double regLoss = 0;
            double marginLoss = 0;
            for(int k=0; k < numComponents; k++) {
                y_max.push_back(mixtureForAllVideos[index][k]->eval(candidateIndices));
                y_gt.push_back(mixtureForAllVideos[index][k]->eval(gtIndices));
                diffLoss += weightVecWithoutLoss[k] * (y_max[k] - y_gt[k]);
                if(isLambdaOne[k] == true) {
                    regLoss += (lambda1 / 2.0) * weightVecWithLoss[k] * weightVecWithLoss[k];
                } else {
                    regLoss += (lambda2 / 2.0) * weightVecWithLoss[k] * weightVecWithLoss[k];
                }
                //regLoss += (largeMarginLambda / 2.0) * weightVecWithLoss[k] * weightVecWithLoss[k];
            }
            marginLoss = marginWeight * candidateVSLScore;
            trainLoss = diffLoss + regLoss + marginLoss;
            epochTrainLoss += trainLoss;
            epochScoreLoss += normalizedGtScore - normalizedCandidateScore;
            epochMarginLoss += marginLoss;
            //trainingLosses.push_back(trainLoss);

            cout<<"Train loss: "<< trainLoss <<"; Diff Loss: "<<diffLoss<<"; Margin loss: "<<marginLoss<<"; Reg Loss: "<<regLoss<<endl;
            //update weights
            //cout << "Calling update weights" << endl;
            largeMargin.update_weights(y_max, y_gt, negWeightsAllowed, isLambdaOne);
            tempWeightVecWithoutLoss = largeMargin.get_weights();
            double deltaNorm = 0;
            double delta;
            for(auto i : weightVecWithoutLoss) {
                delta = weightVecWithoutLoss[i] - tempWeightVecWithoutLoss[i];
                deltaNorm += delta * delta;
            }
            //cout << "***Delta norm non sq" << deltaNorm << endl;
            deltaNorm = sqrt(deltaNorm);
            epochDeltaNorm += deltaNorm;
            cout << "***Delta norm = " << deltaNorm << endl;
            weightVecWithoutLoss = largeMargin.get_weights();
            //deltaNorms.push_back(deltaNorm);
            //cout << "Updated the weight vector" << endl;
            exampleEnd = clockInSec();
            cout << "Time taken for this example: " << (exampleEnd-exampleBegin) << endl << endl;
        }
        epochTrainLoss = epochTrainLoss / videoNames.size();
        epochScoreLoss = epochScoreLoss / videoNames.size();
        epochDeltaNorm = epochDeltaNorm / videoNames.size();
        epochMarginLoss = epochMarginLoss / videoNames.size();
        trainingLosses.push_back(epochTrainLoss);
        deltaNorms.push_back(epochDeltaNorm);
        scoreLosses.push_back(epochScoreLoss);
        marginLosses.push_back(epochMarginLoss);
        epochNum++;
        epochEnd = clockInSec();
        cout << "\n\nTime taken for this epoch: " << (epochEnd-epochBegin)<< endl;
        //store this model with all details
        cout << "Epoch number " << epoch << " complete, saving the model before continuing" << endl;
        //weightVecWithoutLoss = largeMargin.get_weights();
        nlohmann::json thisModel;
        thisModel["components"] = components;
        thisModel["component_lookup"] = componentLookup;
        thisModel["budget"] = budget;
        thisModel["weights"] = weightVecWithoutLoss;
        thisModel["epoch_num"] = epochNum;
        thisModel["category"] = train_category;
        thisModel["num_samples"] = numSamples;
        std::ofstream file(name + "/" + train_category + "_" + budget + "_model_" + to_string(epoch) + ".json");
        file << thisModel;

        cout << "Saving losses and deltas for this epoch" << endl;
        nlohmann::json losses;
        losses["training_losses"] = trainingLosses;
        losses["margin_losses"] = marginLosses;
        losses["delta_norms"] = deltaNorms;
        losses["score_losses"] = scoreLosses;
        losses["category"] = train_category;
        losses["budget"] = budget;
        losses["num_samples"] = numSamples;
        losses["epoch_num"] = epochNum;
        std::ofstream file2(name + "/" + train_category + "_" + budget + "_losses_" + to_string(epoch) + ".json");
        file2 << losses;

        /*cout<<endl<<"Printing component scores for random video:"<<randomVideoName<<" with random snippetId:"<<randomSnippetInd<<"."<<endl;
        for(int k=0; k < numComponents; k++) {
            std::string compName = componentLookup[k].c_str();
            double compRawScore = mixtureForAllVideos[randomVideoInd][k]->eval(randomCandidateSet);
            double compWeight = weightVecWithoutLoss[k];
            double weightedCompScore = compWeight * compRawScore;
            cout<<"Component:"<<compName<<", Raw Score:"<<compRawScore<<", Weight:"<<compWeight<<", Weigthted Score:"<<weightedCompScore<<"."<<endl;
        }*/

    }
    cout << "\n*******************Training Complete********************\n" << endl;
    trainingEnd = clockInSec();
    cout << "Time taken for complete training: " << (trainingEnd-trainingBegin) << endl;

    //report weights learnt at the end of training
    int weightIndex = 0;
    WeightAndComponent weightsAndComponents[numComponents];
    double normModVGGF = 0.0;
    double normModGoogleNetF = 0.0;
    double normModColorR = 0.0;
    double normModColorG = 0.0;
    double normModColorB = 0.0;
    double normModColorH = 0.0;
    double normModColorS = 0.0;
    double normModVGGC = 0.0;
    double normModGoogleNetC = 0.0;
    double normModYoloVoc = 0.0;
    double normModYoloCoco = 0.0;
    double normModVGGP = 0.0;
    double normModGoogleNetP = 0.0;
    double normModYoloVocP = 0.0;
    double normModYoloCocoP = 0.0;
    bool isnormModVGGF = false;
    bool isnormModGoogleNetF = false;
    bool isnormModColorR = false;
    bool isnormModColorG = false;
    bool isnormModColorB = false;
    bool isnormModColorH = false;
    bool isnormModColorS = false;
    bool isnormModVGGC = false;
    bool isnormModGoogleNetC = false;
    bool isnormModYoloVoc = false;
    bool isnormModYoloCoco = false;
    bool isnormModVGGP = 0.0;
    bool isnormModGoogleNetP = 0.0;
    bool isnormModYoloVocP = 0.0;
    bool isnormModYoloCocoP = 0.0;

    int sortArrayIndex = 0;
    double maxConceptWeight = std::numeric_limits<double>::min();
    string maxConcept;
    for(int weightIndex = 0; weightIndex < weightVecWithoutLoss.size(); weightIndex++) {
        if(strstr(componentLookup[weightIndex].c_str(), "concepts")){
          if(weightVecWithoutLoss[weightIndex] > maxConceptWeight){
            maxConceptWeight = weightVecWithoutLoss[weightIndex];
            maxConcept = componentLookup[weightIndex];
          }
        }
        if(strstr(componentLookup[weightIndex].c_str(), "Mod:vgg_features")) {
            normModVGGF += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModVGGF = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:googlenet_features")) {
            normModGoogleNetF += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModGoogleNetF = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:color_hist_r_features")) {
            normModColorR += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModColorR = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:color_hist_g_features")) {
            normModColorG += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModColorG = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:color_hist_b_features")) {
            normModColorB += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModColorB = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:color_hist_h_features")) {
            normModColorH += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModColorH = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:color_hist_s_features")) {
            normModColorS += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModColorS = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:vgg_concepts")) {
            normModVGGC += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModVGGC = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:googlenet_concepts")) {
            normModGoogleNetC += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModGoogleNetC = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:yolo_voc_concepts")) {
            normModYoloVoc += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModYoloVoc = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:yolo_coco_concepts")) {
            normModYoloCoco += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModYoloCoco = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:vgg_p_concepts")) {
            normModVGGP += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModVGGP = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:googlenet_p_concepts")) {
            normModGoogleNetP += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModGoogleNetP = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:yolo_voc_p_concepts")) {
            normModYoloVocP += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModYoloVocP = true;
        } else if(strstr(componentLookup[weightIndex].c_str(), "Mod:yolo_coco_p_concepts")) {
            normModYoloCocoP += pow(weightVecWithoutLoss[weightIndex], 2);
            isnormModYoloCoco = true;
        } else {
            weightsAndComponents[sortArrayIndex].weight = weightVecWithoutLoss[weightIndex];
            weightsAndComponents[sortArrayIndex].componentName = componentLookup[weightIndex];
            sortArrayIndex++;
        }
    }
    if(isnormModVGGF) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModVGGF);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:vgg_features";
        sortArrayIndex++;
    }
    if(isnormModVGGC) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModVGGC);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:vgg_concepts";
        sortArrayIndex++;
    }
    if(isnormModColorB) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModColorB);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:color_hist_b_features";
        sortArrayIndex++;
    }
    if(isnormModColorH) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModColorH);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:color_hist_h_features";
        sortArrayIndex++;
    }
    if(isnormModColorR) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModColorR);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:color_hist_r_features";
        sortArrayIndex++;
    }
    if(isnormModColorS) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModColorS);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:color_hist_s_features";
        sortArrayIndex++;
    }
    if(isnormModColorG) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModColorG);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:color_hist_g_features";
        sortArrayIndex++;
    }
    if(isnormModYoloVoc) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModYoloVoc);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:yolo_voc_concepts";
        sortArrayIndex++;
    }
    if(isnormModYoloCoco) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModYoloCoco);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:yolo_coco_concepts";
        sortArrayIndex++;
    }
    if(isnormModGoogleNetF) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModGoogleNetF);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:googlenet_features";
        sortArrayIndex++;
    }
    if(isnormModGoogleNetC) {

        weightsAndComponents[sortArrayIndex].weight = sqrt(normModGoogleNetC);
        weightsAndComponents[sortArrayIndex].componentName = "Mod:googlenet_concepts";
        sortArrayIndex++;
    }

    cout << "Number of elements in weightsAndComponents array = " << sortArrayIndex << endl;

    WeightAndComponent comparatorObject;
    cout << "Sorting to report weights learnt" << endl;
    std::vector<WeightAndComponent> vectorToSort(weightsAndComponents, weightsAndComponents + sortArrayIndex);
    std::sort(vectorToSort.begin(), vectorToSort.end(), comparatorObject);     //(12 26 32 33 45 53 71 80)
    cout << "Weights learnt in descending order of magnitude and writing thwm to a JSON file: " << endl;

    nlohmann::json componentWeights;
    componentWeights["category"] = train_category;
    componentWeights["budget"] = budget;
    componentWeights["num_samples"] = numSamples;
    componentWeights["epoch_num"] = epochNum;
    nlohmann::json temp;
    std::vector<nlohmann::json> learntComponents;
    bool maxFound = false;
    string maxFeature;
    for (std::vector<WeightAndComponent>::iterator it=vectorToSort.begin(); it!=vectorToSort.end(); ++it) {
        std::cout << "Name: " << (*it).componentName << endl;
        std::cout << "Weight: " << (*it).weight << endl;
        temp["name"] = (*it).componentName;
        temp["weight"] = (*it).weight;
        learntComponents.push_back(temp);
        if(!maxFound && strstr((*it).componentName.c_str(), "Mod")) {
          maxFound = true;
          maxFeature = (*it).componentName;
        }
    }
    componentWeights["descending_components"] = learntComponents;
    std::ofstream file4(name + "/" + train_category + "_" + budget + "_components.json");
    file4 << componentWeights;

TestLabel:

    cout << "\n******Running the model on test data******\n" << endl;
    testingBegin = clockInSec();
    vector<vector<datk::SetFunctions*>> mixtureForAllTestVideos;

    vector<std::string> testVideoNames;

    vector<int> testVideoGroundSetSizes;

    vector<datk::SetFunctions*> lossComponentsOfTestVideos;
    std::string test_category = trainingData["test_category"];
    std::string testFeaturesPath = featuresFolder + "/" + test_category;

    componentIndex = 0;
    cout << "Loading features and preparing all components for all test videos " << endl;
    for (auto it = trainingData["test_videos"].begin(); it != trainingData["test_videos"].end(); ++it) {//for each video in test data
        componentIndex = 0;
        int groundSetSize = 0;
        datk::Set groundSet;
        groundSet.clear();
        std::string video_name = *it;
        cout << "\nProcessing test video: " << video_name << " of category " << test_category << endl;
        cout << "Loading its features from " << testFeaturesPath << endl;
        featuresForThisVideoBegin = clockInSec();
        nlohmann::json masterTestFeaturesJson;
        //TODO:see which features are needed based on desired components and load only them in memory
        //For now, loading all features in memory - create a master json for all features of this video in memory
        DIR* dirFile = opendir(testFeaturesPath.c_str());
        if (dirFile) {
            //cout << "Opening " << featuresFolder << " looking for features" << endl;
            struct dirent* hFile;
            errno = 0;
            //bool featureFileFound = false;
            while (( hFile = readdir( dirFile )) != NULL )
            {
                //cout << "File name " << hFile ->d_name << endl;
                //if(featureFileFound) continue;
                if ( !strcmp( hFile->d_name, "."  )) continue;
                if ( !strcmp( hFile->d_name, ".." )) continue;
                // in linux hidden files all start with '.'
                if (   ( hFile->d_name[0] == '.' )) continue;
                if ( strstr( hFile->d_name, (video_name+".").c_str() ) && strstr( hFile->d_name, "json" ) ) {
                    //cout << "This features file belongs to this video, opening and parsing it" << endl;
                    cout << "Found " << hFile->d_name << endl;
                    nlohmann::json currJson;
                    std::ifstream currJsonFile;
                    currJsonFile.open(testFeaturesPath + "/" + hFile->d_name);
                    if(currJsonFile.fail()) { cout<<"Error opening file:"<<testFeaturesPath + "/" + hFile->d_name <<endl; exit(0);}
                    //featureFileFound = true;
                    currJsonFile >> currJson;
                    for (auto iter = currJson.begin(); iter != currJson.end(); ++iter) {
                        masterTestFeaturesJson[iter.key()] = iter.value();
                        cout << "Added " << iter.key() << " to master JSON of features for test videos" << endl;
                    }
                }
            }
            //if(!featureFileFound) {
            //    cout<<"Feature file not found for video: "<<video_name<<endl;
            //    exit(0);
            //}
            closedir( dirFile );
        }

        groundSetSize = masterTestFeaturesJson["num_features"];
        for(int i=0; i<groundSetSize; i++) {
            groundSet.insert(i);
        }

        double normalizationConstantMin = normData[video_name]["min_score"];
        double normalizationConstantMax = normData[video_name]["max_score"];
        double normalizationConstant = normalizationConstantMax - normalizationConstantMin;
        cout << "\nNormalization constant of this video = " << normalizationConstant << endl;
        featuresForThisVideoEnd = clockInSec();
        cout << "\nTime taken for loading features for this test video: " << (featuresForThisVideoEnd-featuresForThisVideoBegin) << endl;
        //prepare the mixture for this video
        vector<datk::SetFunctions*> fvec;
        mixtureForThisVideoBegin = clockInSec();
        int verifyComponentIndex = 0;

        for (auto compit = components.begin(); compit != components.end(); ++compit) {
            std::cout << "\nFound: " << compit.key() << " | " << compit.value() << "\n";
            if(compit.key()=="SeC") {
                nlohmann::json object = components[compit.key()];
                cout << "SeC is present in components" << endl;
                std::vector<std::string> all_concepts = object["Concepts"];
                for (auto concept: all_concepts) { //for each required concept
                    std::cout << "Concept: " << concept << endl;
                    //look for that concept in master features json of this video
                    std::vector<std::vector<double>> concepts = masterTestFeaturesJson[concept];
                    //create appropriate data
                    // groundSetSize = 0;
                    std::vector<datk::Set> conceptVectors;
                    // datk::Set groundSet;
                    for (auto conceptVector: concepts) {
                        datk::Set currset;
                        for (int index = 0; index < conceptVector.size(); index++) {
                            if(conceptVector[index] > 0) currset.insert(index);
                        }
                        conceptVectors.push_back(currset);
                        // groundSet.insert(groundSetSize);
                        // groundSetSize++;
                    }
                    cout << "Ground set size for SeC " << concept << " for test video " << video_name << " = " << groundSetSize << endl;
                    //create this component and add in the mixture for this video
                    datk::SetCover* setCover = new datk::SetCover(groundSetSize, conceptVectors);
                    double funVal = setCover->eval(groundSet);
                    lambda = abs(funVal) > 0 ? 1/funVal : 1;
                    cout << "F(V) for SeC for " << concept << " = " << 1/lambda << endl;
                    datk::ScaleSetFunctions* setCoverScaled = new datk::ScaleSetFunctions(setCover, lambda);
                    fvec.push_back(setCoverScaled);
                    cout << "\nAdded component SeC " << concept << " to mixture of test video " << video_name << endl;
                    if(componentLookup[verifyComponentIndex]!=("SeC:" + concept)) {
                        cout << "\n**BIG PROBLEM** Index mismatch for component between train and test." << endl;
                        exit(0);
                    }
                    verifyComponentIndex++;
                }
            } else if(compit.key()=="PSC") {
                nlohmann::json object = components[compit.key()];
                cout << "PSC is present in components" << endl;
                std::vector<std::string> all_concepts = object["Concepts"];
                for (auto concept: all_concepts) { //for each required concept
                    std::cout << "Concept: " << concept << endl;
                    //look for that concept in master features json of this video
                    std::vector<std::vector<double>> probVectors = masterTestFeaturesJson[concept];
                    //create appropriate data
                    // groundSetSize = probVectors.size();
                    int numConcepts = probVectors[0].size();
                    // datk::Set groundSet;
                    // for(int i=0; i<groundSetSize; i++) {
                    //     groundSet.insert(i);
                    // }
                    cout << "Ground set size for PSC " << concept << " for test video " << video_name << " = " << groundSetSize << endl;
                    //create this component and add in the mixture for this video
                    datk::ProbabilisticSetCover* probSC = new datk::ProbabilisticSetCover(groundSetSize, numConcepts, probVectors);
                    double funVal = probSC->eval(groundSet);
                    lambda = abs(funVal) > 0 ? 1/funVal : 1;
                    cout << "F(V) for PSC for " << concept << " = " << 1/lambda << endl;
                    datk::ScaleSetFunctions* probSCScaled = new datk::ScaleSetFunctions(probSC, lambda);
                    fvec.push_back(probSCScaled);
                    cout << "\nAdded component PSC " << concept << " to mixture of test video " << video_name << endl;
                    if(componentLookup[verifyComponentIndex]!=("PSC:" + concept)) {
                        cout << "\n**BIG PROBLEM** Index mismatch for components between train and test." << endl;
                        exit(0);
                    }
                    verifyComponentIndex++;
                }
            } else if(compit.key()=="FL" || compit.key()=="SC" || compit.key()=="GC" || compit.key()=="DM") {
                nlohmann::json object = components[compit.key()];
                cout << compit.key()<< " is present in components" << endl;
                std::vector<std::string> all_features = object["Features"];
                std::vector<std::string> similarities = object["Kernels"];
                for (auto feature: all_features) { //for each required concept
                    for (auto similarity: similarities ) { //for each kernel type
                        if(similarity=="dot") {
                            std::cout << "Feature: " << feature << endl;
                            std::cout << "Similarity kernel: dot" << endl;
                            //look for this feature in master features json of this video
                            std::vector<std::vector<double>> features = masterTestFeaturesJson[feature];
                            //create appropriate data
                            std::vector<std::vector<float>> kernel;
                            // groundSetSize = features.size();
                            // datk::Set groundSet;
                            // for(int i=0; i<groundSetSize; i++) {
                            //     groundSet.insert(i);
                            // }
                            cout << "Computing kernel" << endl;
                            double sim = 0;
                            double a_norm = 0, b_norm = 0;
                            double min = std::numeric_limits<double>::max();
                            double max = std::numeric_limits<double>::min();
                            for (int i = 0; i < features.size(); i++) {
                                std::vector<float> currvector;
                                a_norm = sqrt(datk::innerProduct(features.at(i), features.at(i)));
                                for (int j = 0; j < features.size(); j++) {
                                    sim = datk::innerProduct(features.at(i), features.at(j));
                                    b_norm = sqrt(datk::innerProduct(features.at(j), features.at(j)));
                                    sim = a_norm * b_norm > 0 ? sim / (a_norm * b_norm) : 0;
                                    sim = (1.0 + sim) / 2.0;
                                    if (sim < min) min = sim;
                                    if (sim > max) max = sim;
                                    currvector.push_back(sim);
                                }
                                kernel.push_back(currvector);
                            }
                            /*
                            //0-1 normalize using min and max - this becomes the kernel
                            double range = max - min;
                            for (int i = 0; i < features.size(); i++) {
                                for (int j = 0; j < features.size(); j++) {
                                    kernel[i][j] = (kernel[i][j] - min) / range;
                                }
                            }
                            */
                            cout << "Ground set size for FL/SC/GC/DM " << feature << " for test video " << video_name << " = " << groundSetSize << endl;
                            if(compit.key()=="FL") {
                                datk::FacilityLocation* facLoc = new datk::FacilityLocation(groundSetSize, kernel);
                                double funVal = facLoc->eval(groundSet);
                                lambda = abs(funVal) > 0 ? 1/funVal : 1;
                                cout << "F(V) for FL " << feature << " and type dot = " << 1/lambda << endl;
                                datk::ScaleSetFunctions* facLocScaled = new datk::ScaleSetFunctions(facLoc, lambda);
                                fvec.push_back(facLocScaled);
                                cout << "\nAdded component FL " << feature << ":dot: to mixture of test video " << video_name << endl;
                                if(componentLookup[verifyComponentIndex]!=("FL:" + feature)) {
                                    cout << "\n**BIG PROBLEM** Index mismatch for components between train and test." << endl;
                                    exit(0);
                                }
                                verifyComponentIndex++;
                            } else if (compit.key()=="SC") {
                                datk::SaturateCoverage* satCov = new datk::SaturateCoverage(groundSetSize, kernel, 0.1); //TODO: is 0.1 OK?
                                double funVal = satCov->eval(groundSet);
                                lambda = abs(funVal) > 0 ? 1/funVal : 1;
                                cout << "F(V) for SC " << feature << " and type dot = " << 1/lambda << endl;
                                datk::ScaleSetFunctions* satCovScaled = new  datk::ScaleSetFunctions(satCov, lambda);
                                fvec.push_back(satCovScaled);
                                cout << "\nAdded component SC " << feature << ":dot: to mixture of test video " << video_name << endl;
                                if(componentLookup[verifyComponentIndex]!=("SC:" + feature)) {
                                    cout << "\n**BIG PROBLEM** Index mismatch for components between train and test." << endl;
                                    exit(0);
                                }
                                verifyComponentIndex++;
                            } else if(compit.key()=="GC") {
                                datk::GraphCutFunctions* graphCut = new datk::GraphCutFunctions(groundSetSize, kernel, 0.5); //TODO: is 0.5 ok?
                                double funVal = graphCut->eval(groundSet);
                                lambda = abs(funVal) > 0 ? 1/funVal : 1;
                                cout << "F(V) for GC " << feature << " and type dot = " << 1/lambda << endl;
                                datk::ScaleSetFunctions* graphCutScaled = new datk::ScaleSetFunctions(graphCut, lambda);
                                fvec.push_back(graphCutScaled);
                                cout << "\nAdded component GC " << feature << ":dot: to mixture of test video " << video_name << endl;
                                if(componentLookup[verifyComponentIndex]!=("GC:" + feature)) {
                                    cout << "\n**BIG PROBLEM** Index mismatch for components between train and test." << endl;
                                    exit(0);
                                }
                                verifyComponentIndex++;
                            } else if(compit.key()=="DM") {
                                datk::DisparityMin* dispMin = new datk::DisparityMin(groundSetSize, kernel);
                                //lambda = 1/dispMin->eval(groundSet);
                                //cout << "F(V) for DM " << feature << " and type dot = " << 1/lambda << endl;
                                datk::ScaleSetFunctions* dispMinScaled = new datk::ScaleSetFunctions(dispMin, 1);
                                fvec.push_back(dispMinScaled);
                                cout << "\nAdded component DM " << feature << ":dot: to mixture of test video " << video_name << endl;
                                if(componentLookup[verifyComponentIndex]!=("DM:" + feature)) {
                                    cout << "\n**BIG PROBLEM** Index mismatch for components between train and test." << endl;
                                    exit(0);
                                }
                                verifyComponentIndex++;
                            } else {
                                cout << "This is impossible - something other than FL/GC/DM/SC within FL/GC/DM/SC!!!!" << endl;
                            }
                        } else{
                            cout << "Similarity kernel " << similarity << " is not yet implemented";
                            //TODO: numComponents--; but then how about creating new vs load?
                        }
                    }
                }
            } else if (compit.key()=="PS") {
                nlohmann::json object = components[compit.key()];
                cout << compit.key()<< " is present in components" << endl;
                //create appropriate data
                cout << "Browsing shots folder to get shots and snippets information" << endl;
                nlohmann::json shotsJson;
                DIR* dirFile = opendir(shotsFolder.c_str());
                if (dirFile) {
                    cout << "Opening " << shotsFolder << " looking for shots info of this video" << endl;
                    struct dirent* hFile;
                    errno = 0;
                    bool shotsFileFound = false;
                    while (( hFile = readdir( dirFile )) != NULL )
                    {
                        //cout << "File name " << hFile ->d_name << endl;
                        if(shotsFileFound) continue;
                        if ( !strcmp( hFile->d_name, "."  )) continue;
                        if ( !strcmp( hFile->d_name, ".." )) continue;
                        // in linux hidden files all start with '.'
                        if (   ( hFile->d_name[0] == '.' )) continue;
                        if ( strstr( hFile->d_name, (video_name+"_").c_str() ) && strstr( hFile->d_name, "shots" ) && strstr( hFile->d_name, "json" ) ) {
                            //cout << "This shots file belongs to this video, opening and parsing it" << endl;
                            cout << "Found " << hFile->d_name << endl;
                            std::ifstream shotsJsonFile;
                            shotsJsonFile.open(shotsFolder + hFile->d_name);
                            if(shotsJsonFile.fail()) { cout<<"Error opening file:"<<shotsFolder + hFile->d_name <<endl; exit(0);}
                            shotsFileFound = true;
                            shotsJsonFile >> shotsJson;
                        }
                    }
                    if(!shotsFileFound) {
                        cout<<"Shots file not found for video: "<<video_name<<endl;
                        exit(0);
                    }
                    closedir( dirFile );
                }
                // groundSetSize = shotsJson.size();
                std::vector<std::vector<float>> kernel;
                // datk::Set groundSet;
                // for(int i=0; i<groundSetSize; i++) {
                //     groundSet.insert(i);
                // }
                cout << "Number of snippets for test video " << video_name << " according to shots JSON = " << shotsJson.size() << endl;
                cout << "Computing kernel" << endl;
                double sim = 0;
                double min = std::numeric_limits<double>::max();
                double max = std::numeric_limits<double>::min();
                for (int i = 0; i < groundSetSize; i++) {
                    std::vector<float> currvector;
                    for (int j = 0; j < groundSetSize; j++) {
                        if(i==j) sim = 1;
                        else {
                            if(shotsJson[std::to_string(i)] == shotsJson[std::to_string(j)]) {
                                sim = 1.0 - (abs(j - i) - groundSetSize);
                            }
                            else sim = 0;
                        }
                        currvector.push_back(sim);
                    }
                    kernel.push_back(currvector);
                }
                //0-1 normalize using min and max - this becomes the kernel
                //max = 1;
                //min = 1/(groundSetSize - 1);
                //double range = max - min;
                //for (int i = 0; i < groundSetSize; i++) {
                //    for (int j = 0; j < groundSetSize; j++) {
                //        kernel[i][j] = (kernel[i][j] - min) / range;
                //    }
                //}
                cout << "Ground set size for PS for test video " << video_name << " = " << groundSetSize << endl;
                datk::PairwiseSum* pair = new datk::PairwiseSum(groundSetSize, kernel);
                double funVal = pair->eval(groundSet);
                lambda = abs(funVal) > 0 ? 1/funVal : 1;
                cout << "F(V) for PS = " << 1/lambda << endl;
                datk::ScaleSetFunctions* pairScaled = new datk::ScaleSetFunctions(pair, lambda);
                fvec.push_back(pairScaled);
                cout << "\nAdded component PS to mixture of test video " << video_name << endl;
                if(componentLookup[verifyComponentIndex]!="PS") {
                    cout << "\n**BIG PROBLEM** Index mismatch for components between train and test." << endl;
                    exit(0);
                }
                verifyComponentIndex++;
            } else if(compit.key()=="FB") {
                nlohmann::json object = components[compit.key()];
                cout << compit.key()<< " is present in components" << endl;
                std::vector<std::string> all_features = object["Features"];
                std::vector<int> types = object["Types"];
                for (auto feature: all_features) { //for each required feature
                    std::vector<std::vector<double>> features = masterTestFeaturesJson[feature];
                    // groundSetSize = features.size();
                    // datk::Set groundSet;
                    // for(int i=0; i<groundSetSize; i++) {
                    //     groundSet.insert(i);
                    // }
                    vector<datk::SparseFeature> features_input;
                    int numFeatures = features[0].size();
                    for (auto featureVector: features) {
                        datk::SparseFeature s;
                        for (int index = 0; index < featureVector.size(); index++) {
                            if(featureVector[index] != 0) {
                                s.featureIndex.push_back(index);
                                s.featureVec.push_back(featureVector[index]);
                            }
                        }
                        s.numFeatures = numFeatures;
                        s.numUniqueFeatures = s.featureIndex.size();
                        features_input.push_back(s);
                    }
                    vector<double> featureWeights(numFeatures, 1);
                    cout << "Ground set size for FB " << feature << " for test video " << video_name << " = " << groundSetSize << endl;
                    for (auto type: types ) { //for each type of concave function
                        //1:sqrt, 2:inverse, 3:log, 4:min
                        //create this component and add in the mixture for this video
                        datk::FeatureBasedFunctions* fbx = new datk::FeatureBasedFunctions(groundSetSize, type, features_input, featureWeights, 0.2);
                        double funVal = fbx->eval(groundSet);
                        lambda = abs(funVal) > 0 ? 1/funVal : 1;
                        cout << "F(V) for FB " << feature << " and type " << type << " = " << 1/lambda << endl;
                        datk::ScaleSetFunctions* fbxScaled = new datk::ScaleSetFunctions(fbx, lambda);
                        fvec.push_back(fbxScaled);
                        cout << "\nAdded component FB " << feature << " of type " << type << " to mixture of test video " << video_name << endl;
                        if(componentLookup[verifyComponentIndex]!=("FB:" + feature + ":" + to_string(type))) {
                            cout << "\n**BIG PROBLEM** Index mismatch for components between train and test." << endl;
                            exit(0);
                        }
                        verifyComponentIndex++;
                    }
                }
            } else if(compit.key()=="Mod") {
                nlohmann::json object = components[compit.key()];
                cout << compit.key()<< " is present in components" << endl;
                for (auto compsubit = object.begin(); compsubit != object.end(); ++compsubit) {
                    cout << compsubit.key()<< " is present in Mod" << endl;
                    std::vector<std::vector<double>> features = masterTestFeaturesJson[compsubit.key()];
                    // groundSetSize = features.size();
                    int numFeatures = compsubit.value();
                    // datk::Set groundSet;
                    // for(int i=0; i<groundSetSize; i++) {
                    //     groundSet.insert(i);
                    // }
                    for(int i=0; i<numFeatures; i++) {
                        vector<double> weight;
                        double fv = 0;
                        for(int j=0; j<groundSetSize; j++) {
                            weight.push_back(features[j][i]);
                            fv += abs(features[j][i]);
                        }
                        //cout << "Ground set size for Mod " << compsubit.key()<< " and index " << i << " for video " << video_name << " = " << groundSetSize << endl;
                        datk::ModularFunctions* mf = new datk::ModularFunctions(weight, 0);
                        //lambda = 1/mf->eval(groundSet);
                        //cout << "F(V) for Mod " << compsubit.key()<< " and index " << i << " = " << fv << endl;
                        lambda = ((fv == 0)?1:1/fv);
                        datk::ScaleSetFunctions* mfScaled = new datk::ScaleSetFunctions(mf, lambda);
                        fvec.push_back(mfScaled);
                        //cout << "\nAdded component Mod " << compsubit.key()<< " of index " << i << " to mixture of test video " << video_name << endl;
                        if(componentLookup[verifyComponentIndex]!=("Mod:" + compsubit.key()+ ":" + to_string(i))) {
                            cout << "\n**BIG PROBLEM** Index mismatch for components between train and test." << endl;
                            exit(0);
                        }
                        verifyComponentIndex++;
                    }
                }
            } else {
                cout << "Unrecognized key in components JSON file" << endl;
            }
        }
        assert(numComponents == verifyComponentIndex);
        //create loss function component, look up normalization constant and add in the mixture for this video
        nlohmann::json annotation;
        std::string annotationJsonFile = dataset["root"].get<std::string>() + test_category + "/" + dataset["categories"][test_category][video_name]["annotation"].get<std::string>();
        std::ifstream annotationJson;
        annotationJson.open(annotationJsonFile);
        if(annotationJson.fail()) { cout<<"Error opening file:"<<annotationJsonFile <<endl; exit(0);}
        annotationJson >> annotation;

        // groundSetSize = masterFeaturesJson["vgg_features"].size();
        std::vector<datk::Set> segments;
        std::vector<int> ratings;
        std::vector<bool> isRedundant;
        double alpha = modelParams["alpha"];//1;
        double beta = modelParams["beta"];//6;
        double penalty = modelParams["penalty"];//2;
        int segIndex = 0;
        int maxSnippetId = -1;
        for(auto seg : annotation) {
            datk::Set snippets;
            string line = seg["begin"];
            std::vector <string> tokens;
            stringstream check1(line);
            string intermediate;
            while(getline(check1, intermediate, ':')) {
                tokens.push_back(intermediate);
            }
            int hrs = stoi(tokens[0]);
            int mins = stoi(tokens[1]);
            int secs = stoi(tokens[2]);
            float beginSeconds=3600*hrs+60*mins+secs;
            line = seg["end"];
            stringstream check2(line);
            tokens.clear();
            while(getline(check2, intermediate, ':')) {
                tokens.push_back(intermediate);
            }
            hrs = stoi(tokens[0]);
            mins = stoi(tokens[1]);
            secs = stoi(tokens[2]);
            float endSeconds=3600*hrs+60*mins+secs;
            //add snippet ids to this set
            int beginSnippetId = ceil(beginSeconds/2.0);
            int endSnippetId =floor((endSeconds-1)/2.0);
            //for last end, if odd
            if (endSnippetId == groundSetSize) endSnippetId --;
            if (endSnippetId > maxSnippetId) maxSnippetId = endSnippetId;
            for(int id = beginSnippetId; id <= endSnippetId; id++) {
                snippets.insert(id);
            }
            segments.push_back(snippets);
            ratings.push_back(seg["rating"]);
            bool isRepetitive = false;
            if(seg.find("repetitive") == seg.end()) {
                isRepetitive = false;
            } else {
                if(seg["repetitive"] == true) isRepetitive = true;
                else isRepetitive = false;
            }
            isRedundant.push_back(isRepetitive);
        }
        cout << "\nMax snippet id = " << maxSnippetId << endl;
        cout << "Ground set size for VSL for test video " << video_name << " = " << groundSetSize << endl;
        datk::VideoSummLoss* vsl = new datk::VideoSummLoss(groundSetSize, segments, ratings, isRedundant, alpha, beta, penalty, normalizationConstantMax);
        lambda = 1/normalizationConstant;
        cout << "Greedy max for infinite budget of VSL for test video " << video_name << " = " << normalizationConstant << endl;
        datk::ScaleSetFunctions* vslScaled = new datk::ScaleSetFunctions(vsl, lambda);
        lossComponentsOfTestVideos.push_back(vslScaled);
        cout << "Stored VSL component for test video " << video_name << endl;
        cout << "\nAdding the complete mixture for this test video " << video_name << " to the vector of mixtures of all videos" << endl;
        mixtureForAllTestVideos.push_back(fvec);
        testVideoNames.push_back(video_name);
        testVideoGroundSetSizes.push_back(groundSetSize);
        mixtureForThisVideoEnd = clockInSec();
        cout << "Time taken for preparing mixture for this test video: " << (mixtureForThisVideoEnd-mixtureForThisVideoBegin)<< endl;
    }

    vector<string> modFeatures = {"vgg_features", "googlenet_features",
        "color_hist_r_features", "color_hist_g_features", "color_hist_b_features",
        "color_hist_h_features", "color_hist_s_features"};

    vector<string> modConcepts = {"vgg_concepts", "vgg_p_concepts", "googlenet_concepts",
        "googlenet_p_concepts", "yolo_voc_concepts", "yolo_voc_p_concepts",
        "yolo_coco_concepts", "yolo_coco_p_concepts"};

    map<string, map<string, double> > testSubmodScores;
    map<string, double> testMixtureCandidateScores;
    map<string, double> testMixtureGTScores;
    map<string, map<string, vector<double> > > testModValues;
    map<string, map<string, vector<double> > > testConceptValues;

    cout << "\n**Assertions**\n" << endl;
    cout << "Size of mixtureForAllTestVideos: " << mixtureForAllTestVideos.size() << endl;
    cout << "Size of testVideoNames: " << testVideoNames.size() << endl;
    cout << "Size of testVideoGroundSetSizes: " << testVideoGroundSetSizes.size() << endl;
    assert(mixtureForAllTestVideos.size() == testVideoNames.size());
    assert(mixtureForAllTestVideos.size() == testVideoGroundSetSizes.size());

    budgetNumSnippets = 0;
    testingBegin = clockInSec();
    // Running Inference on Test data
    for (int index = 0; index < testVideoNames.size(); index++) {//for each video in test data
        video_name = testVideoNames[index];
        exampleBegin = clockInSec();
        map<string, double> submodMap;
        map<string, vector<double> > modMap;
        map<string, vector<double> > conceptMap;
        cout << "\nBeginning to run inference on: " << video_name << endl;
        duration = dataset["categories"][test_category][video_name]["duration"];
        tmp = int(std::stof(budget)*duration);
        if(tmp%2 == 1) tmp = tmp - 1;
        budgetNumSnippets = tmp/2;
        cout << "Video duration = " << duration << " hence number of snippets expected = " << budgetNumSnippets << endl;
        //compute summary using naive greedy maximization of this mixture
        nlohmann::json gtJson;
        std::ifstream gtJsonFile;
        std::string gt_file_name = gtFolder + "/" + test_category + "/" + video_name + "/" + budget + "/" + test_category + "_" + video_name + "_" + "gt" + "_" + budget + "_0.json";
        cout << "Loading its ground truth summary: " << gt_file_name << endl;
        gtJsonFile.open(gt_file_name);
        if(gtJsonFile.fail()) { cout<<"Error opening file:"<<gt_file_name <<endl; exit(0);}
        gtJsonFile >> gtJson;
        nlohmann::json gtSnippets = gtJson["summary"];
        datk::Set gtIndices;
        for(auto snippet : gtSnippets) {
            gtIndices.insert((float)snippet["begin"]/2);
        }
        budgetNumSnippets = gtIndices.size();
        if(calculateSubModScores) {
          for(int c_i = 0; c_i < numComponents; c_i++) {
              if(strstr(componentLookup[c_i].c_str(), "Mod:")) continue;
              datk::Set singleCompIndices;
              datk::naiveGreedyMax(*(mixtureForAllTestVideos[index][c_i]), budgetNumSnippets, singleCompIndices, 0, false, true);
              double singleCompVSLScore = lossComponentsOfTestVideos[index]->eval(singleCompIndices);
              double singleCompScore = 1.0 - singleCompVSLScore;
              cout<< "Normalized score for "<<componentLookup[c_i]<<" = "<<singleCompScore<<endl;
              assert(singleCompIndices.size() == budgetNumSnippets);
              submodMap[componentLookup[c_i]] = singleCompScore;
          }
        }

        /*if(strstr(maxFeature.c_str(), "concepts")){

        } else{
          //for(auto feat: modFeatures) {
          string feat = maxFeature;
              vector<double> featWeights;
              vector<datk::SetFunctions*> featComps;
              //string featName = "Mod:" + feat;
              for(int c_i = 0; c_i < numComponents; c_i++) {
                  if(strstr(componentLookup[c_i].c_str(), feat.c_str())) {
                      featWeights.push_back(weightVecWithoutLoss[c_i]);
                      featComps.push_back(mixtureForAllTestVideos[index][c_i]);
                  }
              }

              if(featWeights.size() == 0) {
                  cout<<"WARNING!! Feature "<<feat<<" not found."<<endl;
                  continue;
              }

              datk::CombineSetFunctions modComb(testVideoGroundSetSizes[index], featComps, featWeights);
              vector<double> indiceScores;
              for(int g_i = 0; g_i < testVideoGroundSetSizes[index]; g_i++) {
                  datk::Set item;
                  item.insert(g_i);
                  double modValue = modComb.eval(item);
                  indiceScores.push_back(modValue);

              }
              modMap[feat] = indiceScores;
          //}

        }*/
        /*
        // for(auto concept: modConcepts) {
            auto concept = maxConcept;
            // string conceptName = "Mod:" + concept;
            string conceptName = concept;
            bool conceptFound = false;
            for(int c_i = 0; c_i < numComponents; c_i++) {
                if(strstr(componentLookup[c_i].c_str(), conceptName.c_str())) {
                    conceptFound = true;
                    vector<double> conceptScores;
                    for (int g_i = 0; g_i < testVideoGroundSetSizes[index]; g_i++) {
                        datk::Set item;
                        item.insert(g_i);
                        double conceptVal = mixtureForAllTestVideos[index][c_i]->eval(item);
                        conceptScores.push_back(conceptVal);
                    }
                    conceptMap[componentLookup[c_i]] = conceptScores;
                }
            }
            if(!conceptFound) {
                cout<<"WARNING!! Concept "<<conceptName<<" not found."<<endl;
            }*/
        // }


        /*

        vector<double> randWeights(numComponents, 0);
        for (int r_i = 0; r_i < numComponents; r_i++) {
            randWeights[r_i] = (double) rand() / ((double)RAND_MAX + 1);
        }

        datk::CombineSetFunctions fRandComb(testVideoGroundSetSizes[index], mixtureForAllTestVideos[index], randWeights);
        datk::Set randMaxIndices;
        datk::naiveGreedyMax(fRandComb, budgetNumSnippets, randMaxIndices, 0);
        double randMaxVSLScore = lossComponentsOfTestVideos[index]->eval(randMaxIndices);
        cout << "Normalized Random Mixture score = " << 1.0 - randMaxVSLScore <<endl;
        assert(randMaxIndices.size() == budgetNumSnippets);

        vector<double> unifWeights(numComponents, 1);
        datk::CombineSetFunctions fUnifComb(testVideoGroundSetSizes[index], mixtureForAllTestVideos[index], unifWeights);
        datk::Set unifMaxIndices;
        datk::naiveGreedyMax(fUnifComb, budgetNumSnippets, unifMaxIndices, 0);
        double unifMaxVSLScore = lossComponentsOfTestVideos[index]->eval(unifMaxIndices);
        cout << "Normalized Uniform Mixture score = " << 1.0 - unifMaxVSLScore <<endl;
        assert(unifMaxIndices.size() == budgetNumSnippets);

        */

        datk::CombineSetFunctions fComb(testVideoGroundSetSizes[index], mixtureForAllTestVideos[index], weightVecWithoutLoss);
        datk::Set candidateIndices;
        datk::naiveGreedyMax(fComb, budgetNumSnippets, candidateIndices, 0, false, true);
        //compute our score of y_cand and y_gt, store loss, update plot
        cout << "Size of optimum set = " << candidateIndices.size() << endl;
        if(budgetNumSnippets != candidateIndices.size()) {
            cout << "**** CAUTION - we expected a different size!" << endl;
            exit(0);
        }
        //cout << "Candidate indices: " << endl;
        //for(auto i: candidateIndices) {
        //    cout << i << " ";
        //}
        //cout << endl;
        double gtVSLScore = lossComponentsOfTestVideos[index]->eval(gtIndices);
        double normalizedGtScore = 1.0 - gtVSLScore;

        double candidateVSLScore = lossComponentsOfTestVideos[index]->eval(candidateIndices);
        double normalizedCandidateScore = 1.0 - candidateVSLScore;
        // double normalizedCandidateScore = (1/normalization[video_name])*candidateScore;
        cout << "Normalized candidate score = " << normalizedCandidateScore <<endl;
        cout << "Normalized gt score = " << normalizedGtScore <<endl;
        cout <<"\nWriting the output summary produced" << endl;

        nlohmann::json summary;
        summary["category"] = test_category;
        summary["budget"] = budget;
        summary["name"] = video_name;
        summary["algorithm"] = "model";
        summary["train_category"] = train_category_bak;
        std::vector<nlohmann::json> snippets;
        nlohmann::json object;
        for(auto i: candidateIndices) {
            object["begin"] = i*2;
            object["end"] = i*2 + 2;
            snippets.push_back(object);
        }
        summary["summary"] = snippets;
        summary["norm_score"] = normalizedCandidateScore;
        summary["gt_norm_score"] = normalizedGtScore;
        std::ofstream file5(name + "/" + test_category + "_" + video_name + "_" + budget +"_model_summary.json");
        file5 << summary;
	if(calculateSubModScores) {
        testSubmodScores[video_name] = submodMap;
	}
        testMixtureCandidateScores[video_name] = normalizedCandidateScore;
        testMixtureGTScores[video_name] = normalizedGtScore;
        testModValues[video_name] = modMap;
        testConceptValues[video_name] = conceptMap;

        // double normalizedGtScore = (1/normalization[video_name])*gtScore;
        cout << "\n***Inference loss for video " << video_name << " = " << normalizedGtScore - normalizedCandidateScore << endl;
        //testLosses.push_back(normalizedGtScore - normalizedCandidateScore);
        exampleEnd = clockInSec();
        cout << "Time taken for inference on this example: " << (exampleEnd-exampleBegin)<< endl;

    }
    testingEnd = clockInSec();
    cout << "Total time taken for inference on all test videos: " << (testingEnd-testingBegin) << endl;

    /*map<string, map<string, double> > trainSubmodScores;
    map<string, double> trainMixtureCandidateScores;
    map<string, double> trainMixtureGTScores;
    map<string, map<string, vector<double> > > trainModValues;
    map<string, map<string, vector<double> > > trainConceptValues;

    cout << "\n**Assertions**\n" << endl;
    cout << "Size of mixtureForAllTrainVideos: " << mixtureForAllVideos.size() << endl;
    cout << "Size of trainVideoNames: " << videoNames.size() << endl;
    cout << "Size of trainVideoGroundSetSizes: " << groundSetSizes.size() << endl;
    budgetNumSnippets = 0;
    testingBegin = clockInSec();
    // Running Inference for Training data
    for (int index = 0; index < videoNames.size(); index++) {//for each video in train data
        video_name = videoNames[index];
        exampleBegin = clockInSec();
        map<string, double> submodMap;
        map<string, vector<double> > modMap;
        map<string, vector<double> > conceptMap;
        cout << "\nBeginning to run inference on: " << video_name << endl;
        duration = dataset["categories"][train_category][video_name]["duration"];
        tmp = int(std::stof(budget)*duration);
        if(tmp%2 == 1) tmp = tmp - 1;
        budgetNumSnippets = tmp/2;
        cout << "Video duration = " << duration << " hence number of snippets expected = " << budgetNumSnippets << endl;
        //compute summary using naive greedy maximization of this mixture
        nlohmann::json gtJson;
        std::ifstream gtJsonFile;
        std::string gt_file_name = gtFolder + "/" + train_category + "/" + video_name + "/" + budget + "/" + train_category + "_" + video_name + "_" + "gt" + "_" + budget + "_0.json";
        cout << "Loading its ground truth summary: " << gt_file_name << endl;
        gtJsonFile.open(gt_file_name);
        if(gtJsonFile.fail()) { cout<<"Error opening file:"<<gt_file_name <<endl; exit(0);}
        gtJsonFile >> gtJson;
        nlohmann::json gtSnippets = gtJson["summary"];
        datk::Set gtIndices;
        for(auto snippet : gtSnippets) {
            gtIndices.insert((float)snippet["begin"]/2);
        }
        budgetNumSnippets = gtIndices.size();
        if(calculateSubModScores) {
        for(int c_i = 0; c_i < numComponents; c_i++) {
            if(strstr(componentLookup[c_i].c_str(), "Mod:")) continue;
            datk::Set singleCompIndices;
            datk::naiveGreedyMax(*(mixtureForAllVideos[index][c_i]), budgetNumSnippets, singleCompIndices, 0, false, true);
            double singleCompVSLScore = mixtureForAllVideos[index][indexOfLossFunction]->eval(singleCompIndices);
            double singleCompScore = 1.0 - singleCompVSLScore;
            cout<< "Normalized score for "<<componentLookup[c_i]<<" = "<<singleCompScore<<endl;
            assert(singleCompIndices.size() == budgetNumSnippets);
            submodMap[componentLookup[c_i]] = singleCompScore;
        }
      }
      if(strstr(maxFeature.c_str(), "concepts")){

      } else{
        // for(auto feat: modFeatures) {
            string feat = maxFeature;
            vector<double> featWeights;
            vector<datk::SetFunctions*> featComps;
            // string featName = "Mod:" + feat;
            string featName = feat;
            for(int c_i = 0; c_i < numComponents; c_i++) {
                if(strstr(componentLookup[c_i].c_str(), featName.c_str())) {
                    featWeights.push_back(weightVecWithoutLoss[c_i]);
                    featComps.push_back(mixtureForAllVideos[index][c_i]);
                }
            }

            if(featWeights.size() == 0) {
                cout<<"WARNING!! Feature "<<featName<<" not found."<<endl;
                continue;
            }

            datk::CombineSetFunctions modComb(groundSetSizes[index], featComps, featWeights);
            vector<double> indiceScores;
            for(int g_i = 0; g_i < groundSetSizes[index]; g_i++) {
                datk::Set item;
                item.insert(g_i);
                double modValue = modComb.eval(item);
                indiceScores.push_back(modValue);

            }
            modMap[featName] = indiceScores;
        // }
      }


        // for(auto concept: modConcepts) {
            auto concept = maxConcept;
            // string conceptName = "Mod:" + concept;
            string conceptName = concept;
            bool conceptFound = false;
            for(int c_i = 0; c_i < numComponents; c_i++) {
                if(strstr(componentLookup[c_i].c_str(), conceptName.c_str())) {
                    conceptFound = true;
                    vector<double> conceptScores;
                    for (int g_i = 0; g_i < groundSetSizes[index]; g_i++) {
                        datk::Set item;
                        item.insert(g_i);
                        double conceptVal = mixtureForAllVideos[index][c_i]->eval(item);
                        conceptScores.push_back(conceptVal);
                    }
                    conceptMap[componentLookup[c_i]] = conceptScores;
                }
            }
            if(!conceptFound) {
                cout<<"WARNING!! Concept "<<conceptName<<" not found."<<endl;
            }
        // }


        vector<datk::SetFunctions*> mixtureForTrainVideos(mixtureForAllVideos[index].begin(), mixtureForAllVideos[index].begin() + weightVecWithoutLoss.size());
        datk::CombineSetFunctions fComb(groundSetSizes[index], mixtureForTrainVideos, weightVecWithoutLoss);
        datk::Set candidateIndices;
        datk::naiveGreedyMax(fComb, budgetNumSnippets, candidateIndices, 0, false, true);
        //compute our score of y_cand and y_gt, store loss, update plot
        cout << "Size of optimum set = " << candidateIndices.size() << endl;
        if(budgetNumSnippets != candidateIndices.size()) {
            cout << "**** CAUTION - we expected a different size!" << endl;
            exit(0);
        }
        //cout << "Candidate indices: " << endl;
        //for(auto i: candidateIndices) {
        //    cout << i << " ";
        //}
        //cout << endl;
        double gtVSLScore = mixtureForAllVideos[index][indexOfLossFunction]->eval(gtIndices);
        double normalizedGtScore = 1.0 - gtVSLScore;

        double candidateVSLScore = mixtureForAllVideos[index][indexOfLossFunction]->eval(candidateIndices);
        double normalizedCandidateScore = 1.0 - candidateVSLScore;
        // double normalizedCandidateScore = (1/normalization[video_name])*candidateScore;
        cout << "Normalized candidate score = " << normalizedCandidateScore <<endl;
        cout << "Normalized gt score = " << normalizedGtScore <<endl;
        cout <<"\nWriting the output summary produced" << endl;

        nlohmann::json summary;
        summary["category"] = train_category;
        summary["budget"] = budget;
        summary["name"] = video_name;
        summary["algorithm"] = "model";
        summary["train_category"] = train_category_bak;
        std::vector<nlohmann::json> snippets;
        nlohmann::json object;
        for(auto i: candidateIndices) {
            object["begin"] = i*2;
            object["end"] = i*2 + 2;
            snippets.push_back(object);
        }
        summary["summary"] = snippets;
        summary["norm_score"] = normalizedCandidateScore;
        summary["gt_norm_score"] = normalizedGtScore;
        std::ofstream file5(name + "/" + train_category + "_" + video_name + "_" + budget +"_model_summary.json");
        file5 << summary;

        trainSubmodScores[video_name] = submodMap;
        trainMixtureCandidateScores[video_name] = normalizedCandidateScore;
        trainMixtureGTScores[video_name] = normalizedGtScore;
        trainModValues[video_name] = modMap;
        trainConceptValues[video_name] = conceptMap;

        // double normalizedGtScore = (1/normalization[video_name])*gtScore;
        cout << "\n***Inference loss for video " << video_name << " = " << normalizedGtScore - normalizedCandidateScore << endl;
        //testLosses.push_back(normalizedGtScore - normalizedCandidateScore);
        exampleEnd = clockInSec();
        cout << "Time taken for inference on this example: " << (exampleEnd-exampleBegin)<< endl;

    }*/
    //testingEnd = clockInSec();
    //cout << "Total time taken for inference on all train videos: " << (testingEnd-testingBegin) << endl;
    cout << "Saving test losses" << endl;
    nlohmann::json tLosses;
    tLosses["test_submod_scores"] = testSubmodScores;
    //tLosses["train_submod_scores"] = trainSubmodScores;
    tLosses["test_candidate_scores"] = testMixtureCandidateScores;
   //tLosses["train_candidate_scores"] = trainMixtureCandidateScores;
    tLosses["test_gt_scores"] = testMixtureGTScores;
    //tLosses["train_gt_scores"] = trainMixtureGTScores;
    tLosses["test_mod_values"] = testModValues;
    //tLosses["train_mod_values"] = trainModValues;
    tLosses["test_concept_values"] = testConceptValues;
    //tLosses["train_concept_values"] = trainConceptValues;
    tLosses["category"] = test_category;
    tLosses["train_category"] = train_category_bak;
    tLosses["budget"] = budget;
    std::ofstream file7(name + "/" + test_category + "_" + budget + "_inference_results.json");
    file7 << tLosses;
}
