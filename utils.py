import json
import os

def timestamps_to_seconds(timestamps):
    """
    Timstamps of the form hh:mm:ss to seconds
    """
    timestamps = timestamps.strip().split(':')
    assert len(timestamps) == 3
    assert [len(x) for x in timestamps[1:]] == [2, 2]
    timestamps = [int(x) for x in timestamps]
    assert max(timestamps[1:]) < 60 and min(timestamps[1:]) >= 0

    return timestamps[0] * 3600 + timestamps[1] * 60 + timestamps[2]

def annotation_json_file_to_list(annotation_json_file):
    """
    Annotation json file specified by path to a list.
    List schema for list (start_time, end_time, rating, redundant)
    """
    annotation_json = json.load(open(annotation_json_file, 'r'))
    return annotation_json_to_list(annotation_json)

def annotation_json_to_list(annotation_json):
    """
    Annotation json to a list sorted by begin time.
    List schema for list (start_time, end_time, rating, redundant)
    """
    result = sorted([
        (
            timestamps_to_seconds(x['begin']),
            timestamps_to_seconds(x['end']),
            int(x['rating']),
            False if not 'repetitive' in x else x['repetitive']
        ) for x in annotation_json])
    assert is_valid_annotation(result)
    return result

def is_valid_annotation(annotation):
    annotation = sorted(annotation)
    prev_end = 0
    for x in annotation:
        begin, end, _, _ = x
        if end <= begin:
            return (
                False,
                (
                    'Error at segement %s.'
                    + 'segement end <= begin'
                ) % str(x)
            )
        if begin != prev_end:
            return (
                False,
                (
                    'Error at segment %s.'
                    + 'Continuity error with previous segment.'
                ) % str(x)
            )
        prev_end = end

    return True


def is_json_file(file_path):
    return os.path.splitext(file_path)[1] in {'.json', '.JSON'}

def normalize_annotation(annotation, beta):
    """
    Convert [(start_time, end_time, rating, redundant),...] list to
    [(start_time_mod, end_time_mod, rating, duration)...].

    start_time_mod is start_time adjusted to even time boundaries
    by incrementing the original start time

    end_time_mod is end_time adjusted to even time boundaries
    by incrementing except when end_time is video_duration.
    end_time is also trimmed if duration > than beta

    duration is end_time_mod - start_time_mod

    video_duration is assumed to be max(end_time) in the input list
    """
    assert beta % 2 == 0
    vid_dur = max([x[1] for x in annotation])
    vid_dur = vid_dur if vid_dur % 2 == 0 else vid_dur - 1

    result = []
    for tup in annotation:
        start = tup[0] if tup[0] % 2 == 0 else tup[0] + 1
        end = tup[1] if tup[1] % 2  == 0 else \
                tup[1] - 1 if tup[1] > vid_dur else tup[1] + 1
        end = min(end, start + beta) if tup[3] and tup[2] >= 0 else end
        if end > start:
            result.append((start, end, tup[2], end - start))

    return result

def segments_to_two_sec_snippets(segments):
    result = []
    segments = sorted(segments, key=lambda x: x[0])
    for seg in segments:
        snips = [{'begin': x, 'end': x+2} for x in range(seg[0], seg[1], 2)]
        result.extend(snips)

    return result

