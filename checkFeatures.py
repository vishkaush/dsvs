import json
import glob
import argparse
import sys
import os, os.path

def parse(filename):
    try:
        with open(filename) as f:
            return json.load(f)

    except ValueError as e:
        print('invalid json: %s' % e)
        return None # or: raise

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to do a sanity check on all feature JSONs")
    parser.add_argument('--root', required=True, help='''Full path to root folder inside which all features need to be checked''')
    parser.add_argument('--category', required=True, help='''Category for which features need to be checked''')
    parser.add_argument('--dataset', required=True, help='''Path to dataset JSON''')

    args = parser.parse_args()

    dataset_json=json.load(open(args.dataset))

    print("Checking number of feature files for category " + args.category)
    numFeatureFiles = len([name for name in os.listdir(args.root) if os.path.isfile(os.path.join(args.root, name))])
    numVideos = dataset_json["categories"][args.category]["num_videos"]
    expectedFeatureFiles = numVideos * 10
    # print("numFeatureFiles = " + str(numFeatureFiles))
    # print("expectedFeatureFiles = " + str(expectedFeatureFiles))
    if numFeatureFiles != expectedFeatureFiles:
        print("**********Number of features mismatch*************")

    print("Checking vgg features")
    for filename in glob.glob(args.root + '**/*vgg*.json', recursive=True):
        print("Checking " + filename)
        test = parse(filename)
        if "num_features" not in test or "vgg_concepts" not in test or "vgg_p_concepts" not in test or "vgg_features" not in test:
            print("************At least one element is missing**************")
        if test["num_features"] != len(test["vgg_concepts"]) or len(test["vgg_concepts"]) != len(test["vgg_p_concepts"]):
            print("************Feature dimension mismatch**************")
        baseFileName = os.path.basename(filename)
        featureFileName = os.path.splitext(baseFileName)[0]
        components = featureFileName.split(".")
        videoName = components[0]
        duration = dataset_json["categories"][args.category][videoName]["duration"]
        calculatedDuration = test["num_features"] * 2
        print("Actual duration = " + str(duration))
        print("Duration according to feature snippets = " + str(calculatedDuration))
        print("Diff in duration = " + str(duration - calculatedDuration))
        if duration - calculatedDuration > 1:
            print("********Duration mismatch*********")


    print("Chceking yolo voc features")
    for filename in glob.glob(args.root + '**/*voc*.json', recursive=True):
        print("Checking " + filename)
        test = parse(filename)
        if "num_features" not in test or "yolo_voc_concepts" not in test or "yolo_voc_p_concepts" not in test:
            print("************At least one element is missing**************")
        if test["num_features"] != len(test["yolo_voc_concepts"]) or len(test["yolo_voc_concepts"]) != len(test["yolo_voc_p_concepts"]):
            print("************Feature dimension mismatch**************")
        baseFileName = os.path.basename(filename)
        featureFileName = os.path.splitext(baseFileName)[0]
        components = featureFileName.split(".")
        videoName = components[0]
        duration = dataset_json["categories"][args.category][videoName]["duration"]
        calculatedDuration = test["num_features"] * 2
        print("Actual duration = " + str(duration))
        print("Duration according to feature snippets = " + str(calculatedDuration))
        print("Diff in duration = " + str(duration - calculatedDuration))
        if duration - calculatedDuration > 1:
            print("********Duration mismatch*********")


    print("Chceking yolo coco features")
    for filename in glob.glob(args.root + '**/*coco*.json', recursive=True):
        print("Checking " + filename)
        test = parse(filename)
        if "num_features" not in test or "yolo_coco_concepts" not in test or "yolo_coco_p_concepts" not in test:
            print("************At least one element is missing**************")
        if test["num_features"] != len(test["yolo_coco_concepts"]) or len(test["yolo_coco_concepts"]) != len(test["yolo_coco_p_concepts"]):
            print("************Feature dimension mismatch**************")
        baseFileName = os.path.basename(filename)
        featureFileName = os.path.splitext(baseFileName)[0]
        components = featureFileName.split(".")
        videoName = components[0]
        duration = dataset_json["categories"][args.category][videoName]["duration"]
        calculatedDuration = test["num_features"] * 2
        print("Actual duration = " + str(duration))
        print("Duration according to feature snippets = " + str(calculatedDuration))
        print("Diff in duration = " + str(duration - calculatedDuration))
        if duration - calculatedDuration > 1:
            print("********Duration mismatch*********")


    print("Chceking googlenet features")
    for filename in glob.glob(args.root + '**/*googlenet*.json', recursive=True):
        print("Checking " + filename)
        test = parse(filename)
        if "num_features" not in test or "googlenet_concepts" not in test or "googlenet_p_concepts" not in test or "googlenet_features" not in test:
            print("************At least one element is missing**************")
        if test["num_features"] != len(test["googlenet_concepts"]) or len(test["googlenet_concepts"]) != len(test["googlenet_p_concepts"]):
            print("************Feature dimension mismatch**************")
        baseFileName = os.path.basename(filename)
        featureFileName = os.path.splitext(baseFileName)[0]
        components = featureFileName.split(".")
        videoName = components[0]
        duration = dataset_json["categories"][args.category][videoName]["duration"]
        calculatedDuration = test["num_features"] * 2
        print("Actual duration = " + str(duration))
        print("Duration according to feature snippets = " + str(calculatedDuration))
        print("Diff in duration = " + str(duration - calculatedDuration))
        if duration - calculatedDuration > 1:
            print("********Duration mismatch*********")


    print("Chceking color features")
    for filename in glob.glob(args.root + '**/*color*.json', recursive=True):
        print("Checking " + filename)
        test = parse(filename)
        if "num_features" not in test or "color_hist_r_features" not in test or "color_hist_g_features" not in test or "color_hist_b_features" not in test or "color_hist_s_features" not in test or "color_hist_h_features" not in test:
            print("************At least one element is missing**************")
        if test["num_features"] != len(test["color_hist_r_features"]) or len(test["color_hist_r_features"]) != len(test["color_hist_g_features"]) or len(test["color_hist_g_features"]) != len(test["color_hist_h_features"]) or len(test["color_hist_h_features"]) != len(test["color_hist_b_features"]) or len(test["color_hist_b_features"]) != len(test["color_hist_s_features"]):
            print("************Feature dimension mismatch**************")
        baseFileName = os.path.basename(filename)
        featureFileName = os.path.splitext(baseFileName)[0]
        components = featureFileName.split(".")
        videoName = components[0]
        duration = dataset_json["categories"][args.category][videoName]["duration"]
        calculatedDuration = test["num_features"] * 2
        print("Actual duration = " + str(duration))
        print("Duration according to feature snippets = " + str(calculatedDuration))
        print("Diff in duration = " + str(duration - calculatedDuration))
        if duration - calculatedDuration > 1:
            print("********Duration mismatch*********")
