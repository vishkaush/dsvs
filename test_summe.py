#!/usr/bin/python2

import sys
import os
import logging
import numpy as np
import sklearn.metrics as metrics
import cPickle as pickle

import learning_framework

logging.basicConfig(level=logging.DEBUG)


def rating_to_annotation_list(ratings):
    annotation_list = []
    for i, r in enumerate(ratings):
        annotation_list.append(((i * 2, (i+1)*2), r, False))
    return annotation_list

def get_test_data(test_file, ratings_path, feature_path):
    testing_examples = []
    example_gt = []
    with open(test_file, 'r') as f:
        vid_files = [line.strip() for line in f]

    for vid in vid_files:
        logging.info("Processing test_file %s" % vid)
        vid = os.path.splitext(os.path.basename(vid))[0]
        ratings_pkl_path = ratings_path + '/' + vid + '.ratings.pkl'
        feature_base_path =  feature_path + '/' + vid

        vgg_feature = pickle.load(open(feature_base_path + '.vgg_features.pkl', 'rb'))
        googlenet_feature = pickle.load(open(feature_base_path + '.googlenet_features.pkl', 'rb'))
        yolo_voc_feature = pickle.load(open(feature_base_path + '.yolo_voc_features.pkl', 'rb'))
        yolo_coco_feature = pickle.load(open(feature_base_path + '.yolo_coco_features.pkl', 'rb'))
        color_hist_feature = pickle.load(open(feature_base_path + '.color_hist_features.pkl', 'rb'))

        feat_len_dict = dict()

        feature_list = [vgg_feature, googlenet_feature, yolo_voc_feature,
                        yolo_coco_feature, color_hist_feature]

        for feat in feature_list:
            for item in feat:
                if not isinstance(feat[item], int):
                    feat_len_dict[item] = len(feat[item][0])

        # Fix later for shot boundary
        vid_len = 2 * vgg_feature['num_features']
        Y = vgg_feature['num_features']
        snippet_timestamp = [(x * 2, x * 2 + 2) for x in range(Y)]
        gaussian_sigma = 10
        alpha = 1
        beta = 6
        penalty = -2

        ratings = pickle.load(open(ratings_pkl_path, 'rb'))

        print("Vid %s, Vid len %d, rating len %d" % (vid, vid_len,
            len(ratings["ratings"][0]) * 2))

        assert vid_len == len(ratings["ratings"][0]) * 2
        budget = 0
        annotation_list = []
        min_score = 0
        max_score = 0
        y_gt = [0]
        vid_gt = []
        for rater in range(len(ratings["budget"])):
            #for sum_size in {0.1, 0.2, 0.3}:
                #annotation_list = rating_to_annotation_list(
                #        ratings["ratings"][rater])
                #budget = ratings["budget"][rater] * 2
            budget = max(budget, ratings["budget"][rater] * 2)
            gt = list(ratings["ratings"][rater].argsort()[::-1][:ratings["budget"][rater]])
            vid_gt.append(gt)
                #max_score = budget * np.exp(3 * alpha)
                #min_score = budget * penalty
        S = learning_framework.VideoElem(budget=budget, Y=Y, y_gt = y_gt, snippet_timestamp=snippet_timestamp,
                annotation=annotation_list, max_score=max_score, min_score=min_score,
                gaussian_sim_sigma=gaussian_sigma, alpha=alpha, beta=beta, penalty=penalty,
                vgg_concepts = vgg_feature['vgg_concepts'],
                googlenet_concepts = googlenet_feature['googlenet_concepts'],
                yolo_voc_concepts = yolo_voc_feature['yolo_voc_concepts'],
                yolo_coco_concepts = yolo_coco_feature['yolo_coco_concepts'],
                vgg_p_concepts = vgg_feature['vgg_p_concepts'],
                googlenet_p_concepts = googlenet_feature['googlenet_p_concepts'],
                vgg_features = vgg_feature['vgg_features'],
                googlenet_features = googlenet_feature['googlenet_features'],
                color_hist_r_features = color_hist_feature['color_hist_r_features'],
                color_hist_g_features = color_hist_feature['color_hist_g_features'],
                color_hist_b_features = color_hist_feature['color_hist_b_features'],
                color_hist_h_features = color_hist_feature['color_hist_h_features'],
                color_hist_s_features = color_hist_feature['color_hist_s_features']
            )


        testing_examples.append(S)
        example_gt.append(vid_gt)

    return testing_examples, feat_len_dict, example_gt

'''
def get_test_data(test_file):
    testing_examples = []
    with open(test_file, 'r') as f:
        vid_files = [line.strip() for line in f]

    for vid in vid_files:
        logging.info("Processing test_file %s" % vid)
        feature_file_path =  FEATURE_PATH + vid + '.features.pkl'
        ratings_file_path = RATING_PATH + vid + '.ratings.pkl'
        feature = pickle.load(open(feature_file_path, 'rb'))
        ratings = pickle.load(open(ratings_file_path, 'rb'))

        if len(feature["feature_vector"]) != len(ratings["ratings"][0]):
            logging.warn("Feature length mismatch. Skipping %s" % vid)
            continue

        vid_examples = []

        for rater in range(len(ratings["budget"])):
            S = VideoElem()
            S.Y = list(range(len(ratings["ratings"][rater])))
            S.feature = feature["feature_vector"]
            S.budget = ratings["budget"][rater]
            S.y_gt = list(ratings["gt"][rater])

            vid_examples.append(S)

        testing_examples.append(vid_examples)

    return testing_examples
'''

if __name__ == "__main__":
    test_file = sys.argv[1]
    annotation_path = sys.argv[2]
    feature_path = sys.argv[3]
    weight_file = sys.argv[4]

    weights = pickle.load(open(weight_file, "rb"))
    testing_examples, feat_len_dict, example_gt = get_test_data(test_file, annotation_path, feature_path)
    sat_cov_c = 1

    f_scores_avg = []
    f_scores_max = []
    for i in range(len(testing_examples)):
        summary_ind = learning_framework.get_summary(testing_examples[i], weights, feat_len_dict, sat_cov_c)
        f_scores = []
        for gt in example_gt[i]:
            summary = np.zeros(len(testing_examples[i].Y))
            summary[summary_ind[:len(gt)]] = 1
            y_gt = np.zeros(len(testing_examples[i].Y))
            y_gt[gt] = 1
            f_scores.append(metrics.f1_score(y_gt, summary))
        f_scores_avg.append(np.average(f_scores))
        f_scores_max.append(np.max(f_scores))
        print("F-score-avg: vid-id: %d, score: %f" % (
            i, f_scores_avg[i]))
        print("F-score-max: vid-id: %d, score: %f" % (
            i, f_scores_max[i]))
    print("Avg avg. F-score: %f" % np.average(f_scores_avg))
    print("Avg max. F-score: %f" % np.average(f_scores_max))

    '''
    for r in range(max_num_raters):
        for i in range(len(vid)):
            summary_ind = learning_framework.get_summary(instance, weights, feat_len_dict, sat_cov_c)
            summary = np.zeros(len(instance.Y))
            summary[summary_ind] = 1
            gt = np.zeros(len(instance.Y))
            gt[instance.y_gt] = 1
            f_score = metrics.f1_score(gt, summary)
            print("F-Score: vid-id: %d, rater-id: %d, score: %f" % (
                i, r, f_scores))
            f_scores[i][r] = f_score
            f_scores_avg[i] = np.average(f_scores[i][:r+1])
            f_scores_max[i] = np.max(f_scores[i][:r+1])
            print("F-score-avg: vid-id: %d, score: %f" % (
                i, f_scores_avg[i]))
        f_scores_avg.append(np.average(vid_f_scores))
        f_scores_max.append(np.max(vid_f_scores))

    print("Avg avg. F-score: %f" % np.average(f_scores_avg))
    print("Avg max. F-score: %f" % np.average(f_scores_max))
    '''
