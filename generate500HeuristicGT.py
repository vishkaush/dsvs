#! /usr/bin/python3

import json
import argparse
import os
import itertools
import numpy as np
import random
from utils import *

ALPHA = 1
BETA = 6
PENALTY = -2

def all_seg_picks_generator(segs, budget):
    assert segs[0][3] >= budget
    segs = [x for x in segs if x[3] >= budget]
    for seg in segs:
        yield [(seg[0], seg[0] + budget, seg[2], budget)]

def select_segments_randomly(same_len_segs, other_segs, budget):
    same_len = same_len_segs[0][3]
    num_same_len_pick = int(budget/same_len)
    rem_budget = budget - num_same_len_pick * same_len
    assert num_same_len_pick < len(same_len_segs)
    assert rem_budget >= 0
    random.shuffle(same_len_segs)
    same_len_segs_set = set(same_len_segs)
    result = []
    for comb in itertools.combinations(same_len_segs, num_same_len_pick):
        comb = list(comb)
        if rem_budget > 0:
            non_selected = same_len_segs_set - set(comb)
            for rest in all_seg_picks_generator(
                    list(non_selected) + other_segs, rem_budget):
                result.append(comb + rest)
                if len(result) >= 500:
                    break
        else:
            result.append(comb)
        if len(result) >= 500:
            break

    return result

def _yield_rand_segs(dp_arr, segs, budget, index):
    assert dp_arr[index][budget]
    if budget == 0:
        yield []
        return
    if index == 0:
        yield [segs[0]] if dp_arr[index][budget] else []
        return
    if dp_arr[index-1][budget]:
        for rand_segs in _yield_rand_segs(dp_arr, segs, budget, index-1):
            yield rand_segs
    if dp_arr[index-1][budget - segs[index][3]]:
        for rand_segs in _yield_rand_segs(
                dp_arr, segs, budget - segs[index][3], index-1):
            rand_segs.append(segs[index])
            yield rand_segs

def get_rand_segs(dp_arr, segs, budget, residue_seg=None):
    result = []
    for rand_segs in _yield_rand_segs(dp_arr, segs, budget, len(segs) - 1):
        if residue_seg is not None:
            rand_segs.append(residue_seg)
        result.append(rand_segs)
        if len(result) >= 500:
            break

    return result


def can_fit_budget(dp_dict, segs, budget, index):
    if budget == 0:
        return True
    elif index < 0 or budget < 0:
        return False
    elif (index, budget) in dp_dict:
        return dp_dict[(index, budget)]
    else:
        ans1 = can_fit_budget(dp_dict, segs, budget, index - 1)
        ans2 = can_fit_budget(dp_dict, segs, budget - segs[index][3], index - 1)

        dp_dict[(index, budget)] = ans1 or ans2
        return dp_dict[(index, budget)]

def get_dp_arr(seg_len, budget):
    dp_arr = []
    for i in range(seg_len):
        dp_arr.append([False] * (budget + 1))
    return dp_arr

def fill_dp_arr(dp_arr, segs, budget):
#    can_fit_budget(dp_dict, segs, budget, len(segs) - 1)
    assert len(dp_arr) >= len(segs)
    seg_slice_len = len(dp_arr[0])
    assert seg_slice_len >= budget
    for seg_slice in dp_arr:
        assert len(seg_slice) == seg_slice_len
    for i in range(len(segs)):
        for j in range(budget + 1):
            if j == 0:
                dp_arr[i][j] = True
            elif i == 0:
                if j == segs[0][3]:
                    dp_arr[i][j] = True
            else:
                ans1 = dp_arr[i-1][j]
                ans2 = dp_arr[i-1][j - segs[i][3]] if j >= segs[i][3] else False
                dp_arr[i][j] = ans1 or ans2



def get_500_heuristic_gt_summaries(
        annotation, budget_list):
    """
    Ensure only normalized annotations are passed (see util.py)
    and budget_list is sorted and is of even integers.
    """
    assert budget_list == sorted(budget_list)
    assert [x % 2 for x in budget_list] == [0] * len(budget_list)

    three_rated_segs = [x for x in annotation if x[2] == 3]
    two_rated_segs = [x for x in annotation if x[2] == 2]
    one_rated_segs = [x for x in annotation if x[2] == 1]
    zero_rated_segs = [x for x in annotation if x[2] == 0]

    budget_ind = 0
    curr_len = 0
    selected = []
    result =  dict()
    for segs in [three_rated_segs, two_rated_segs, one_rated_segs, zero_rated_segs]:
        segs = sorted(segs, key = lambda x: x[3], reverse=True)
        if len(segs) == 0:
            continue

        tot_seg_duration = sum([x[3] for x in segs])

        while True:

            if budget_ind >= len(budget_list):
                break

            rem_budget = budget_list[budget_ind] - curr_len
            if tot_seg_duration <= rem_budget:
                selected.extend(segs)
                curr_len += tot_seg_duration
                rem_budget = budget_list[budget_ind] - curr_len
                if rem_budget == 0:
                    result[budget_ind] = [tuple(selected)]
                    budget_ind += 1
                break
            else:
                if len(segs) == 1:
                    assert segs[0][3] >= rem_budget
                    rand_segs = [
                        [(segs[0][0], segs[0][0] + rem_budget, segs[0][2], rem_budget)]
                    ]
                else:
                    dp_arr = get_dp_arr(len(segs), rem_budget)
                    fill_dp_arr(dp_arr, segs, rem_budget)
                    if dp_arr[len(segs) - 1][rem_budget]:
                        rand_segs = get_rand_segs(dp_arr, segs, rem_budget)
                    else:
                        best_i = []
                        best_score = -1 * float('inf')
                        to_take = []
                        temp_segs_len = len(segs) - 1

                        for i, seg in enumerate(segs):
                            temp_segs = segs[:i] + segs[i + 1:]
                            dp_arr = get_dp_arr(temp_segs_len, rem_budget)
                            fill_dp_arr(dp_arr, temp_segs, rem_budget)
                            for j in range(1, seg[3]):
                                if rem_budget - j >= 0 and \
                                        dp_arr[temp_segs_len-1][rem_budget-j]:
                                    score = (
                                        2 * (rem_budget - j)
                                        + j * (1 + j / seg[3])
                                    )
                                    if score > best_score:
                                        best_i = [i]
                                        to_take = [j]
                                        best_score = score
                                    elif score == best_score:
                                        best_i.append(i)
                                        to_take.append(j)

                        assert len(best_i) > 0
                        assert len(best_i) == len(to_take)
                        rand_segs = []
                        for i, j in zip(best_i, to_take):
                            temp_segs = segs[:i] + segs[i + 1:]
                            dp_arr = get_dp_arr(temp_segs_len, rem_budget)
                            fill_dp_arr(dp_arr, temp_segs, rem_budget)
                            residue_seg = (
                                segs[i][0], segs[i][0] + j, segs[i][2], j
                            )
                            rand_segs += get_rand_segs(
                                dp_arr, temp_segs, rem_budget - j, residue_seg
                            )
                            if len(rand_segs) >= 500:
                                rand_segs = rand_segs[:500]
                                break


                result[budget_ind] = []
                for rand_seg in rand_segs:
                    assert sum([x[3] for x in rand_seg]) == rem_budget
                    result[budget_ind].append(tuple(selected + rand_seg))
                budget_ind += 1


        # End of while
        if budget_ind >= len(budget_list):
            break

    for i in range(len(budget_list)):
        if i not in result:
            result[i] = [tuple(selected)]

    return result


def process_dataset(
        dataset_json_file, category_arg, video_arg, loss_params, out_dir):
    dataset_json = json.load(open(dataset_json_file, 'r'))
    root_path = dataset_json['root']
    budget_fracs = [0.05, 0.15, 0.3]
    if category_arg is not None and \
            category_arg not in dataset_json['categories']:
        print(category_arg, 'not found')
        return
    for category in dataset_json['categories']:
        if category_arg is not None and category_arg != category:
            continue
        if video_arg is not None and \
                video_arg not in dataset_json['categories'][category]:
            print(video_arg, 'not found')
            return
        for video in dataset_json['categories'][category]:
            if video_arg is not None and video_arg != video:
                continue
            if video not in {'total_duration', 'num_videos'}:
                metadata = dataset_json['categories'][category][video]
                json_file = metadata['annotation']
                duration = metadata['duration']
                dir_prefix = video.split('_')[0]
                json_file_path =  os.path.join(root_path, dir_prefix, json_file)
                annotation = annotation_json_file_to_list(json_file_path)
                annotation = normalize_annotation(annotation, loss_params['beta'])
                budget = [int(duration * x) for x in budget_fracs]
                budget = [x if x % 2 == 0 else x - 1 for x in budget]
                print('Processing video', video)
                gt_summaries = get_500_heuristic_gt_summaries(
                    annotation, budget)
                for budget_ind in gt_summaries:
                    budget_frac = budget_fracs[budget_ind]
                    print(
                        'Num. of', budget_frac, 'summaries:',
                        len(gt_summaries[budget_ind])
                    )
                    for i, summary in enumerate(gt_summaries[budget_ind]):
                        summary = segments_to_two_sec_snippets(summary)
                        data = {}
                        data['summary'] = summary
                        data['category'] = category
                        data['name'] = video
                        data['budget'] = budget_frac
                        summary_json_name = '_'.join([
                            category, video, 'gt', str(budget_frac), str(i)
                        ])
                        summary_json_name += '.json'
                        summary_json_path = os.path.join(
                            out_dir, category, video, str(budget_frac)
                        )
                        if not os.path.exists(summary_json_path):
                            os.makedirs(summary_json_path)
                        print('Writing summary file', summary_json_name)
                        json.dump(
                            data, open(os.path.join(
                                summary_json_path, summary_json_name), 'w')
                        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
        Process dataset.json to obtain min and max scores.''')

    parser.add_argument('--dataset', required=True, help='''
        Path to dataset.json.''')
    parser.add_argument('--alpha', default=ALPHA, type=float, help='''
        Alpha value for score function.''')
    parser.add_argument('--beta', default=BETA, type=float, help='''
        Beta factor for score function.''')
    parser.add_argument('--penalty', default=PENALTY, type=float, help='''
        Penalty. Must be negative.''')
    parser.add_argument('--category', default=None, help='''
        Category to process. If not specified will run for all categories.''')
    parser.add_argument('--video', default=None, help='''
        Video to process. If not specified will run for all videos.
        If this option is provided a category must also be provided.''')
   # parser.add_argument('--mode', default=2, type=int, choices={1, 2}, help='''
   #     Mode for score calculation. 1: linear, 2: squared. Not used in this''')
    parser.add_argument('--out-dir', required=True, help='''
        Directory to store outputs.''')

    args = parser.parse_args()

    loss_params = {
        #'mode': args.mode,
        'alpha': args.alpha,
        'beta': args.beta,
        'penalty': args.penalty
    }

    print("Loss params: %s." % str(loss_params))

    if args.video is not None and args.category is None:
        raise ValueError("Option video provided without option category.")

    process_dataset(
            args.dataset, args.category, args.video, loss_params, args.out_dir
    )
