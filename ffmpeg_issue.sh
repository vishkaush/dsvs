#!/bin/bash

#for file in /media/ssd1/data/final/Surveillance\ Office/*.mp4
#do
#    ffmpeg -i "$file" -preset ultrafast -movflags faststart -threads 0 -strict -2 "${file/.mp4/_new.mp4}"
#done
#
#for file in /media/ssd1/data/final/Surveillance\ Entry\ Exit/*.mp4
#do
#    ffmpeg -i "$file" -preset ultrafast -movflags faststart -threads 0 -strict -2 "${file/.mp4/_new.mp4}"
#done
#
#for file in /media/ssd1/data/final/Birthday/*.mp4
#do
#    ffmpeg -i "$file" -preset ultrafast -movflags faststart -threads 0 -strict -2 "${file/.mp4/_new.mp4}"
#done
#
#for file in /media/ssd1/data/final/Cricket/*.mp4
#do
#    ffmpeg -i "$file" -preset ultrafast -movflags faststart -threads 0 -strict -2 "${file/.mp4/_new.mp4}"
#done
#
#for file in /media/ssd1/data/final/Soccer/*.mp4
#do
#    ffmpeg -i "$file" -preset ultrafast -movflags faststart -threads 0 -strict -2 "${file/.mp4/_new.mp4}"
#done

for file in /media/ssd1/data/final/Surveillance\ Classroom/*.mp4
do
    ffmpeg -i "$file" -preset ultrafast -movflags faststart -threads 0 -strict -2 "${file/.mp4/_new.mp4}"
done
for file in /media/ssd1/data/final/Surveillance\ Classroom/*.avi
do
    ffmpeg -i "$file" -preset ultrafast -movflags faststart -threads 0 -strict -2 "${file/.avi/_new.avi}"
done
