# A Framework towards Domain Specific Video Summarization

This project is an implementation of our paper [A Framework towards Domain Specific Video Summarization](https://arxiv.org/abs/1809.08854).
