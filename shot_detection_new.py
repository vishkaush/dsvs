import sys
import os
import json
import subprocess
import math
from bs4 import BeautifulSoup

#Pass the arguments as: <dataset_json_file> <output_dir>

#generate shots
def generate_shots(dataset, out_dir):
    #get all the categories from the dataset file
    categories = dataset['categories'].keys()
    #gets the root directory where our dataset is stored
    root = dataset['root']
    root = root.encode('utf-8')
    for each_category in categories:
        #for each video, generates the shots json file which classifies which snippet belongs to which shot number
        for key,value in dataset['categories'][each_category].items():
            if key != "num_videos" and key != "total_duration":
                #checks if the json file is already created
                if(os.path.exists(out_dir + '/' + each_category + '_' + key + '_shots' + '.json')):
                    print("------------------------------------------")
                    print("Skipping " + key + ".json, already exists.")
                    print("------------------------------------------")
                    continue

                #gets the complete path for the file
                file_name = dataset['categories'][each_category][key]['file_name']
                file_name = file_name.encode('utf-8')
                file_path = root + '/' + each_category + '/' + file_name            

                #finding the shots
                command = "shotdetect -i " + "'" + file_path + "'" + " -o " + out_dir + " -s 60 -v"
                print("------------------------------------------")
                print("Finding shots for video: " + key)
                print("------------------------------------------")
                process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
                process.wait()
                file = open(out_dir + 'result.xml', 'r')
                result = BeautifulSoup(file, 'lxml')
                currentShotId = 0
                currentSnippetId = 0
                backlog1 = 0
                backlog2 = 0
                accumulatedDuration = 0
                totalNumSnippets = (int(dataset['categories'][each_category][key]['duration']))/2
                #print("totalNumSnippets = " + str(totalNumSnippets))
                #totalNumSnippets = duration of video / 2
                #totalNumSnippets = 16
                stop = False
                accumulating = False
                data={}
                shots = result.findAll('shots') 
                #print(shots)
                for i, s in enumerate(shots[0]):
                    #print(i, s)
                    if stop == True:
                        #print("Have been asked to stop, stopping")
                        break
                    if accumulating:
                        #print("We have been accumulating")
                        accumulatedDuration += int(s['msduration'])
                        if accumulatedDuration > 2000:
                            #print("Enough accumulated")
                            accumulating = False
                            #assign snippets to this
                            remaining = accumulatedDuration - backlog1
                            #print("Initial remaining for this shot = " + str(remaining))
                            while(remaining >= 2000):
                                #assign currentSnippetId to currentShotId
                                data[currentSnippetId] = currentShotId 
                                #print("Assigned " + str(currentSnippetId) + " to " + str(currentShotId))
                                #print("Current snippet id: " + currentSnippetId)
                                currentSnippetId = currentSnippetId + 1
                                if currentSnippetId == totalNumSnippets:
                                    print("No more remaining snippets to assign")
                                    stop = True
                                    break                           
                                remaining = remaining - (2000 - backlog2)
                                #print("Remaining after assigning = " + str(remaining))
                                backlog1 = 0
                                backlog2 = 0 
                            if remaining == 0:
                                backlog1 = 0
                                backlog2 = 0
                            elif remaining >= 1000 and not stop:
                                #assign the next snippet also to this shot
                                data[currentSnippetId] = currentShotId
                                #print("Assigned " + str(currentSnippetId) + " to " + str(currentShotId))
                                currentSnippetId = currentSnippetId + 1
                                backlog1 = 2000 - remaining
                            else:
                                backlog2 = remaining
                            accumulatedDuration = 0
                        else:
                            #print("Need to accumulate more")
                            continue
                    else:
                        #print("We were not accumulating...")
                        if int(s['msduration']) <= 2000:
                            #print("...but the new shot is very small, will need to start accumulating")
                            accumulatedDuration = int(s['msduration'])
                            accumulating= True
                            continue
                        else:
                            #print("...and this shot is big enough")
                            #assign snippets to this
                            remaining = int(s['msduration']) - backlog1
                            #print("Initial remaining for this shot" + str(remaining))
                            while(remaining >= 2000):
                                #assign currentSnippetId to currentShotId
                                data[currentSnippetId] = currentShotId 
                                #print("Assigned " + str(currentSnippetId) + " to " + str(currentShotId))
                                currentSnippetId = currentSnippetId + 1
                                if currentSnippetId == totalNumSnippets:
                                    print("More snippets can't be added")
                                    stop = True
                                    break                           
                                remaining = remaining - (2000 - backlog2)
                                #print("Remaining after assigning = " + str(remaining))
                                backlog1 = 0
                                backlog2 = 0 
                            if remaining == 0:
                                backlog1 = 0
                                backlog2 = 0
                            elif remaining >= 1000 and not stop:
                                #assign the next snippet also to this shot
                                data[currentSnippetId] = currentShotId
                                #print("Assigned " + str(currentSnippetId) + " to " + str(currentShotId))
                                currentSnippetId = currentSnippetId + 1
                                backlog1 = 2000 - remaining
                            else:
                                backlog2 = remaining
                            currentShotId += 1
                #writing into json files
                with open(out_dir + '/' + each_category + '_' + key + '_shots' + '.json', 'w') as outfile:
                    json.dump(data, outfile)
                os.rename(out_dir + 'result.xml', out_dir + 'result_' + each_category + '_' + key + '.xml')
                delete_unnecessary_files(out_dir)
                
#deleting the unnecessary files created during the shots generation
def delete_unnecessary_files(out_dir):
    os.remove(out_dir + '/finished')
    os.remove(out_dir + '/hsv.png')
    os.remove(out_dir + '/colors.png')
    os.remove(out_dir + '/motion.csv')
    os.remove(out_dir + '/qte_mvmt.png')
    #os.remove(out_dir + '/result.xml')
    os.remove(out_dir + '/video.xml')

def main():
    #checks if two arguments are passes in the command line
    if (len(sys.argv) != 3):
        print("------------------------------------------")
        print(" Pass argument as: <dataset_json_file> <output_directory>")
        print("------------------------------------------")
        sys.exit(0)
    #checks if the dataset file exists at the specified directory
    if (os.path.exists(sys.argv[1])):
        dataset_file = sys.argv[1]
    else: 
        print("------------------------------------------")
        print("File " + sys.argv[1] + " does not exists. Enter a valid directory or File.")
        print("------------------------------------------")
        sys.exit(0)
    #checks if the output directory exists
    if (os.path.exists(sys.argv[2])):
        out_dir = sys.argv[2]
    else: 
        print("------------------------------------------")
        print("Directory " + sys.argv[2] + " does not exists. Enter a valid directory.")
        print("------------------------------------------")
        sys.exit(0)    
    #reads the dataset file
    with open(dataset_file) as f:
        dataset = json.load(f)
    #generates shots
    generate_shots(dataset, out_dir)


if __name__ == '__main__':
    main()
