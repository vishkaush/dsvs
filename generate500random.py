#Arguments are, in order:
#Path of Dataset JSON
#Path of folder where the random JSONs have to be saved. The path should be with / at the end


import json
import numpy as np
import math
import cv2
import time
import sys
import datetime
import itertools
import collections
import functools
from random import *
import random
import os
import errno

random.seed(1000)

beta=6

def time_in_seconds_annotation(numbers):
    # print(numbers)
    seconds=int(numbers[4])+10*int(numbers[3])+60*int(numbers[2])+600*int(numbers[1])+3600*int(numbers[0])
    return seconds

ratings=np.zeros(10000)
repetitives=np.zeros(10000)


def make_json(summary,title,category,video_name,algo,budget):
    data={}
    # print(summary)
    # print("MAKE_JSON")
    data["summary"]=summary
    # print("DATA SUMMARY")
    # print(data["summary"])
    data["category"]=category
    data["video"]=video_name
    data["algorithm"]=algo
    data["budget"]=budget
    if not os.path.exists(os.path.dirname(title+'.json')):
            try:
                os.makedirs(os.path.dirname(title+'.json'))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
    with open(title+'.json', 'w') as outfile:
            json.dump(data, outfile)


def get_score_candidate(data,category,beta,fraction,video_name):
    # print("ALGORITHM 3")

    annotation_file=data["categories"][category][video_name]["annotation"]
    length=data["categories"][category][video_name]["duration"]
    # print(length)
    budget=fraction*length
    budget2=budget
    length = length if length % 2 == 0 else length - 1
    video=data["root"]+category+"/"+data["categories"][category][video_name]["file_name"]
    scenes = json.load(open(data["root"]+category+"/"+annotation_file))
    # print (scenes)
    # print(video_name)
    allowable_scenes=[]
    # print(annot)

    arr=np.zeros(10)
    beta_arr=np.zeros(4)
    n_scenes=np.zeros(3)
    items=[]
    arr1=np.zeros(10)
    for scene in scenes:
        item={}
        time=0
        begin=time_in_seconds_annotation(scene["begin"].replace(':',''))
        end=time_in_seconds_annotation(scene["end"].replace(':',''))
        if begin%2==1:
            begin+=1
        if end%2==1 and end < length:
            end+=1
        elif end%2==1 and end > length:
            end-=1
        elif end%2==1:
            raise ValueError("Invalid end", end)
        if end == begin:
            continue
        rept="repetitive" in scene
        if scene["rating"]>=0 and end-begin>0:
            if rept == True and scene["repetitive"] == True:
                if (end - begin) > beta:
                    i = randint(begin, end - beta)
                    new_scene = {}
                    new_scene["begin"] = i
                    new_scene["end"] = i + beta
                    allowable_scenes.append(new_scene)
                else:
                    new_scene={}
                    new_scene["begin"]=begin
                    new_scene["end"]=end
                    allowable_scenes.append(new_scene)
            else:
                new_scene={}
                new_scene["begin"]=begin
                new_scene["end"]=end
                allowable_scenes.append(new_scene)
        rating=scene["rating"]

    budget=int(budget)
    if budget%2==1:
        budget=budget-1

    all_random_summaries=[]
    corrupt = False
    # print(allowable_scenes)
    for idx in range(500):
        print("Summary no. " + str(idx))
        corrupt = False
        remainingBudget=budget
        random_summary=[]
        trials=0
        while remainingBudget>0:
            i=randint(0,len(allowable_scenes)-1)
            random_segment=allowable_scenes[i]

            begin=random_segment["begin"]
            end=random_segment["end"]
            temp = []
            present = False
            for t in range(begin, end, 2):
                snippet = {}
                snippet["begin"] = t
                snippet["end"] = t+2
                temp.append(snippet)
                if snippet in random_summary:
                    present = True

            if end-begin<=remainingBudget and random_segment not in random_summary and not present:
                trials = 0
                for snippet in temp:
                    random_summary.append(snippet)
                remainingBudget-=end-begin
            else:
                trials+=1
                if trials>=1000:
                    #print("Tried 1000 times for picking next valid random segment - didn't succeed")
                    print("Not possible")
                    corrupt = True
                    break
        if not corrupt:
            make_json(random_summary,sys.argv[2]+"/"+category+"/"+video_name+"/"+str(fraction)+"/"+category+"_"+video_name+"_random_"+str(fraction)+"_"+str(idx),category,video_name,"random",fraction)

data = json.load(open(sys.argv[1]))

for fraction in [0.05,0.15,0.3]:
    for category in data["categories"]:
        for video_name in data["categories"][category]:
            if video_name != "num_videos" and video_name != "total_duration":
            # if video_name=="birthday1":
                print("Generating random summaris for " + video_name)
                scenes_rating=[[],[],[],[]]
                get_score_candidate(data,category,beta,fraction,video_name)
