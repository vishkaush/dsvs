#!/usr/bin/python3

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import sys
import os
import cv2
import logging
import pickle
import numpy as np
import tensorflow as tf
import argparse
import json
import vgg19
import extract_frames

logging.basicConfig(level=logging.DEBUG)

def preprocess_frame(frame):
    """
    Resize frames for VGGNet
    """
    short_edge = min(frame.shape[:2])
    x_edge = int((frame.shape[0] - short_edge) / 2)
    y_edge = int((frame.shape[1] - short_edge) / 2)
    frame = frame[x_edge: x_edge + short_edge, y_edge: y_edge + short_edge]
    return cv2.resize(frame, (224, 224)).reshape(1, 224, 224, 3)

def get_vgg_features(
        video_file, video_segments, extraction_fps=5.0, snippet_size=2,
        threshold=0.5, batch_size=64):
    logging.info("Extracting features of %s" % video_file)
    with tf.Session() as sess:
        images = tf.placeholder("float", [None, 224, 224, 3])

        vgg_net = vgg19.Vgg19()
        with tf.name_scope("content_vgg"):
            vgg_net.build(images)

        feature = np.array([]).reshape((0, 4096))
        concept = np.array([]).reshape((0, 1000))
        p_concept = np.array([]).reshape((0, 1000))

        num_segments = 0

        for segments in extract_frames.get_frames(
                video_file, video_segments, preprocess_frame,
                extraction_fps, snippet_size):
            num_segments += 1
            seg_feature = np.array([]).reshape((0, 4096))
            seg_p_concept = np.array([]).reshape((0, 1000))
            for batch_start in range(0, len(segments), batch_size):
                batch = np.concatenate(segments[batch_start: batch_start + batch_size])
                feed_dict = {images: batch}
                fc6, prob = sess.run([vgg_net.fc6, vgg_net.prob], feed_dict=feed_dict)
                seg_feature = np.append(seg_feature, fc6, axis=0)
                seg_p_concept = np.append(seg_p_concept, prob, axis=0)

            feature = np.append(feature, np.mean(seg_feature, axis=0).reshape((1, 4096)), axis=0)
            seg_concept = np.mean(seg_p_concept, axis=0)
            p_concept = np.append(p_concept, seg_concept.reshape((1, 1000)), axis=0)
            seg_concept[seg_concept > threshold] = 1
            seg_concept[seg_concept <= threshold] = 0
            concept = np.append(concept, seg_concept.reshape((1, 1000)), axis=0)

    assert num_segments == len(feature) == len(concept) == len(p_concept)

    vgg_features = {"num_features": len(feature), "vgg_features": feature,
                    "vgg_concepts": concept, "vgg_p_concepts": p_concept}

    return vgg_features


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Extract VGGNet19 features from a video.")
    parser.add_argument('--video_file', required=True,
            help='''Path to video to extract features from.''')
    parser.add_argument('--out_dir', required=True,
            help='''Path to directory where extracted features must be saved.''')
    parser.add_argument('--segment_json',
            help='''Path to video segment JSON.''')
    parser.add_argument('--extraction_fps', type=float, default=5.0,
            help='''FPS at which the video frames are to extracted.''')
    parser.add_argument('--snippet_size', type=int, default=2,
            help='''Deafult snippet size to segment the video. Used only if
            --segment_json is not provided.''')

    args = parser.parse_args()

    if args.segment_json is None:
        print("Segment JSON set as None. Will use %d second snippets."
                % (args.snippet_size))
        video_segments = None
    else:
        video_segments = extract_frames.JSON_to_segment_tuple(args.segment_json)


    base_file_name = os.path.basename(args.video_file)
    base_file_name_no_ext = os.path.splitext(base_file_name)[0]

    feature = get_vgg_features(
            args.video_file, video_segments,
            extraction_fps=args.extraction_fps, snippet_size=args.snippet_size)
    feature["file_name"] = base_file_name
    feature_json = dict()
    for key in feature:
        if isinstance(feature[key], np.ndarray):
            feature_json[key] = feature[key].tolist()
        else:
            feature_json[key] = feature[key]
    with open(args.out_dir + "/" + base_file_name_no_ext + ".vgg_features.pkl", 'wb') as f:
        pickle.dump(feature, f, protocol=2)
    with open(args.out_dir + "/" + base_file_name_no_ext + ".vgg_features.json", 'w') as f:
        f.write(json.dumps(feature_json))
