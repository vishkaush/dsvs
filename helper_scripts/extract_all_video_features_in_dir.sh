#!/bin/bash

in_dir=$1
out_dir=$2

for vid in "$in_dir"/*.mp4
do
    out_vid="$out_dir/${vid##*/}"
    if [ ! -f "${out_vid%.mp4}.color_hist_features.json" ] || [ ! -f "${out_vid%.mp4}.color_hist_features.pkl" ] ; then
        echo "color_hist_of: " $vid
        ./extract_color_hist_features.py --video_file "$vid" --out_dir "$out_dir"
    fi
    if [ ! -f "${out_vid%.mp4}.googlenet_features.json" ] || [ ! -f "${out_vid%.mp4}.googlenet_features.pkl" ] ; then
        echo "googlenet of: " $vid
        ./extract_googlenet_features.py --video_file "$vid" --out_dir "$out_dir"
    fi
    if [ ! -f "${out_vid%.mp4}.vgg_features.json" ] || [ ! -f "${out_vid%.mp4}.vgg_features.pkl" ] ; then
        echo "vggnet of: " $vid
        ./extract_vgg19_features.py --video_file "$vid" --out_dir "$out_dir"
    fi
    if [ ! -f "${out_vid%.mp4}.yolo_voc_features.json" ] || [ ! -f "${out_vid%.mp4}.yolo_voc_features.pkl" ] ; then
        echo "yolo voc of: " $vid
        ./extract_yolo_features.py --mode voc --video_file "$vid" --out_dir "$out_dir"
    fi
    if [ ! -f "${out_vid%.mp4}.yolo_coco_features.json" ] || [ ! -f "${out_vid%.mp4}.yolo_coco_features.pkl" ] ; then
        echo "yolo coco of: " $vid
        ./extract_yolo_features.py --mode coco --video_file "$vid" --out_dir "$out_dir"
    fi
done
