#!/usr/bin/python3
import sys
import logging
import pickle
import numpy as np
import scipy.io

logging.basicConfig(level=logging.DEBUG)

def get_ratings(mat_file):
    gt = scipy.io.loadmat(mat_file)
    snippet_size = 2.0
    duration = gt.get('video_duration')

    num_snippets = int(duration / snippet_size) + 1 \
            if duration % snippet_size != 0 \
            else int(duration / snippet_size)
    num_users = gt.get('user_score').shape[1]

    ratings = []
    user_gt = []
    budget = []

    for i in range(num_users):
        user_ratings = [0] * num_snippets

        for segment in gt.get('segments')[0][i]:
            start = segment[0]
            end = segment[1]
            curr = int(int(start / snippet_size) * snippet_size)

            while curr < end:
                user_ratings[int(curr / snippet_size)] = 1
                curr += snippet_size

        user_gt.append(user_ratings)
        budget.append(int(np.sum(user_ratings)))

    mean = np.mean(user_gt, axis=0)
    std = np.std(user_gt, axis=0)
    mod_z = np.abs((user_gt - mean) / std)
    mod_z = np.nan_to_num(mod_z)

    ratings = (mean + mod_z * user_gt) / (1 + mod_z)

    # Convert to 0-1 range
    ratings[ratings > 0.8] = -30
    ratings[ratings > 0.6] = -20
    ratings[ratings > 0.4] = -10
    ratings[ratings > 0.2] = -5
    ratings[ratings >= 0] = -100
    ratings[ratings ==  -100] = -1
    ratings[ratings == -5] = 0
    ratings[ratings == -10] = 1
    ratings[ratings == -20] = 2
    ratings[ratings == -30] = 3


    return ratings, np.array(user_gt), np.array(budget), snippet_size, duration


if __name__ == "__main__":
    mat_file = sys.argv[1]
    out_dir = sys.argv[2]
    base_file_name = mat_file.split("/")[-1].split(".mat")[0]

    ratings, gt, budget, snippet_size, duration = get_ratings(mat_file)

    pickle_data = {"ratings": ratings, "gt": gt, "budget": budget,
            "snippet_size": snippet_size, "duration": duration}

    with open(out_dir + "/" + base_file_name + ".ratings.pkl", 'wb') as f:
        pickle.dump(pickle_data, f, protocol=2)
