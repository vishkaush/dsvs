#!/usr/bin/python3

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import sys
import os
import cv2
import logging
import pickle
import json
import numpy as np
import lightnet
import extract_frames
import argparse

logging.basicConfig(level=logging.DEBUG)

def get_yolo_features(
        video_file, video_segments, mode,
        extraction_fps=5.0, snippet_size=2, threshold=0.5):
    num_segments = 0
    logging.info("Extracting features of %s" % video_file)
    concept_dim = 20 if mode == 'voc' else 80
    cfg_dir = './yolo_cfg/voc' if mode == 'voc' else './yolo_cfg/coco'
    concept = np.array([]).reshape((0, concept_dim))
    p_concept = np.array([]).reshape((0, concept_dim + 1)) # For dummy object

    yolo = lightnet.load('yolo-' + mode, cfg_dir)
    for segments in extract_frames.get_frames(
            video_file, video_segments,
            extraction_fps=extraction_fps, snippet_size=snippet_size):
        num_segments += 1
        seg_concept = np.zeros(concept_dim)
        seg_p_concept = np.zeros(concept_dim + 1) # For dummy concept
        for frame in segments:
            #frame_concept = [0] * concept_dim
            #frame_p_concept = [0] * concept_dim
            detections = yolo(lightnet.Image.from_bytes(
                    cv2.imencode('.jpg', frame)[1].tobytes()))
            for ins in detections:
                #frame_concept[ins[0]] += 1
                #frame_p_concept[ins[0]] = max(frame_p_concept[ins[0]], ins[2])
                seg_concept[ins[0]] += 1
            #seg_concept = np.maximum(seg_concept, frame_concept)
            #seg_p_concept = np.maximum(seg_p_concept, frame_p_concept)
        seg_concept = seg_concept / len(segments)
        if np.sum(seg_concept) == 0:
            # Dummy object probablity is one
            # if no object found in segment
            seg_p_concept[concept_dim] = 1
        else:
            seg_p_concept[concept_dim] = 0
            seg_p_concept[:concept_dim] = seg_concept / np.sum(seg_concept)
        p_concept = np.append(p_concept, seg_p_concept.reshape((1, concept_dim + 1)), axis=0)
        concept = np.append(concept, seg_concept.reshape((1, concept_dim)), axis=0)

    assert num_segments == len(concept) == len(p_concept)

    yolo_features = {"num_features": len(concept),
                     "yolo_" + mode + "_concepts": concept,
                     "yolo_" + mode + "_p_concepts": p_concept}

    return yolo_features


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Extract VGGNet19 features from a video.")
    parser.add_argument('--video_file', required=True,
            help='''Path to video to extract features from.''')
    parser.add_argument('--out_dir', required=True,
            help='''Path to directory where extracted features must be saved.''')
    parser.add_argument('--mode', required=True, choices=['voc', 'coco'],
            help='''Feature to extract. Either 'voc' or 'coco.''')
    parser.add_argument('--segment_json',
            help='''Path to video segment JSON.''')
    parser.add_argument('--extraction_fps', type=float, default=5.0,
            help='''FPS at which the video frames are to extracted.''')
    parser.add_argument('--snippet_size', type=int, default=2,
            help='''Deafult snippet size to segment the video. Used only if
            --segment_json is not provided.''')

    args = parser.parse_args()

    if args.segment_json is None:
        print("Segment JSON set as None. Will use %d second snippets."
                % (args.snippet_size))
        video_segments = None
    else:
        video_segments = extract_frames.JSON_to_segment_tuple(args.segment_json)


    base_file_name = os.path.basename(args.video_file)
    base_file_name_no_ext = os.path.splitext(base_file_name)[0]

    feature = get_yolo_features(
            args.video_file, video_segments, args.mode,
            extraction_fps=args.extraction_fps, snippet_size=args.snippet_size)
    feature["file_name"] = base_file_name
    feature_json = dict()
    for key in feature:
        if isinstance(feature[key], np.ndarray):
            feature_json[key] = feature[key].tolist()
        else:
            feature_json[key] = feature[key]
    with open(args.out_dir + "/" + base_file_name_no_ext + ".yolo_" \
            + args.mode + "_features.pkl", 'wb') as f:
        pickle.dump(feature, f, protocol=2)
    with open(args.out_dir + "/" + base_file_name_no_ext + ".yolo_" \
            + args.mode + "_features.json", 'w') as f:
        f.write(json.dumps(feature_json))
