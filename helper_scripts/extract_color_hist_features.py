#!/usr/bin/python3
import sys
import os
import cv2
import logging
import pickle
import numpy as np
import json
import extract_frames
import argparse

logging.basicConfig(level=logging.DEBUG)

def get_color_hist_features(
        video_file, video_segments, extraction_fps=5.0, snippet_size=2):
    logging.info("Extracting features of %s" % video_file)
    hist_r_feature = np.array([]).reshape((0, 256))
    hist_g_feature = np.array([]).reshape((0, 256))
    hist_b_feature = np.array([]).reshape((0, 256))
    hist_h_feature = np.array([]).reshape((0, 180))
    hist_v_feature = np.array([]).reshape((0, 256))

    num_segments = 0

    for segments in extract_frames.get_frames(
            video_file, video_segments,
            extraction_fps=extraction_fps, snippet_size=snippet_size):
        num_segments += 1

        seg_r_hist = cv2.calcHist(segments, [2], None, [256], [0, 256]).reshape((1, 256))
        seg_g_hist = cv2.calcHist(segments, [1], None, [256], [0, 256]).reshape((1, 256))
        seg_b_hist = cv2.calcHist(segments, [0], None, [256], [0, 256]).reshape((1, 256))

        seg_r_hist /= np.sum(seg_r_hist)
        seg_g_hist /= np.sum(seg_g_hist)
        seg_b_hist /= np.sum(seg_b_hist)

        hist_r_feature = np.append(hist_r_feature, seg_r_hist, axis=0)
        hist_g_feature = np.append(hist_g_feature, seg_g_hist, axis=0)
        hist_b_feature = np.append(hist_b_feature, seg_b_hist, axis=0)

        hsv_segments = [cv2.cvtColor(x, cv2.COLOR_BGR2HSV) for x in segments]
        seg_h_hist = cv2.calcHist(hsv_segments, [0], None, [180], [0, 180]).reshape((1, 180))
        seg_v_hist = cv2.calcHist(hsv_segments, [1], None, [256], [0, 256]).reshape((1, 256))

        seg_h_hist /= np.sum(seg_h_hist)
        seg_v_hist /= np.sum(seg_v_hist)

        hist_h_feature = np.append(hist_h_feature, seg_h_hist, axis=0)
        hist_v_feature = np.append(hist_v_feature, seg_v_hist, axis=0)

    assert num_segments == len(hist_r_feature) == len(hist_g_feature) == len(hist_b_feature)

    color_hist_features = {"num_features": num_segments, "color_hist_r_features": hist_r_feature,
                          "color_hist_g_features": hist_g_feature, "color_hist_b_features": hist_b_feature,
                          "color_hist_h_features": hist_h_feature, "color_hist_s_features": hist_v_feature}

    return color_hist_features


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Extract VGGNet19 features from a video.")
    parser.add_argument('--video_file', required=True,
            help='''Path to video to extract features from.''')
    parser.add_argument('--out_dir', required=True,
            help='''Path to directory where extracted features must be saved.''')
    parser.add_argument('--segment_json',
            help='''Path to video segment JSON.''')
    parser.add_argument('--extraction_fps', type=float, default=5.0,
            help='''FPS at which the video frames are to extracted.''')
    parser.add_argument('--snippet_size', type=int, default=2,
            help='''Deafult snippet size to segment the video. Used only if
            --segment_json is not provided.''')

    args = parser.parse_args()

    if args.segment_json is None:
        print("Segment JSON set as None. Will use %d second snippets."
                % (args.snippet_size))
        video_segments = None
    else:
        video_segments = extract_frames.JSON_to_segment_tuple(args.segment_json)


    base_file_name = os.path.basename(args.video_file)
    base_file_name_no_ext = os.path.splitext(base_file_name)[0]

    feature = get_color_hist_features(
            args.video_file, video_segments,
            extraction_fps=args.extraction_fps, snippet_size=args.snippet_size)
    feature["file_name"] = base_file_name
    feature_json = dict()
    for key in feature:
        if isinstance(feature[key], np.ndarray):
            feature_json[key] = feature[key].tolist()
        else:
            feature_json[key] = feature[key]
    with open(args.out_dir + "/" + base_file_name_no_ext + ".color_hist_features.pkl", 'wb') as f:
        pickle.dump(feature, f, protocol=2)
    with open(args.out_dir + "/" + base_file_name_no_ext + ".color_hist_features.json", 'w') as f:
        f.write(json.dumps(feature_json))
