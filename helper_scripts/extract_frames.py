#!/usr/bin/python3
import sys
import cv2
import logging
import pickle
import numpy as np


logging.basicConfig(level=logging.DEBUG)

def get_frames(
        video_file, video_segments,
        frame_preprocess_func=None, extraction_fps=5.0, snippet_size=2):
    """
    This is a generator for memory efficiency reasons.
    video_segments: a list (start, end) timestamps in seconds of video segments
                    or None if we want to use 2 second snippets
    frame_preprocess_func: a function which process the frame to the desired
                           resolution/color scheme of the caller
    """
    logging.info("Extracting frames from %s." % video_file)
    vid = cv2.VideoCapture(video_file)
    video_fps = vid.get(cv2.CAP_PROP_FPS)
    tot_frames = vid.get(cv2.CAP_PROP_FRAME_COUNT)
    video_len_in_s = tot_frames / video_fps
    logging.info("Video length %f." % video_len_in_s)
    logging.info("Video fps %f." % video_fps)
    seek_length_ms = int(1000 / extraction_fps)
    assert seek_length_ms > 0
    if video_segments is not None:
        video_segments = sorted(video_segments)
    else:
        video_segments = [(x, x + snippet_size) \
                for x in range(0, int(video_len_in_s), snippet_size)]

    frames = []
    success, prev_frame = vid.read()
    if not success:
        raise ValueError("Cannot read first frame of %s" % video_file)

    for segment in video_segments:
        seg_frames = []
        seg_start, seg_end = segment
        logging.info("Processing snippet: (%d, %d)." % (seg_start, seg_end))
        for seek_pos_in_ms in range(seg_start * 1000, seg_end * 1000, seek_length_ms):
            vid.set(cv2.CAP_PROP_POS_MSEC, seg_start * 1000)
            success, frame = vid.read()
            if success:
                if frame_preprocess_func is not None:
                    frame = frame_preprocess_func(frame)
                seg_frames.append(frame)
        assert len(seg_frames) != 0
        logging.info("No. of frames: %d" % len(seg_frames))
        yield seg_frames

