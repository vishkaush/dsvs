#!/usr/bin/python3

import argparse
import pickle
import os
import warnings
import matplotlib
from sklearn.manifold import TSNE

warnings.simplefilter(action='ignore', category=FutureWarning)
matplotlib.use('Agg')

import ggplot
import pandas as pd


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Plot TSNE of various features.")
    parser.add_argument('--feature_files', required=True, nargs='+',
            help='''A list of pickle files from which to load the feature dicts.''')
    parser.add_argument('--feature', required=True,
            choices = ['vgg', 'googlenet', 'color_hist_r', 'color_hist_g',
                       'color_hist_b', 'color_hist_h', 'color_hist_s'],
            help='''The feature for which TSNE is being computed.
                    NOTE: The key "[feature]_features" must be present in the
                          pickle file dicts.''')
    parser.add_argument('--out_file', required=True,
            help='''The path of the .jpg to print the TSNE plot.''')

    args = parser.parse_args()

    features = []
    labels = []
    for feature_file in args.feature_files:
        data = pickle.load(open(feature_file, 'rb'))
        base_file_name = os.path.basename(feature_file)
        base_file_name_no_ext = os.path.splitext(base_file_name)[0]
        features += list(data[args.feature + '_features'])
        labels += [base_file_name_no_ext] * len(data[args.feature + '_features'])

    assert len(features) == len(labels)

    tsne_2d = TSNE().fit_transform(features)
    df = pd.DataFrame(data = {
            'x': tsne_2d[:, 0], 'y': tsne_2d[:, 1], 'labels': labels})

    chart = ggplot.ggplot(df, ggplot.aes(x='x', y='y', color='labels'))
    chart += ggplot.geom_point()

    chart.save(args.out_file)
