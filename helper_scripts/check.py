import json
import sys

one_video_json=json.load(open(sys.argv[1]))
print("Value of num_features = " + str(one_video_json["num_features"]))
print("Size of vgg_features = " + str(len(one_video_json["color_hist_r_features"])))
print("Size of first element of vgg_features = " + str(len(one_video_json["color_hist_r_features"][0])))
print("Size of vgg_p_concepts = " + str(len(one_video_json["color_hist_g_features"])))
print("Size of first element of vgg_p_concepts = " + str(len(one_video_json["color_hist_g_features"][0])))
print("Size of vgg_concepts = " + str(len(one_video_json["color_hist_b_features"])))
print("Size of first element of vgg_concepts = " + str(len(one_video_json["color_hist_b_features"][0])))
print("Size of vgg_concepts = " + str(len(one_video_json["color_hist_h_features"])))
print("Size of first element of vgg_concepts = " + str(len(one_video_json["color_hist_h_features"][0])))
print("Size of vgg_concepts = " + str(len(one_video_json["color_hist_s_features"])))
print("Size of first element of vgg_concepts = " + str(len(one_video_json["color_hist_s_features"][0])))
