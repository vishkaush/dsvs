#!/usr/bin/python2


#run as:  unsupervisedWrapper.py <Dataset_json_file> <complete_feature_root_directory> <complete_output_directory_path>
import sys
import os
import io
import logging
import os.path
import random
import json
import numpy as np
import sklearn.metrics as metrics
import learning_framework
import cPickle as pickle

# FEATURE ORDER:(FOR EACH VIDEO)
# set_cover_concepts
# prob_set_cover_concepts
# fac_location_features
# set_cover_features
# feature_based: function used are(in the same order):- log,sqrt, reciprocal
# disparity_min_features
# uniform
# random

# SIMILARITY_KERNEL_USED:
# cosine_similarity


all_video_names = []
all_categories = []

budget = [0.05, 0.15, 0.30]

numSnippetsInAllVideos = []

#stores the list of  dictionary for all the uniform videos generated
u_summary = []
#stores the list of dictionary for all the random videos generated
r_summary = []
#stores the list of dictionary for all the sub_modular videos generated
sub_mod_summary = []
#appeds all the above three lists
final_summary = []
logging.basicConfig(level=logging.DEBUG)


#find uniform snippets according to the budget from the list of all the snippets
#for each video generated 4 summaries
def generate_store_uniform_summary(output_dir):
    print("------------------------------------------")
    print ("IN UNIFORM SUMMARY")
    print("------------------------------------------")
    #print(type(all_video_names[0]))
    for i, snpt in enumerate(numSnippetsInAllVideos):
        #finds category name from the video name
        video_name = all_video_names[i]
        cat = "_".join(video_name.split('_')[:-1])
        for b in budget:
            file_name = cat + "_" + video_name + '_uniform_' + str(b)
            file = output_dir + '/' + file_name + '.json'
            if os.path.exists(file):
                print("------------------------------------------")
                print('SKIPPING ' + file_name + '.json, ALREADY EXISTS')
                print("------------------------------------------")
                continue
            u_s = []
            snpt_budget = int(b*snpt)
            size = snpt/snpt_budget
            l = []
            for i in range(0,snpt_budget):
                l.append(i*size)
            #creates dictionary for all the (videos x budget)
            d= {}
            d['summary'] = l
            d['budget'] = b
            d['algorithm'] = 'uniform'
            d['video'] = video_name
            d['category'] = cat
            u_s.append(d)
            u_s = convert_list_to_dict(u_s)
            print("------------------------------------------")
            print('WRITING ' + file_name + '.json')
            print("------------------------------------------")
            generate_json_per_video(output_dir, u_s)




#find random snippet according to the budget from the list of all the snippets
#for each video generated 4 summaries
def generate_store_random_summary(output_dir):
    print("------------------------------------------")
    print ("IN RANDOM SUMMARY")
    print("------------------------------------------")
    for i, snpt in enumerate(numSnippetsInAllVideos):
        #finds category name from the video name
        video_name = all_video_names[i]
        cat = "_".join(video_name.split('_')[:-1])
        for b in budget:
            file_name = cat + '_' + video_name + '_random_' + str(b)
            file = output_dir + '/' + file_name + '.json'
            if os.path.exists(file):
                print("------------------------------------------")
                print('SKIPPING ' + file_name + '.json, ALREADY EXISTS')
                print("------------------------------------------")
                continue
            r_s = []
            snpt_budget = int(b*snpt)
            l = random.sample(range(0,snpt),snpt_budget)
            #creates dictionary for all the (videos x budget)
            d= {}
            d['summary'] = l
            d['budget'] = b
            d['algorithm'] = 'random'
            d['video'] = video_name
            d['category'] = cat
            r_s.append(d)
            r_s = convert_list_to_dict(r_s)
            print("------------------------------------------")
            print('WRITING ' + file_name + '.json')
            print("------------------------------------------")
            generate_json_per_video(output_dir, r_s)


#generates summaries for submodular functions
def generate_store_submodular_summary(output_dir, weights, testing_examples, feat_len_dict, sat_cov_c):
    print("------------------------------------------")
    print ("IN GENERATE SUBMODULAR SUMMARY")
    print("------------------------------------------")
    for i, v in enumerate(testing_examples):
        bud = find_budget(i)
        vid = all_video_names[int(i/3)]
        cat = "_".join(vid.split('_')[:-1])
        summary_ind = learning_framework.get_summary_per_shell(v, weights, feat_len_dict, sat_cov_c, bud, vid, cat, output_dir)
        if(len(summary_ind)==0):
            print("------------------------------------------")
            print('SKIPPING ' + cat + '_' + vid + '_' + str(bud) + 'SUBMODULAR_FUNCTIONS' + '.json, ALREADY EXISTS')
            print("------------------------------------------")
            continue
        for dic in summary_ind:
            dic['budget'] = bud
            dic['video'] = vid
            dic['category'] = cat
        summary_ind = convert_list_to_dict(summary_ind)
        for d in summary_ind:
            d_lis = []
            d_lis.append(d)
            print("------------------------------------------")
            print('WRITING ' + cat + '_' + vid + '_' + d_lis[0]['algorithm'] + '_' + str(d_lis[0]['budget']) + '_'+ d_lis[0]['feature']+'.json')
            print("------------------------------------------")
            generate_json_per_video(output_dir, d_lis)


#finds budget value for the above function
def find_budget(i):
    if(i%3) == 0:
        return budget[0]
    elif(i%3) == 1:
        return budget[1]
    else:
        return budget[2]

#converts from list of indices to list of dictoinaries for all the 'summary' key in dictionaries in the 'final_summary' list
def convert_list_to_dict(lis):
    #print(final_summary)
    for dic in lis:
        #print(dic)
        smr_list = dic['summary']
        smr_dic = convert_list(smr_list)
        dic['summary'] = smr_dic
    return lis

#converts list of indices into list of dictionaries
# eg: input -  [1,2,3,4] , output - [{'begin':2, 'end':4},{'begin':4, 'end':6},{'begin':6, 'end':8}]
def convert_list(lis):
    final_list = []
    for x in lis:
        d = {}
        d['begin'] = 2*x
        d['end'] = 2*(x+1)
        final_list.append(d)
    return final_list

def generate_json_per_video(output_dir, lis):
    for dic in lis:
        #creates the name for the summary output file
        file_name = dic['category'] +'_' + dic['video'] + '_' + dic['algorithm'] + '_' + str(dic['budget'])
        if dic.has_key("feature"):
            file_name += '_' + dic['feature']
        cat_directory = output_dir + '/' + dic['category']
        if not os.path.exists(cat_directory):
        	print(cat_directory + " doesn't exist, creating one")
        	os.makedirs(cat_directory)
        vid_directory = cat_directory + '/' + dic['video']
        if not os.path.exists(vid_directory):
        	print(vid_directory + " doesn't exist, creating one")
        	os.makedirs(vid_directory)
        bud_directory = vid_directory + '/' + str(dic['budget'])
        if not os.path.exists(bud_directory):
        	print(bud_directory + " doesn't exist, creating one")
        	os.makedirs(bud_directory)

        file = bud_directory + '/' + file_name
        with io.open(file + '.json','w',encoding="utf-8") as f:
            f.write(unicode(json.dumps(dic, ensure_ascii=False)))

#loops over all the categories
#creates an instance for all the videos for all the different features in all the category
def get_test_data(dataset, feature_path):
    #feature_path is a list containing a path to the features for each category
    #dataset contains all the videos names with their categories, root path
    print("------------------------------------------")
    print ("IN TEST DATA")
    print("------------------------------------------")
    testing_examples = []

    feat_len_dict = dict()
    categories = dataset['categories'].keys()
    all_categories.extend(categories)
    for each_category in categories:
        video_names = []
        for key,value in dataset['categories'][each_category].items():
            if key != "num_videos" and key != "total_duration":
                video_names.append(key)
        all_video_names.extend(video_names)
        testing_example, feat_len_d = get_test_data_per_category(video_names, feature_path, each_category)
        testing_examples.extend(testing_example)
        feature_len_dict = feat_len_d
    return testing_examples, feat_len_dict


#finds the length of all the videos in a category and stores them in an array
#creates an instance for all the videos for all the different features in a category and stores them in a list 'testing_example'
def get_test_data_per_category(video_names, feature_path, category):
    print("------------------------------------------")
    print ("IN TEST DATA PER CATEGORY")
    print("------------------------------------------")
    testing_example = []
    for vid in video_names:
        logging.info("Processing test_file %s" % vid)
        feature_base_path = feature_path + '/' + category + '/' + vid

        print("Feature base path: " + feature_base_path)
        #loads all the features
        vgg_feature = pickle.load(open(feature_base_path + '.vgg_features.pkl', 'rb'))
        googlenet_feature = pickle.load(open(feature_base_path + '.googlenet_features.pkl', 'rb'))
        yolo_voc_feature = pickle.load(open(feature_base_path + '.yolo_voc_features.pkl', 'rb'))
        yolo_coco_feature = pickle.load(open(feature_base_path + '.yolo_coco_features.pkl', 'rb'))
        color_hist_feature = pickle.load(open(feature_base_path + '.color_hist_features.pkl', 'rb'))

        feat_len_dict = dict()

        feature_list = [vgg_feature, googlenet_feature, yolo_voc_feature,
                        yolo_coco_feature, color_hist_feature]

        for feat in feature_list:
            for item in feat:
                if not isinstance(feat[item], int):
                    feat_len_dict[item] = len(feat[item][0])

        # Fix later for shot boundary
        vid_len = 2 * vgg_feature['num_features']
        #gives video length in seconds eg: for cricket_1.mp4 , 3040 sec

        Y = vgg_feature['num_features']
        #outputs [(0, 2), (2, 4), (4, 6), (6, 8),......,(3038,3040)]
        snippet_timestamp = [(x * 2, x * 2 + 2) for x in range(Y)]
        numSnippets = len(snippet_timestamp)
        numSnippetsInAllVideos.append(numSnippets)

        gaussian_sigma = 10
        alpha = 1
        beta = 6
        penalty = -2
        for sum_size in [0.05, 0.15, 0.30]:
            # Hard fix later. right now just appprox
            budget = vid_len * sum_size
            max_score = budget * budget * np.exp(3 * alpha)
            min_score = budget * budget * penalty

            S = learning_framework.VideoElem(budget=budget, Y=Y, y_gt = [0], snippet_timestamp=snippet_timestamp,
                    annotation=[], max_score=max_score, min_score=min_score,
                    gaussian_sim_sigma=gaussian_sigma, alpha=alpha, beta=beta, penalty=penalty,
                    vgg_concepts = vgg_feature['vgg_concepts'],
                    googlenet_concepts = googlenet_feature['googlenet_concepts'],
                    yolo_voc_concepts = yolo_voc_feature['yolo_voc_concepts'],
                    yolo_coco_concepts = yolo_coco_feature['yolo_coco_concepts'],
                    vgg_p_concepts = vgg_feature['vgg_p_concepts'],
                    googlenet_p_concepts = googlenet_feature['googlenet_p_concepts'],
                    vgg_features = vgg_feature['vgg_features'],
                    googlenet_features = googlenet_feature['googlenet_features'],
                    color_hist_r_features = color_hist_feature['color_hist_r_features'],
                    color_hist_g_features = color_hist_feature['color_hist_g_features'],
                    color_hist_b_features = color_hist_feature['color_hist_b_features'],
                    color_hist_h_features = color_hist_feature['color_hist_h_features'],
                    color_hist_s_features = color_hist_feature['color_hist_s_features']
                )


            testing_example.append(S)

    return testing_example, feat_len_dict

if __name__ == "__main__":
    print("------------------------------------------")
    print ("IN MAIN")
    print("------------------------------------------")
    #checks if the argument is passes correclty
    if (len(sys.argv) != 4):
        print("------------------------------------------")
        print("Pass arguments as : \n<Dataset_json_file> <complete_feature_root_directory> <complete_output_directory_path>")
        print("------------------------------------------")
        sys.exit(0)

    #dataset json file as input
    json_file = sys.argv[1]
    #feature root directory path
    feature_path = sys.argv[2]
    #output directory path
    output_dir = sys.argv[3]
    #reads the dataset file
    with open(json_file) as f:
        dataset = json.load(f)

    weights = [1]
    testing_examples, feat_len_dict = get_test_data(dataset, feature_path)

    sat_cov_c = 1

    generate_store_uniform_summary(output_dir)
    generate_store_random_summary(output_dir)
    generate_store_submodular_summary(output_dir, weights, testing_examples, feat_len_dict, sat_cov_c)
    #appends the results of above three summaries into a single 'summary' list
    #gives final list of summaries
    print ("...............DONE, CHECK THE OUTPUT DIRECTORY.................")
