import json
import glob
import argparse
import os
from utils import *

beta = 6

def parse(filename):
    try:
        with open(filename) as f:
            return json.load(f)

    except ValueError as e:
        print('invalid json: %s' % e)
        return None # or: raise

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to verify summary sizes of all videos of a particular category")
    parser.add_argument('--category', required=True, help='''Category name''')
    parser.add_argument('--summaries', required=True, help='''Full path to root folder with summaries that need to be checked''')
    parser.add_argument('--dataset', required=True, help='''Full path to the dataset JSON which will be used for getting duration info''')
    args = parser.parse_args()
    dataset = parse(args.dataset)
    category = args.category
    root_path = dataset['root']
    for video_name in dataset["categories"][category]:
        if video_name != "num_videos" and video_name != "total_duration":
            print("Video: " + video_name)
            videoDuration = dataset["categories"][category][video_name]["duration"]
            print("Video duration = " + str(videoDuration))
            json_file_path = os.path.join(
                root_path, category, dataset['categories'][category][video_name]['annotation'])
            annotation = annotation_json_file_to_list(json_file_path)
            all_pos_duration = sum([x[1] - x[0] for x in annotation if x[2] >= 0])
            pos_red_duration = sum([x[1] - x[0] for x in annotation if x[2] >= 0 and x[3]])
            neg_red_duration = sum([x[1] - x[0] for x in annotation if x[2] < 0 and x[3]])
            annotation = normalize_annotation(annotation, beta)
            pos_duration = sum([x[3] for x in annotation if x[2] >= 0])
            print("Positive seconds", pos_duration, all_pos_duration, pos_red_duration, neg_red_duration)
            for budget in [0.05, 0.15, 0.3]:
                tmp = int(float(budget) * int(videoDuration))
                if tmp%2 == 1:
                    tmp = tmp - 1
                expectedNumSnippets = tmp/2
                print("Expected number of snippets in summary of budget " + str(budget) + " = " + str(expectedNumSnippets))
                for filename in glob.glob(args.summaries + category + "/" + video_name + "/" + str(budget) + '/*.json', recursive=True):
                    print("Checking " + os.path.basename(filename))
                    test = parse(filename)
                    actualNumSnippets = len(test["summary"])
                    print("Found actualNumSnippets = " + str(actualNumSnippets))
                    if actualNumSnippets != expectedNumSnippets:
                        print(os.path.basename(filename) + ": **** Mismatch: expected " + str(expectedNumSnippets) + " found " + str(actualNumSnippets))
