import json
import glob
import argparse
import os

def parse(filename):
    try:
        with open(filename) as f:
            return json.load(f)

    except ValueError as e:
        print('invalid json: %s' % e)
        return None # or: raise

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to do a sanity check on all JSONs")
    parser.add_argument('--root', required=True, help='''Full path to root folder inside which all JSONs need to be checked''')
    parser.add_argument('--dataset', required=True, help='''Full path to the dataset JSON which will be used for getting duration info''')
    args = parser.parse_args()
    dataset = parse(args.dataset)
    for filename in glob.glob(args.root + '**/*.json', recursive=True):
        lastEnd = "0:00:00"
        print("Checking " + filename)
        test = parse(filename)
        baseFileName = os.path.basename(filename)
        videoName = os.path.splitext(baseFileName)[0]
        components = videoName.split("_")
        if len(components) == 2:
            category = components[0]
        elif len(components) == 3:
            category = components[0] + " " + components[1]
        elif len(components) == 4:
            category = components[0] + " " + components[1] + " " + components[2]
        else:
            print("WEIRD!!!!CAUTION!!!!")
        duration = dataset["categories"][category][videoName]["duration"]
        for item in test:
            if "begin" not in item or "end" not in item or "rating" not in item:
                print("Found a segment with missing begin or end or rating")
            if type(item["rating"]) != int:
                print("Found a segment with a non-integer rating value")
                print("begin " + item["begin"])
            if "repetitive" in item and item["repetitive"] == False:
                print("Found a segment explicitly marked non-repetitive")
                print("begin " + item["begin"])
            begin = item["begin"]
            if begin != lastEnd:
                print("Found a segment whose begin is not equal to last end")
                print("begin " + item["begin"])
                print("lastEnd " + lastEnd)
            lastEnd = item["end"]
        durationNumbers = lastEnd.replace(':','')
        calculatedDuration = int(durationNumbers[4])+10*int(durationNumbers[3])+60*int(durationNumbers[2])+600*int(durationNumbers[1])+3600*int(durationNumbers[0])
        #print("Calculated duration = " + str(calculatedDuration))
        #print("Original duration = " + str(duration))
        if calculatedDuration != duration:
            print("CAUTION: Total duration mismatch!")
